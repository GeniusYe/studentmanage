<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户登录</title>
<link href="css/login.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="plugins/encryption/sha1.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#login-form").bind("submit", function()
		{
			$("#login-form input[name='j_password']").val(hex_sha1($("#login-form input[name='password']").val()));
		});
		
		window.location.href = "http://ics.fudan.edu.cn";
	});
</script>
</head>

<body>
	<div id="message-box"> 用户名或密码错误！ </div>
	<div id="wrap">
		<div id="header"> </div>
	    <div id="content-wrap">
	    	<div class="space"> </div>
   	  		<form id="login-form" action="j_spring_security_check" method="post"><div class="content">
		        <div class="field"><label>账　户：</label><input class="username" name="j_username" value="" type="text" /></div>
				<div class="field"><label>密　码：</label><input class="password" name="password" value="" type="password" /><br /></div>
				<input class="password" name="j_password" value="" type="hidden" />
		        <!-- <div class="field"><label>验证码：</label><input class="captcha" maxlength="6" name="" value="DDZTJ" type="text" /><br />
		        <div class="yzm-box"> </div> 
		        </div>-->
		        <div class="btn"><input name="" type="submit" class="login-btn" value="" /></div>
		      </div>
		    </form>
			<div style="text-align: center">
				<a href="http://ics.fudan.edu.cn">新版网站正在试用中</a>
			</div>
	    </div>
	    <div id="footer"> </div>
	</div>
</body>
</html>
