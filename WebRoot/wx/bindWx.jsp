<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>微信绑定-评分系统</title>
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="../plugins/encryption/sha1.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function()
		{
			$("#password-form").bind("submit", function()
			{
				$("#password-form input[name='password']").val(hex_sha1($("#password-form input[name='password-input']").val()));
			});
		});
	</script>
	<style>
	<!--
	*{
	font-weight:bold;
	}
	input{
	width:100%;
	height:35px;
	resize:none;
	}
	#ft{
	position:fixed;
	bottom:20px;
	text-align:center;
	}
	-->
	</style>
</head>

<body>
	<div id="wrapper">
        <div id="contents">
        	<div id="password-div">
                <form id="password-form" action="http://ics.fudan.edu.cn/score/wx/bindWx.action" method="post">
                    <input type="hidden" name="wxOpenId" value="<%=request.getParameter("wxOpenId") %>">
					<p name="name">用户名</p>
					<p name="input"><input name="username" type="text"/></p>
					<p>密码</p>
					<p><input name="password-input" type="password"></p>
					<p><input name="password" type="hidden"></p>
					<p name="submit"><input style="height:40px" type="submit" value="绑&nbsp;定"></p>
                </form>
            </div>
        </div>
    </div>
</body>
</html>