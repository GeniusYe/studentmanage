$(function()
{
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChoose);
	
	$("#itemDiv").height(300);
	
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#header .global-subnav li[name='public-hearings-fill']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});	

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	var value = $(this).attr("value");
	if (value > 0)
	{
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
		$("#markDiv > div[name='loadingDiv']").removeClass("hidden");
		
		$("#itemDiv").attr("src", "queryPublicHearingsItemInfoAndTextToAllUsersAction?itemId=" + value);	
	}
	else
	{
		$("#itemDiv").attr("src", "");
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
	}
	$(this).addClass("current");
}

function subFrameLoaded()
{
	$("#markDiv").addClass("hidden");
	$("#itemDiv").removeClass("hidden");
	$("#markDiv > div[name='loadingDiv']").addClass("hidden"); 
	var $subDocument = document.getElementById('itemDiv').contentWindow;
	var $height = $subDocument.getTotalHeight();
	if ($height < 280)
	{
		$height = 280;
	}
	
	$("#itemDiv").height($height + 20);
} 