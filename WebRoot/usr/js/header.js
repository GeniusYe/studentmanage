$(function()
{
	$("#stateWrongDialog").dialog({
		autoOpen: false,
		width: 400,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$course_id = $("#course_id").val();
	$("input.courseId").val($course_id);
});

function jump($name, $data)
{
	window.location.href = $name + "?courseId=" + $course_id;
}

function showStateWrongDialog()
{
	$('#stateWrongDialog').dialog('open');
}