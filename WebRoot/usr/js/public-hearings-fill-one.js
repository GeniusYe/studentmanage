$(function()
{
	$("#saveErrorDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$(".item textarea, .item input").bind("keyup", function()
	{
		var $div = $(this).parent().parent().parent();
		$(".item[name=" + $div.attr("name") + "] > form input[name='submit']").removeClass("hidden"); 
	});

	$("form.itemForm").ajaxForm({
		type: 'post',
		dataType: 'json',
		beforeSubmit: function(formData, jqForm, options)
		{
			var $ifErr = false;
			var $errMsg = "<strong>有如下错误，请检查后重新提交。</strong><br />";
			var $temp;
			$temp = formData[2].value;
			if ($temp.length > 255){
				$ifErr = true;
				$errMsg += "建议不能超255个字符<br />";
			}
			if ($ifErr)
			{
				$("#saveErrorDialog").html($errMsg);
				$("#saveErrorDialog").dialog("open");
			}
		},
		success: function(data)
		{
			$(".item[name=" + data.infoTarget + "] input[name='value']").attr("value", data.value);
			var $unSubmittedSpan = $(".item[name=" + data.infoTarget + "] div[name='user'] span");
			var $submitButton = $(".item[name=" + data.infoTarget + "] div[name='submit'] > input[name='submit']");
			var $submitSuccessMsg = $(".item[name=" + data.infoTarget + "] div[name='submit'] > span");
			$unSubmittedSpan.addClass("hidden");
			$submitButton.addClass("hidden");
			$submitSuccessMsg.fadeIn(800).fadeOut();
		}
	});
	
	$(".valueSlider").each(function()
	{
		var $id = $(this).attr('name');
		var $input = $(".item[name=" + $id + "] div[name='valueDisplay'] input[name='value']");
		var $min = $(this).attr("minScore") * 1;
		var $max = $(this).attr("maxScore") * 1;
		var $current = $(this).attr("value") * 1;
		
		$(this).slider({
			range: "min",
			value: $current,
			min: $min,
			max: $max,
			slide: function(event, ui) {
				$input.val(ui.value);
				$(".item[name=" + $(this).attr("name") + "] div[name='submit'] input[name='submit']").removeClass("hidden");
			}
		});
	}); 
	parent.subFrameLoaded();
});	

function getTotalHeight(){
	return document.getElementById("theEnd").offsetTop;
}