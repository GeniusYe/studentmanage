function submit()
{
	var $newPassword = $("#password-div input[name='newPassword-input']").val();
	var $oldPassword = $("#password-div input[name='oldPassword-input']").val();
	if ($newPassword != $("#password-div input[name='newPassword-confirm']").val())
	{
		alert("两次输入的密码不一致");
	}
	else if ($newPassword.length < 5 || $newPassword.length > 16)
	{
		alert("密码长度应在5-16间");
	}
	else
	{
		$("#password-form input[name='oldPassword']").val(hex_sha1($oldPassword));
		$("#password-form input[name='newPassword']").val(hex_sha1($newPassword));
		$("#password-form").submit();
	}
}