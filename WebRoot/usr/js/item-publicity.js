$(function()
{
	$("#itemList li[name='allScore']").bind("click", itemChooseA);
	$("#itemList li[name='collectionItem']").bind("click", itemChooseC);
	$("#itemList li[name='publicHearingsItem']").bind("click", itemChooseP);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChooseI);

	$nowEditingId = null;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#header .global-subnav li[name='item-publicity']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
});

function itemChooseI()
{
	itemChoose.call(this, -1);
}

function itemChooseA()
{
	itemChoose.call(this, 0);
}
 
function itemChooseC()
{
	itemChoose.call(this, 1);
}
		
function itemChooseP()
{
	itemChoose.call(this, 2);
}
		
function itemChoose(type)
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$nowEditingId = $(this).attr("value");
	if (type == -1)
		$nowEditingType = null;
	else if (type == 0)
		$nowEditingType = 0;
	else if (type == 1)
		$nowEditingType = 1;
	else if (type == 2)
		$nowEditingType = 2;
	if ($nowEditingId > 0 || $nowEditingType == 0)
	{
		$.get(
			'queryScoreByItemId.action', 
			{type:$nowEditingType, itemId: $nowEditingId, courseId: $course_id}, 
			setResult, 
			'json'
		);
	}
	else
		setResult();
	$(this).addClass("current");
}

function setResult(data)
{
	var $scoreDiv = $("#itemDiv div[name='scoreDiv']");
	var $scoreSpan = $("#itemDiv div[name='scoreDiv'] span[name='score']");
	var $rankDiv = $("#itemDiv div[name='rankDiv']");
	var $rankSpan = $("#itemDiv div[name='rankDiv'] span[name='rank']");
	if (data == null)
	{
		$scoreDiv.addClass("hidden");
		$rankDiv.addClass("hidden");
		return;
	}
	$scoreDiv.removeClass("hidden");
	var score, rank, total;
	if (data.type == 0)
	{
		score = data.totalScore.value;
		rank = data.totalScore.rank;
	}
	else if (data.type == 1)
	{
		score = data.collectionItemScore.value;
		rank = data.collectionItemScore.rank;
	}
	else if (data.type == 2)
	{
		score = data.publicHearingsItemScore.value;
		rank = data.publicHearingsItemScore.rank;
	}
	total = data.totalUserSize;
	score = Math.round(score * 100)/100;
	$scoreSpan.text(score);
	if (rank != null)
	{
		$rankDiv.removeClass("hidden");
		if (((rank - 1) / total < 0.02) && rank > 1)
			var line = [['people in front of you', 1], ['people after you', 49]];
		else if (((rank - 1) / total > 0.98) && rank != total)
			var line = [['people in front of you', 49], ['people after you', 1]];
		else
			var line = [['people in front of you', rank - 1], ['people after you', total - rank]];
		$.jqplot('rankChart', [line], {
			//title:'pieRenderer ',//设置饼状图的标题
			seriesDefaults: 
			{
				fill: true, 
				showMarker: false, 
				shadow: false,
				renderer:$.jqplot.PieRenderer,
				rendererOptions:
				{
					diameter: undefined,
					padding: 20,
					sliceMargin: 9,
					fill:true,
					shadow:true,
					shadowOffset: 2,
					shadowDepth: 5,
					shadowAlpha: 0.07
				}
			},
			legend:
			{
				show: true,
				location: 's'
			}     
		});
	}
	else
		$rankDiv.addClass("hidden");
}