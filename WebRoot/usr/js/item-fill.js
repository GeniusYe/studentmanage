$(function()
{
	$("#saveErrorDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#uploadImgDialog").dialog({
		autoOpen: false,
		width: 400,
		resizable: false,
		buttons: {
			关闭: function() {
				$(this).dialog("close");
			}
		}
	});

	$(".item textarea, .item input").bind("keyup", function()
	{
		var $div = $(this).parent().parent().parent();
		$(".item[name=" + $div.attr("name") + "] > form input[name='submit']").removeClass("hidden"); 
	});

	$("form.itemForm").ajaxForm({
		url: 'fillCollectionItemInfo.action',
		type: 'post',
		dataType: 'json',
		beforeSubmit: function(formData, jqForm, options){
			var $ifErr = false;
			var $errMsg = "<strong>有如下错误，请检查后重新提交。</strong><br />";
			var $temp;
			$temp = formData[1].value;
			if ($temp.length > 255){
				$ifErr = true;
				$errMsg += "解释不能超255个字符<br />";
			}
			if ($ifErr)
			{
				$("#saveErrorDialog").html($errMsg);
				$("#saveErrorDialog").dialog("open");
				return false;
			}
		},
		success: function(data)
		{
			$(".item[name=" + data.itemId + "] textarea[name='text']").attr("value", data.text);
			$(".item[name=" + data.itemId + "] input[name='value']").attr("value", data.value);
			var $submitButton = $(".item[name=" + data.itemId + "] div[name='submit'] > input[name='submit']");
			var $submitSuccessMsg = $(".item[name=" + data.itemId + "] div[name='submit'] > span");
			$submitButton.addClass("hidden");
			$submitSuccessMsg.fadeIn(800).fadeOut();
		}
	});
	
	$("form.itemImageForm").ajaxForm({
		 url: "fillCollectionItemInfoImage.action",
		 type: "post",
		 dataType: "json",
		 beforeSubmit: function(formData, jqForm, options){
			var $ifErr = false;
			var $errMsg = "<strong>有如下错误，请检查后重新提交。</strong><br />";
			var $temp;
			$temp = $("form.itemImageForm > input[name='upload']").attr("value");
			if ($temp == null || $temp == "")
			{
				$ifErr = true;
				$errMsg += "您未选择图片<br />";
			}
			if ($ifErr)
			{
				$("#saveErrorDialog").html($errMsg);
				$("#saveErrorDialog").dialog("open");
				return false;
			}
			$("#uploadImgDialog div[name='loadingBar']").removeClass("hidden");
			$("form.itemImageForm").addClass("hidden");
		},
		success: function(data)
		{
			addOnePicFrame(data.itemId, data.id);
			alert("上传成功");
			$("#uploadImgDialog").dialog("close");
		}
	});
	
	$(".valueSlider").each(function()
	{
		var $id = $(this).attr('name');
		var $input = $(".item[name=" + $id + "] div[name='valueDisplay'] input[name='value']");
		var $min = $(this).attr("minScore") * 1;
		var $max = $(this).attr("maxScore") * 1;
		var $current = $(this).attr("value") * 1;
		
		$(this).slider({
			range: "min",
			value: $current,
			min: $min,
			max: $max,
			slide: function(event, ui) {
				$input.val(ui.value);
				$(".item[name=" + $(this).attr("name") + "] div[name='submit'] input[name='submit']").removeClass("hidden");
			}
		});
	});

	defaultForm($(".item textarea[name='text']"), "请输入您的解释");
	
	$("#contents").corner();

	$("#itemsDiv").each(function()
	{
		$(this).corner();
	});
	
	$("#header .global-subnav li[name='item-fill']").addClass("current");
});	

function showPic(itemId)
{
	$.ajax({
		type: "GET",
		url: "queryCollectionItemInfoImage.action",
		dataType: "json",
		data: {itemId: itemId},
		success: function(data)
		{
			$(".item[name=" + data.itemId + "] div[name='picDiv'] div.picFrame").remove();
			$.each(data.itemInfoImage, function(n, value){
				addOnePicFrame(data.itemId, value.id);
			});
			$(".item[name=" + data.itemId + "] div[name='picDiv']").slideDown();
		}
	});	
}

function addOnePicFrame(itemName, id)
{
	var $tempPicFrame = $("#picFrameModel").clone();
	$tempPicFrame.removeAttr("id");
	$tempPicFrame.removeClass("hidden");
	$tempPicFrame.prependTo($(".item[name=" + itemName + "] div[name='picDiv']"));
	$tempPicFrame.attr("name", id);
	$(".item[name=" + itemName + "] div[name='picDiv'] div[name=" + id + "] img").attr("src", "queryCollectionItemInfoOneImage.action?id=" + id);
	$(".item[name=" + itemName + "] div[name='picDiv'] div[name=" + id + "] img").attr("onClick", "javascript:window.open('queryCollectionItemInfoOneImage.action?id=" + id + "');");
	$(".item[name=" + itemName + "] div[name='picDiv'] div[name=" + id + "] a").attr("href", "javascript:delPic(" + id + ");");
}

function addPic(i)
{
	$("#uploadImgDialog div[name='loadingBar']").addClass("hidden");
	$("form.itemImageForm").removeClass("hidden");
	$("form.itemImageForm > input[name='itemId']").attr("value", i);
	$("#uploadImgDialog").dialog("open");
}

function delPic(i)
{
	$.ajax({
		url:"deleteCollectionItemInfoImage.action",
		type:"get",
		dataType:"json",
		data:{id:i},
		success:function(data)
		{
			$(".item div[name='picDiv'] div.picFrame[name='" + data.id + "']").remove();
			alert("删除成功");
		}
	});
}