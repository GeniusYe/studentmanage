<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据提交-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-only-frame.css" />
    <link rel="stylesheet" href="css/item-seperate-item.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-form/jquery.form.js" type="text/javascript" ></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
        <div id="contents">
        	<div id="itemsDiv">
                <s:iterator value="collectionItemList">
                	<s:if test="ifUserSubmitText == 1 || ifUserSubmitPic == 1 || ifCollection == 1">
	                    <div class="item" name="<s:property value='id' />">
	                    	<div name="title">
	                        	<h1>
	                      			<s:property value="itemId" /> : <s:property value="name" />
	                            </h1>
	                        </div>
	                        <div name="contents">
	                      		<s:property value="contents" />
	                        </div>
	                        <form class="itemForm">
	                        	<input type="hidden" name="itemId" value="<s:property value='id' />" />
	                        	<input type="hidden" name="courseId" class="courseId" />
	                            <s:if test="ifUserSubmitText==1">
	                                <div name="text">
	                                    <textarea name="text"><s:if test='collectedItemInfo != null'><s:property value='collectedItemInfo[0].text' /></s:if></textarea>
	                                </div>
	                            </s:if>
	                            <div name="value">
	                                <s:if test="ifCollection==1">
	                                	<s:if test='collectedItemInfo[0] != null'>
	                                    	<div class="valueSlider" name="<s:property value='id' />"  
	                                        	minScore="<s:property value='minScore' />" maxScore="<s:property value='maxScore' />"
	                            				value="<s:property value='collectedItemInfo[0].value' />">
	                            			</div>
	                            			<div name="valueDisplay">
                                				<input name="value" type="text" value="<s:property value='collectedItemInfo[0].value' />"/>
                                			</div>
	                            		</s:if><s:else>
	                            			<div class="valueSlider" name="<s:property value='id' />"  
	                                        	minScore="<s:property value='minScore' />" maxScore="<s:property value='maxScore' />"
	                            				value="<s:property value='defaultScore' />">
	                            			</div>
	                            			<div name="valueDisplay">
                                				<input name="value" type="text" value="<s:property value='defaultScore' />" />
                                			</div>
	                            		</s:else>
	                                </s:if>
	                            </div>
                                <s:if test="ifUserSubmitText == 1 || ifCollection == 1">
                                    <div name="submit">
                                        <input name="submit" type="submit" class="hidden" />
                                        <span name="submitSuccessMsg" class="hidden">提交成功</span>
                                    </div>
                                </s:if>
	                        </form>
                            <s:if test="ifUserSubmitPic==1">
                                <div class="clear">
                                </div>
                                <div name="showPicDiv">
                                	<a href="javascript:showPic(<s:property value='id' />);">这个项目可以提交图片。</a>
                                </div>
                                <div name="picDiv" class="hidden">
                                    <div class="picFrameNew">
                                    	<a href="javascript:addPic(<s:property value='id' />);">添加</a>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </s:if>
	                        <div class="clear">
	                        </div>
	                    </div>
                    </s:if>
                </s:iterator>
            </div>
        </div>
    </div>
    <div id="theEnd"></div>
    <%@ include file="footer.jsp" %>
    <div class="picFrame hidden" id="picFrameModel"><img /><br /><a>删除</a></div>
    <div id="saveErrorDialog" title="消息框" class="hidden">
		<span>失败</span>
	</div>
    <div id="uploadImgDialog" title="上传图片" class="hidden">
    	<span>上传过程中请不要关闭窗口，以免上传失败。如果长时间无反应，请按F5刷新后重试。<br />
        	请注意：只接受jpg,png,gif格式且大小在200KB以下在图片。</span>
        <div name="loadingBar" class="hidden">
        </div>
        <form enctype="multipart/form-data" class="itemImageForm">
            <input type="hidden" name="itemId" /> 
            <input name="upload" type="file" />
            <input type="submit" value="上传" />
        </form>
    </div>
    <script type="text/javascript" src="js/defaultForm.js"></script>
	<script type="text/javascript" src="js/item-fill.js"></script>
</body>
</html>