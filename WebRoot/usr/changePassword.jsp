<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>密码修改-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/password.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
	<script src="../plugins/encryption/sha1.js" type="text/javascript"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
        <div id="contents">
        	<div id="password-div">
	        	<form id="password-form" action="changeCurrentUserPassword.action" method="post">
	            	<table>
	                	<s:if test="errorCode != null">
	                	<tr name="error">
	                    	<td colspan="2">
	                        	<span>
	                            	<s:if test="errorCode == 2">
	                                	旧密码错误
	                                </s:if>
	                            </span>
	                        </td>
	                    </tr>
	                    </s:if>
	                	
	                	<tr>
	                    	<td name="name">
	                        	旧密码：
	                        </td>
	                        <td name="input">
	                			<input name="oldPassword-input" type="password" />
	                			<input name="oldPassword" type="hidden" />
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>
	                        	新密码：
	                        </td>
	                        <td>
	                        	<input name="newPassword-input" type="password" />
	                        	<input name="newPassword" type="hidden" />
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>
	                        	确认密码：
	                        </td>
	                        <td>
	                        	<input name="newPassword-confirm" type="password" />
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td colspan="2">
	                        	<div name="submit">
	                        		<a name="submit" href="javascript:submit();">修改</a>
	                            </div>
	                        </td>
	                    </tr>
	                </table>
				</form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
	<script type="text/javascript" src="js/change-password.js"></script>
</body>
</html>