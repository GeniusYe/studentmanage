<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="english">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目设定-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-optional-frame.css" />
    <link rel="stylesheet" href="css/item-publicity.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../plugins/jquery-jqplot/jquery.jqplot.min.css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="../plugins/jquery-jqplot/jquery.jqplot.min.js"></script>
	<script language="javascript" type="text/javascript" src="../plugins/jquery-jqplot/plugins/jqplot.pieRenderer.min.js"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
    	<div id="itemListDiv">
        	<div name="list">
        		<div name="headOperate">
        			<ul>
        				<li><a href="queryItemWhichShallBePublicized.action?courseId=<s:property value="courseId" />">成绩查询</a></li>
        				<li><a>课程评估</a></li>
        				<li><a href="../common/course_selection.jsp">返回</a></li>
        			</ul>
        		</div>
            	<div name="indexDiv">
                	<span>初始页面</span>
                </div>
            	<div name="limitDiv">
                    <ol id="itemList">
                    	<li name="allScore" > 
                            	总成绩
                        </li>
                        <s:iterator value="publicHearingsItemList" id="itemsP">
                        	<s:if test="type==0">
                            <li value=<s:property value="id" /> name="publicHearingsItem" > 
                                <s:property value="itemId" />&nbsp;:&nbsp;<s:property value="name" />
                            </li>
                            </s:if>
                        </s:iterator>
                        <s:iterator value="collectionItemList" id="itemsC">
                            <li value=<s:property value="id" /> name="collectionItem" > 
                                <s:property value="itemId" />&nbsp;:&nbsp;<s:property value="name" />
                            </li>
                        </s:iterator>
                    </ol>
                </div>
                <div name="control">
            		<a name="up" href="javascript:listUp();">up</a>
                	<a name="down" href="javascript:listDown();">down</a>
            	</div>
            </div>
        </div>
        <div id="contents">
            <div id="markDiv" class="hidden">
            </div>
            <div id="itemDiv">
                <div name="scoreAndRankDiv">
                	<div name="scoreDiv" class="hidden">
                    	<span>您的分数是：</span><span name="score"></span>
                    </div>
                    <div name="rankDiv" class="hidden">
                        <div id="rankChart">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
	<div id="saveErrorDialog" title="消息框" class="hidden">
		<span>失败</span>
	</div>
    <script src="js/list-move.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/item-publicity.js"></script>
</body>
</html>