<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据提交-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-seperate-item.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-form/jquery.form.js" type="text/javascript" ></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
	<div id="wrapper">
        <div id="contents">
        	<div id="itemsDiv">
        		<div name="item-contents">
                	<span><s:property value="publicHearingsItem.contents" /></span>
            	</div>
            	<s:iterator value="results" status="s">
                	<div class="item" name="<s:property value='results[#s.index][0]' />">
                    	<div name="user">
                    		<h1>	
                            	<s:property value='results[#s.index][1]' />
                            	<span class="noClass <s:if test='results[#s.index][2] != null'>hidden</s:if> <s:if test='publicHearingsItemInfoTextTarget[0] != null'>hidden</s:if>">
                            		(未评价)
                            	</span>
                            </h1>
                        </div>
                        <form class="itemForm" action="
                        	<s:if test='publicHearingsItem.type == 0'>
                        		fillPublicHearingsItemInfo.action
                        	</s:if><s:else>
                        	<s:if test="publicHearingsItem.type == 1">
                        		fillPublicHearingsItemInfoText.action
                        	</s:if></s:else>">
                        	<input type="hidden" name="itemId" value="<s:property value='itemId' />" />
                            <input type="hidden" name="infoTarget" value="<s:property value='results[#s.index][0]' />" />
                            <s:if test="publicHearingsItem.type == 1">
	                            <div name="text">
		                        	<textarea name="text"><s:property value='results[#s.index][2]' /></textarea>
		                        </div>
		                    </s:if>
		                    <s:else>
		                    <s:if test="publicHearingsItem.type == 0">
	                            <div name="checkValue">
	                            	<s:if test="results[#s.index][2] != null">
	                            		<div class="valueSlider" name="<s:property value='results[#s.index][0]' />" 
	                            			minScore="<s:property value='publicHearingsItem.minScore' />" maxScore="<s:property value='publicHearingsItem.maxScore' />"
	                            			value="<s:property value='results[#s.index][2]' />">
	                            		</div>
	                            		<div name="valueDisplay">
	                                		<input name="value" type="text" value="<s:property value='results[#s.index][2]' />"/>
	                                	</div>
	                            	</s:if>
	                            	<s:else>
	                            		<div class="valueSlider" name="<s:property value='results[#s.index][0]' />" 
	                            			minScore="<s:property value='publicHearingsItem.minScore' />" maxScore="<s:property value='publicHearingsItem.maxScore' />"
	                            			value="<s:property value='publicHearingsItem.defaultScore' />">
	                            		</div>
	                            		<div name="valueDisplay">
	                                		<input name="value" type="text" value="<s:property value='publicHearingsItem.defaultScore' />"/>
	                                	</div>
	                            	</s:else>
	                            </div>
	                        </s:if>
	                        </s:else>
                            <div name="submit">
                                <input name="submit" type="submit" class="noClass <s:if test='results[#s.index][2] != null'>hidden</s:if>" />
                                <span name="submitSuccessMsg" class="hidden">提交成功</span>
                            </div>
                        </form>
                        <div class="clear">
                        </div>
                    </div>
                </s:iterator>
            </div>
        </div>
    </div>
    <div id="theEnd"></div>
    <div id="saveErrorDialog" title="消息框" class="hidden">
		<span>失败</span>
	</div>
	<script type="text/javascript" src="js/public-hearings-fill-one.js"></script>
</body>
</html>