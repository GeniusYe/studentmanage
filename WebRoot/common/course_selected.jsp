<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>请等候跳转</title>
    <script type="text/javascript">
		window.location.href = "../<s:property value='redirectAddress' />";
	</script>
  </head>
  
  <body>
  	如果页面不发生跳转，您可以点击<a href="../<s:property value='redirectAddress' />">这里</a>手工跳转
  </body>
</html>
