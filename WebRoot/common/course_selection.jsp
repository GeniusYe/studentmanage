<%@ page language="java" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>课程选择</title>
<link href="css/course_selection.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="course">
		<div>
			<a href="identifyUserRole.action?courseId=7">
				<div class="item">
					<div>
						<h4>13 程序设计</h4>
						<h5>13 Programming</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;戴开宇老师</h6>
						<p>助教：&nbsp;刘骐硕，张栩晨，曾君</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=8">
				<div class="item">
					<div>
						<h4>13 程序设计</h4>
						<h5>13 Programming</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;陈荣华老师</h6>
						<p>助教：&nbsp;倪敏悦，张彭景，刘雅诺</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=1">
				<div class="item">
					<div>
						<h4>12 计算机系统基础下</h4>
						<h5>12 Introduction To Computer System II </h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;李弋老师</h6>
						<p>助教：&nbsp;叶家杰，周予维，付焜，徐天宇</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=13">
				<div class="item">
					<div>
						<h4>12 计算机系统基础下</h4>
						<h5>12 Introduction To Computer System II </h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;唐渊老师</h6>
						<p>助教：&nbsp;夏旭华，程义婷</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=2">
				<div class="item">
					<div>
						<h4>12 数据结构与算法分析</h4>
						<h5>12 Introduction To Data Structure And Algorithms </h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;郑骁庆老师</h6>
						<p>助教：&nbsp;刘骐硕，周予维</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=12">
				<div class="item">
					<div>
						<h4>12 面向对象程序设计</h4>
						<h5>12 Object-Oriented Programming </h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;外教</h6>
						<p>助教：&nbsp;陈易，马骁烊</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=3">
				<div class="item">
					<div>
						<h4>11 智能系统原理与设计</h4>
						<h5>11 ？？？？？？</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;郑骁庆老师</h6>
						<p>助教：&nbsp;朱成纯，韦进仕</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=4">
				<div class="item">
					<div>
						<h4>11 多媒体基础</h4>
						<h5>11 Introduction To Multimedia</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;刘新老师</h6>
						<p>助教：&nbsp;叶家杰，杨侃</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=5">
				<div class="item">
					<div>
						<h4>11 数据库设计</h4>
						<h5>11 Database Design</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;吴毅坚老师</h6>
						<p>助教：&nbsp;吴东，陈璐，树岸</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=6">
				<div class="item">
					<div>
						<h4>11 操作系统</h4>
						<h5>11 Operating System</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;李旻老师</h6>
						<p>助教：&nbsp;余时强，马晓凯，王欣</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=10">
				<div class="item">
					<div>
						<h4>11 科学的理论与实践</h4>
						<h5>11 Principle and Practice of Science</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;外教</h6>
						<p>助教：&nbsp;李榴丹，张彭景</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=11">
				<div class="item">
					<div>
						<h4>11 形式语言与计算理论</h4>
						<h5>11 Formal Language and Computing Theory</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;外教</h6>
						<p>助教：&nbsp;余时强，徐日</p>
					</div>
				</div>
			</a>
			<a href="identifyUserRole.action?courseId=9">
				<div class="item">
					<div>
						<h4>10  编译原理</h4>
						<h5>10 Compile</h5>
					</div>
					<div>
						<h6>任课教师：&nbsp;杨珉老师</h6>
						<p>助教：&nbsp;南雨宏，肖卫</p>
					</div>
				</div>
			</a>
			<a href="../usr/changePassword.jsp">
				<div class="item">
					<div>
						<h4>密码修改</h4>
						<h5>Password Modify</h5>
					</div>
					<div>
						<h6>请点击这里</h6>
						<p>......</p>
					</div>
				</div>
			</a>
		</div>
	</div>
</body>
</html>