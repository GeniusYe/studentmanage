<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>认定设定-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-setting.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
    	<div id="itemListDiv">
        	<div name="list">
            	<div name="indexDiv">
                	<span>初始页面</span>
                </div>
            	<div name="limitDiv">
                    <ol id="itemList">
                        <s:iterator value="collectionItemList" id="items">
                        <li value=<s:property value="id" />>
                            <s:property value="itemId" />&nbsp;:&nbsp;<s:property value="name" />
                        </li>
                        </s:iterator>
                    </ol>
                </div>
                <div name="control">
            		<a name="up" href="javascript:listUp();">up</a>
                	<a name="down" href="javascript:listDown();">down</a>
            	</div>
            </div>
        </div>
        <div id="contents">
            <div id="markDiv">
            </div>
            <div id="itemDiv" class="hidden">
                <form id="itemForm">
                    <div name="title">
                        <span></span>
                    </div>
                    <input type="hidden" name="id" />
                    <div name="userSet">
                        <div name="checkUsers">
                            <div class="users" name="1">
                                <a href="">
                                    10302010001 四字名字 按
                                </a>
                            </div>
                            <div class="users" name="1">
                                <a href="">
                                    10302010001 四字名字 按
                                </a>
                            </div>
                            <div class="users" name="1">
                                <a href="">
                                    10302010001 四字名字 按
                                </a>
                            </div>
                        </div>
                        <div name="allUsers">
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div name="submit">
                        <s:if test="systemState.state<=2">
                            <input type="submit" name="submit" value="提交更改" />
                        </s:if>
                        <s:else>
                            <span name="unsubmitable">系统状态不允许修改</span>
                        </s:else>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    <div id="saveSuccessDialog" title="消息框" class="hidden">
		<span>保存成功！</span>
	</div>
    <script src="js/list-move.js" type="text/javascript"></script> 
	<script type="text/javascript" src="js/item-check-setting.js"></script>
</body>
</html>