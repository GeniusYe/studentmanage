<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>项目设定-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-setting.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-form/jquery.form.js" type="text/javascript" ></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
    	<div id="itemListDiv">
        	<div name="list">
            	<div name="indexDiv">
                	<span>初始页面</span>
                </div>
            	<div name="limitDiv">
                    <ol id="itemList">
                        <s:iterator value="collectionItemList" id="items">
                        <li value="<s:property value="id" />"> 
                            <s:property value="itemId" />&nbsp;:&nbsp;<s:property value="name" />
                        </li>
                        </s:iterator>
                    </ol>
                </div>
                <div name="control">
            		<a name="up" href="javascript:listUp();">up</a>
                	<a name="down" href="javascript:listDown();">down</a>
                	<s:if test="systemState.state==1">
            			<a name="new" href="javascript:newItem();">&nbsp;新建</a>
                		<a name="del" href="javascript:delItem();">&nbsp;删除</a>
            		</s:if>
            	</div>
            </div>
        </div>
        <div id="contents">
            <div id="markDiv">
            </div>
            <div id="itemDiv" class="hidden">
                <form id="itemForm">
                    <input type="hidden" name="id" />
                    <input type="hidden" name="courseId" class="courseId" />
                    <div name="scoreSet">
                        <table>
                            <tr>
                                <td>
                                    题号：
                                </td>
                                <td name="input">
                                    <input type="text" name="itemId" />
                                </td>
                                <td>
                                    名称：
                                </td>
                                <td colspan="3">
                                    <input type="text" name="name" />
                                </td>
                            </tr>
                            <tr>
                                <td name="mark">
                                    最低分：
                                </td>
                                <td name="input">
                                    <input type="text" name="minScore" />
                                </td>
                                <td name="mark">
                                    最高分：
                                </td>
                                <td name="input">
                                    <input type="text" name="maxScore" />
                                </td>
                                <td name="mark">
                                    默认得分：
                                </td>
                                <td name="input">
                                    <input type="text" name="defaultScore" />
                                </td>
                            </tr>
                            <tr>
                            	<td colspan="6">
                            		以下项目用于多助教共同评定同一项目时使用，比如PJ由三位助教分别评定，为保证公平起见，可以将所有助教的打分调整成平均分和离散程度一致的调整分
                            	</td>
                            </tr>
                            <tr>
                                <td>
                                    平均分：
                                </td>
                                <td>
                                    <input type="text" name="averageScore" />
                                </td>
                                <td>
                                    离散程度：
                                </td>
                                <td>
                                    <input type="text" name="deltaScore" />
                                </td>
                                <td>
                                    比重：
                                </td>
                                <td>
                                    <input type="text" name="weight" />
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td>
                                    题目类型：
                                </td>
                                <td colspan="3">
                                    <select width="20px" name="type" enabled="false">
                                        <option value="0">直接得分</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div name="settingSet">
                        <table>
                            <tr style="display:none;">
                                <td>
                                    是否允许学生提交预期得分
                                </td>
                                <td>
                                    <input type="checkbox" name="ifCollection" />
                                </td>
                                <td>
                                    是否需要助教认定
                                </td>
                                <td>
                                    <input type="checkbox" name="ifCheck" checked="true" />
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td name="mark">
                                    是否允许用户提交文字解释
                                </td>
                                <td name="check">
                                    <input type="checkbox" name="ifUserSubmitText" />
                                </td>
                                <td name="mark">
                                    是否允许用户提交图片解释（功能暂无）
                                </td>
                                <td name="check">
                                    <input type="checkbox" name="ifUserSubmitPic" />
                                </td>
                            </tr>
                            
                            <tr style="display:none;">
                                <td name="mark">
                                    是否允许用户提交代码（功能暂无）
                                </td>
                                <td name="check">
                                    <input type="checkbox" name="ifUserSubmitCode" />
                                </td>
                                <td name="mark">
                                    用户Git版本库地址
                                </td>
                                <td>
                                    <input type="text" name="codeDirectory" />
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td name="startFillingTime">
                                    开始提交时间
                                </td>
                                <td name="check">
                                    <input type="text" name="startFillingTime" />
                                </td>
                                <td name="endFillingTime">
                                    结束提交时间
                                </td>
                                <td name="check">
                                    <input type="text" name="endFillingTime" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div name="contents" style="display:none;">
                        用户提示信息：
                        <textarea name="contents"></textarea>
                    </div>
                    <div name="checkRequirement" style="display:none;">
                        认定提示信息：
                        <textarea name="checkRequirement"></textarea>
                    </div>
                    <div class="clear"></div>
                    <div name="submit">
                        <s:if test="systemState.systemType == 1">
                            <input name="submit" type="submit" value="提交更改" />
                            <a id="calculate-trigger" name="calculate">统计得分</a>
                        </s:if>
                        <s:elseif test="systemState.systemType == 0">
                        	<s:if test="systemState.state == 1">
                            	<input name="submit" type="submit" value="提交更改" />
                           	</s:if>
                           	<s:else>
                            	<span name="unsubmitable">系统状态不允许修改</span>
                        	</s:else>
                        </s:elseif>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    <div id="saveSuccessDialog" title="消息框" class="hidden">
		<span>操作已成功！</span>
	</div>
    <div id="calculatingDialog" title="消息框" class="hidden">
		<div name="loadingDiv">
		    <div name="word">
		        <span>统计中，请稍候。</span>
		    </div>
		    <div name="loadingBar">
		    </div>
		</div>
	</div>
	<div id="saveErrorDialog" title="消息框" class="hidden">
		<span>失败</span>
	</div>
    <script type="text/javascript" src="js/list-move.js"></script>
	<script type="text/javascript" src="js/item-setting.js"></script>
</body>
</html>