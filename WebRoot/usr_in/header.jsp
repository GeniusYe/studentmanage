<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="stylesheet" href="css/common.css" />
<link rel="stylesheet" href="css/usr_in_header.css" /> 
<script type="text/javascript" src="js/header.js"></script>
<input type="hidden" id="course_id" value="<s:property value='courseId' />" />
<div id="header">
	<ul>
		<li name="system-setting" class="global-nav-master">
			<a href="javascript:jump('querySystemState.action');">
				<strong>系统设定</strong>
            </a>
			<ul class="global-subnav">
            	<li name="systemstate-setting">
                	<a href="javascript:jump('querySystemState.action');">
						运行阶段
					</a>
                </li>
				<li name="item-setting">
					<a href="javascript:jump('queryCollectionItemForEdit.action');">
						项目设定
					</a>
				</li>
				<li name="public-hearings-item-setting">
					<a href="javascript:jump('queryPublicHearingsItemForEdit.action');">
						互评项目设定
					</a>
				</li>
				<li name="item-check-setting">
					<a href="javascript:jump('queryCollectionItemForCheckSet.action');">
						认定设定
					</a>
				</li>
                <li name="item-publicity-setting">
                	<a href="javascript:jump('queryCollectionItemForPublicitySet.action');">
						公示设定
					</a>
                </li>
			</ul>
		</li>
        <li name="item-check" class="global-nav-master">
			<a href="javascript:jump('queryCollectionItemForCheckByCurrentUser.action');">
				<strong>成绩认定</strong>
            </a>
		</li>
		<li name="system-moniter" class="global-nav-master">
			<a href="javascript:jump('queryCollectionItemForMonitor.action');">
				<strong>状态监控</strong>
            </a>
            <ul class="global-subnav">
            	<li name="score-monitor">
                	<a href="javascript:jump('queryCollectionItemForMonitor.action');">
						分数监控
					</a>
                </li>
            	<li name="force-check">
                	<a href="javascript:jump('queryCollectionItemForCheckForce.action');">
						强制分数
					</a>
                </li>
            </ul>
		</li>
		<li name="result-create" class="global-nav-master">
			<a href="
				<s:if test='systemState.systemType == 1 || systemState.state == 6 || systemState.state == 7'>
					javascript:jump('generateReport.action');
				</s:if><s:else>
					javascript:showStateWrongDialog();
				</s:else>">
				<strong>生成报表</strong>
            </a>
		</li>
    </ul>
</div>
<div class="clear"></div>
<div id="stateWrongDialog" title="消息框" class="hidden">
	<span>系统状态不允许此操作</span>
</div>
<div id="standardSystemNoFunctionDialog" title="消息框" class="hidden">
	<span>此版本不提供此功能，如需要此功能，请联系提供商</span>
</div>
