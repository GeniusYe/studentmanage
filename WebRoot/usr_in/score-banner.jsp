<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>分数监控-评分系统</title>
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/item-setting.css" />
    <link rel="stylesheet" type="text/css" href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" />
    <link rel="stylesheet" type="text/css" href="../plugins/jquery-jqplot/jquery.jqplot.min.css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
    <script language="javascript" type="text/javascript" src="../plugins/jquery-jqplot/jquery.jqplot.min.js"></script>
	<script language="javascript" type="text/javascript" src="../plugins/jquery-jqplot/plugins/jqplot.barRenderer.min.js"></script>
	<script language="javascript" type="text/javascript" src="../plugins/jquery-jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
</head>

<body>
    <%@ include file="header.jsp" %>
	<div id="wrapper">
    	<div id="itemListDiv">
        	<div name="list">
            	<div name="indexDiv">
                	<span>初始页面</span>
                </div>
            	<div name="limitDiv">
                    <ol id="itemList">
                        <s:iterator value="collectionItemList" id="items">
                        <li value=<s:property value="id" />>
                            <s:property value="itemId" />&nbsp;:&nbsp;<s:property value="name" />
                        </li>
                        </s:iterator>
                    </ol>
                </div>
                <div name="control">
            		<a name="up" href="javascript:listUp();">up</a>
                	<a name="down" href="javascript:listDown();">down</a>
            	</div>
            </div>
        </div>
        <div id="contents">
            <div id="markDiv">
            </div>
        	<div id="itemDiv" class="hidden">
	        	<form id="itemForm">
                    <div name="title">
                        <span></span>
                    </div>
                    <input type="hidden" name="id" />
                    <div name="submit">
                        <s:if test="systemState.systemType == 1">
                            <a id="calculate-trigger" name="calculate">统计得分</a>
                        </s:if>
                        <div class="clear"></div>
                    </div>
                    <div id="graphDiv">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    <div id="saveSuccessDialog" title="消息框" class="hidden">
		<span>操作已成功！</span>
	</div>
    <div id="calculatingDialog" title="消息框" class="hidden">
		<div name="loadingDiv">
		    <div name="word">
		        <span>统计中，请稍候。</span>
		    </div>
		    <div name="loadingBar">
		    </div>
		</div>
	</div>
	<script type="text/javascript" src="js/list-move.js"></script>
	<script type="text/javascript" src="js/score-banner.js"></script>
</body>
</html>