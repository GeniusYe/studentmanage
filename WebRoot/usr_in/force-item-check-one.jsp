<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据提交-评分系统</title>
    <link rel="stylesheet" href="css/common.css" />
    <link rel="stylesheet" href="css/item-seperate-item.css" />
    <link href="../plugins/jquery-ui/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" type="text/css" />
	<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-form/jquery.form.js" type="text/javascript" ></script>
    <script src="../plugins/jquery-ui/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="../plugins/jquery-corner/jquery.corner.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
	<div id="wrapper">
        <div id="contents">
           	<div id="calculate-div">
				<a id="calculate-trigger" name="calculate">统计得分</a>
				<div class="clear"></div>
			</div>
        	<div id="itemsDiv">
            	<div name="item-checkRequirement">
                	<span><s:property value="collectionItem.checkRequirement" /></span>
            	</div>
                <div class="clear"></div>
                <s:iterator value="results" status="s">
                	<div class="item" name="<s:property value='results[#s.index][0]' />">
                    	<div name="user">
                    		<h1>	
                            	<s:property value='results[#s.index][1]' />
                            	<span class="<s:if test="results[#s.index][2] != null">hidden</s:if>" name="notSubmit">
                            		(未统一认定)
                            	</span>
                            </h1>
                        </div>
                        <s:if test="results[#s.index][4] != null">
	                        <div name="userSubmittedText">
	                        	用户的解释：<s:property value='results[#s.index][4]' />
	                        </div>
                        </s:if>
                        <s:if test="results[#s.index][5] != null">
	                        <div name="userSubmittedValue">
								用户期望得分：<s:property value='results[#s.index][5]' />
	                        </div>
                        </s:if>
                        <s:if test="collectionItemInfo[0].images != null">
                            <div class="clear">
                            </div>
                            <div name="showPicDiv">
                                <a href="javascript:showPic(<s:property value='itemId' />, <s:property value='id' />);">用户提交了图片。</a>
                            </div>
                            <div name="picDiv" class="hidden">
                                <div class="clear">
                                </div>
                            </div>
                        </s:if>
                        <s:if test="collectionItem.ifUserSubmitCode == true">
                            <div class="clear">
                            </div>
                            <div name="showCodeDiv">
                                <a href="javascript:showCode('overview', '', <s:property value='itemId' />, <s:property value='results[#s.index][0]' />);">该项目有关联的代码。</a>
                            </div>
                        </s:if>
                        <form class="itemForm" >
                        	<input type="hidden" name="itemId" value="<s:property value='itemId' />" />
                            <input type="hidden" name="infoTarget" value="<s:property value='results[#s.index][0]' />" />
                            <div name="checkValue">
                            	<s:if test="results[#s.index][2] != null">
                            		<div class="valueSlider"
                            			minScore="<s:property value='collectionItem.minScore' />" maxScore="<s:property value='collectionItem.maxScore' />"
                            			value="<s:property value='results[#s.index][2]' />">
                            		</div>
                            		<div name="valueDisplay">
                                		<input name="value" type="text" class="valueInput" value="<s:property value='results[#s.index][2]' />"/>
                                	</div>
                            	</s:if>
                            	<s:else>
                            		<div class="valueSlider"
                            			minScore="<s:property value='collectionItem.minScore' />" maxScore="<s:property value='collectionItem.maxScore' />"
                            			value="<s:property value='collectionItem.defaultScore' />">
                            		</div>
                            		<div name="valueDisplay">
                                		<input name="value" type="text" class="valueInput" value="<s:property value='collectionItem.defaultScore' />"/>
                                	</div>
                            	</s:else>
                            </div>
                            <div name="submit">
                                <input name="submit" type="submit" class="<s:if test='results[#s.index][2] != null'>hidden</s:if>" />
                                <a name="delete" href="javascript:deleteItem(<s:property value='itemId' />, <s:property value="results[#s.index][0]" />);" class="<s:if test='results[#s.index][2] != null'></s:if><s:else>hidden</s:else>" >删除</a>
                                <span name="submitSuccessMsg" class="hidden">提交成功</span>
                            </div>
                        </form>
                        <div class="clear">
                        </div>
                    </div>
                </s:iterator>
            </div>
        </div>
    </div>
    <div id="theEnd"></div>
    <div class="picFrame" id="picFrameModel" class="hidden"><img /></div>
    <script type="text/javascript">
    	$itemId = <s:property value="itemId" />;
    </script>
	<script type="text/javascript" src="js/force-item-check-one.js"></script>
</body>
</html>