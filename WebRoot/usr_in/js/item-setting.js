$(function()
{
	$nowEditingId = null;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChoose);
	
	$("#saveSuccessDialog, #saveErrorDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});

	$("#calculatingDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false
	});
	
	$("#itemForm").ajaxForm({
		url: 'createCollectionItem.action',
		type: 'GET',
		dataType: 'json',
		beforeSubmit: function(){
			var $ifErr = false;
			var $errMsg = "<strong>有如下错误，请检查后重新提交。</strong><br />";
			var $temp;
			$temp = $("#itemForm input[name='name']").attr("value");
			if ($temp == null || $temp.length == 0){
				$ifErr = true;
				$errMsg += "名称不能为空<br />";
			}
			var $tempMin = $("#itemForm input[name='minScore']").attr("value");
			var $tempMax = $("#itemForm input[name='maxScore']").attr("value");
			var $tempDefault = $("#itemForm input[name='defaultScore']").attr("value");
			if ($tempMin == null || $tempMin.length == 0 || $tempMax == null || $tempMax.length == 0 || $tempDefault == null || $tempDefault.length == 0){
				$ifErr = true;
				$errMsg += "最高分最低分和默认得分不能为空<br />";
			}
			var $ifCollection = $("#itemForm input[name='ifCollection']").attr("checked");
			var $ifCheck = $("#itemForm input[name='ifCheck']").attr("checked");
			if (!($ifCollection == "checked" || $ifCheck == "checked")) {
				$ifErr = true;
				$errMsg += "是否认定和是否收集数据至少要选中一项<br />";
			}
			$temp = $("#itemForm textarea[name='contents']").attr("value");
			if ($temp.length > 255){
				$ifErr = true;
				$errMsg += "用户提示信息不能超255个字符<br />";
			}
			$temp = $("#itemForm textarea[name='checkRequirement']").attr("value");
			if ($temp.length > 255){
				$ifErr = true;
				$errMsg += "认定提示信息不能超255个字符<br />";
			}
			if ($ifErr)
			{
				$("#saveErrorDialog").html($errMsg);
				$("#saveErrorDialog").dialog("open");
				return false;
			}
		},
		success: function(data)
		{
			if (!($nowEditingId > 0))
			{
				$("#itemList").append("<li value=" + data.collectionItem.id + ">" + data.collectionItem.itemId + " : " + data.collectionItem.name + "</li>");
				$("#itemList li:last").bind("click", itemChoose);
			}
			else
			{
				$("#itemList li[value=" + data.collectionItem.id + "]").replaceWith("<li value=" + data.collectionItem.id + ">" + data.collectionItem.itemId + " : " + data.collectionItem.name + "</li>");
				$("#itemList li[value=" + data.collectionItem.id + "]").bind("click", itemChoose);
			}
			$("#saveSuccessDialog").dialog("open");
			setCollectionItem(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			$("#saveErrorDialog").dialog("open");
		}
	});

	$("#calculate-trigger").bind("click", function(){
		$("#calculatingDialog").dialog("open");
		$.ajax({
			url:"calculateCollectionItemScoreById.action",
			type:"get",
			datatype:"json",
			data:{itemId: $nowEditingId},
			success:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveSuccessDialog").dialog("open");
			},
			error:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveErrorDialog").dialog("open");
			}
		});
	});
	
	//header current
	$("#header .global-nav-master[name='system-setting']").addClass("current");
	$("#header li.global-nav-master.current li[name='item-setting']").addClass("current");
	
	//frame corder
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$nowEditingId = $(this).attr("value");
	if ($nowEditingId > 0)
	{
		$.get(
			'queryCollectionItemById.action', 
			{Id: $nowEditingId}, 
			setCollectionItem, 
			'json'
		);
	}
	else
		setCollectionItem({collectionItem:{id:null}});
	$(this).addClass("current");
}

function newItem()
{
	$("#itemList li").removeClass("current");
	var newItem = {};
	var collectionItem = {};
	collectionItem.id = 0;
	collectionItem.itemId = null;
	collectionItem.name = null;
	collectionItem.type = 0;
	collectionItem.minScore = null;
	collectionItem.maxScore = null;
	collectionItem.defaultScore = null;
	collectionItem.averageScore = null;
	collectionItem.deltaScore = null;
	collectionItem.weight = null;
	collectionItem.ifCollection = false;
	collectionItem.ifCheck = true;
	collectionItem.ifUserSubmitText = false;
	collectionItem.ifUserSubmitPic = false;
	collectionItem.ifUserSubmitCode = false;
	collectionItem.contents = null;
	collectionItem.checkRequirement = null;
	collectionItem.codeDirectory = "";
	newItem.collectionItem = collectionItem;
	setCollectionItem(newItem);
}

function delItem()
{
	if (!($nowEditingId > 0))
	{
		alert("请选择项目");
	}
	else
	{
		$.get(
			'deleteCollectionItemById.action', 
			{itemId: $nowEditingId}, 
			function(data, textStatus){
				$("#itemList li[value=" + data.itemId + "]").remove();
				$nowEditingId = 0;
				var nullData = {};
				var item = {};
				item.id = 0;
				nullData.collectionItem = item;
				setCollectionItem(nullData);
				$("#saveSuccessDialog").dialog("open");
			}, 
			'json'
		);
	}
}

function setCollectionItem(data)
{
	$nowEditingId = data.collectionItem.id;
	if ($nowEditingId != null)
	{
		$("#markDiv").addClass("hidden");
		$("#itemDiv").removeClass("hidden");
	}
	else
	{
		
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
		return;
	}
	$("#itemDiv input[name='id']").attr("value", data.collectionItem.id);
	$("#itemDiv input[name='itemId']").attr("value", data.collectionItem.itemId);
	$("#itemDiv input[name='name']").attr("value", data.collectionItem.name);
	$("#itemDiv input[name='minScore']").attr("value", data.collectionItem.minScore);
	$("#itemDiv input[name='maxScore']").attr("value", data.collectionItem.maxScore);
	$("#itemDiv input[name='defaultScore']").attr("value", data.collectionItem.defaultScore);
	$("#itemDiv input[name='averageScore']").attr("value", data.collectionItem.averageScore);
	$("#itemDiv input[name='deltaScore']").attr("value", data.collectionItem.deltaScore);
	$("#itemDiv input[name='weight']").attr("value", data.collectionItem.weight);
	$("#itemDiv input[name='ifCollection']").attr("checked", data.collectionItem.ifCollection);
	$("#itemDiv input[name='ifCheck']").attr("checked", data.collectionItem.ifCheck);
	$("#itemDiv input[name='ifUserSubmitText']").attr("checked", data.collectionItem.ifUserSubmitText);
	$("#itemDiv input[name='ifUserSubmitPic']").attr("checked", data.collectionItem.ifUserSubmitPic);
	$("#itemDiv input[name='ifUserSubmitCode']").attr("checked", data.collectionItem.ifUserSubmitCode);
	$("#itemDiv input[name='codeDirectory']").attr("value", data.collectionItem.codeDirectory);
	$("#itemDiv select[name='type']").attr("value", data.collectionItem.type);
	$("#itemDiv textarea[name='contents']").attr("value", data.collectionItem.contents);
	$("#itemDiv textarea[name='checkRequirement']").attr("value", data.collectionItem.checkRequirement);
	$("#itemDiv input[name='startFillingTime']").attr("value", data.collectionItem.startFillingTime);
	$("#itemDiv input[name='endFillingTime']").attr("value", data.collectionItem.endFillingTime);
}