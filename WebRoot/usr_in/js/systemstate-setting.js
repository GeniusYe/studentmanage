$(function()
{
	$nowEditingId = 0;
	
	$("#contents").corner();

	$("#saveSuccessDialog").dialog({
		modal: true,
		resizable: false,
		autoOpen: false,
		buttons: {
			确定: function() {
				$("#saveSucess").dialog('close');
			}
		}
	});
	$('#warningDialog').dialog({
		autoOpen: false,
		width: 400,
		resizable: false,
		buttons: {
			是: function() {
				$(this).dialog("close");
				$("#contents > div[name='submit']").addClass("hidden");
				$("#contents > div[name='loadingDiv']").removeClass("hidden");
				var $act = "changeSystemStateToNextStep.action";
				$.ajax({
					type: "GET",
					url: $act, 
					dataType: "json",
					success: function(data, textStatus){
						window.location.reload(true);
						/*var $h1 = $("#contents > div[name='state'] > h1");
						if (data.systemState.enabled == 1)
						{
							$("#contents > div[name='submit']").removeClass("hidden");
							$("#contents > div[name='loadingDiv']").addClass("hidden");
						}
						if (data.systemState.state == 2)
						{
							$h1.text("数据收集阶段");
						}
						else if (data.systemState.state == 3)
						{
							$h1.text("得分认定阶段");
						}
						else if (data.systemState.state == 4)
						{
							$h1.text("保留阶段");
						}
						else if (data.systemState.state == 5)
						{
							$h1.text("保留阶段");
						}
						else if (data.systemState.state == 6)
						{
							$h1.text("公示阶段");
							$("#contents > div[name='submit'] > a[name='submit']").replaceWith("<span name='unsubmitable'>系统运行已经结束</span>");
						}
						$("#saveSuccessDialog").open("open");*/
					}, 
					error: function(XMLHttpRequest, textStatus, errorThrown){
						alert("操作不成功");
					}
				});
			},
			否: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#contents a[name='submit']").bind("click", function()
	{
		$('#warningDialog').dialog('open');
		return false;
	});

	$("#header .global-nav-master[name='system-setting']").addClass("current");
	$("#header li.global-nav-master.current li[name='systemstate-setting']").addClass("current");
});