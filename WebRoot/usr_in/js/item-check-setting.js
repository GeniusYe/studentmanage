$(function()
{
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChoose);
	
	$("#saveSuccessDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$.ajax({
		type:"GET", 
		dataType:"json",
		url:"queryTaUsers.action",
		data: {courseId: $course_id},
		success: function(data, textStatus)
		{
			users = data.users;
			var $allUsersDiv = $("#itemDiv div[name='allUsers']");
			$allUsersDiv.children().remove();
			$("#itemDiv div[name='checkUsers']").children().remove();
			$.each(data.users, function()
			{
				var $element = $("<div class='users' name='" + this.id + "'><a href=''>" + this.username + " " + this.name + "</a></div>");
				$element.bind("click", moveUserToCheck);
				$allUsersDiv.append($element);
			});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert("用户列表加载失败");
		}

		
	});

	$("#itemForm").submit(function()
	{
		var $act = "createCollectionItemCheck.action?id=" + $("#itemDiv input[name='id']").attr("value");
		$("#itemDiv div[name='checkUsers']").children().each(function()
		{
			$act = $act + "&checkOperatorId=" + $(this).attr("name");
		});
		$.ajax({
			type: "GET",
			url: $act, 
			dataType: "json",
			success: function(data, textStatus){
				$("#saveSuccessDialog").dialog("open");
				setCollectionItem(data);
			}, 
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("操作不成功");
			}
		});
		return false;
	});
	
	$nowEditingId = 0;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#header .global-nav-master[name='system-setting']").addClass("current");
	$("#header li.global-nav-master.current li[name='item-check-setting']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});

function moveUserToCheck()
{
	var $element = $(this);
	var $checkUsersDiv = $("#itemDiv div[name='checkUsers']");
	$element.unbind("click");
	$element.bind("click", moveUserToUncheck);
	$checkUsersDiv.append($element);
	return false;
}

function moveUserToUncheck()
{
	var $element = $(this);
	var $allUsersDiv = $("#itemDiv div[name='allUsers']");
	$element.unbind("click");
	$element.bind("click", moveUserToCheck);
	$allUsersDiv.append($element);
	return false;
}

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$nowEditingId = $(this).attr("value");
	if ($nowEditingId > 0)
	{
		$.get(
			'queryCollectionItemWithCheckOperatorsById.action', 
			{Id: $nowEditingId}, 
			setCollectionItem, 
			'json'
		);
	}
	else
		setCollectionItem();
	$(this).addClass("current");
}

function setCollectionItem(data)
{
	if ($nowEditingId != null)
	{
		$("#markDiv").addClass("hidden");
		$("#itemDiv").removeClass("hidden");
		}
		else
		{
			$("#itemDiv").addClass("hidden");
			$("#markDiv").removeClass("hidden");
			return;
		}
		$("#itemDiv input[name='id']").attr("value", data.collectionItem.id);
		$("#itemDiv div[name='title'] span").text(data.collectionItem.itemId + " : " + data.collectionItem.name);
		$("#itemDiv div[name='checkUsers']").children().each(function()
		{
			moveUserToUncheck.apply($(this));
		});
		$.each(data.collectionItem.checkOperator, function()
		{
			$("#itemDiv div.users[name=" + this.id + "]").each(function(){
				moveUserToCheck.apply($(this));
			});
		});
	}