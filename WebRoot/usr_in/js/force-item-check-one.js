$(function()
{
	$(".item textarea, .item input").bind("keyup", function()
	{
		var $div = $(this).parent().parent();
		$(".item[name=" + $div.attr("name") + "] > form input[name='submit']").removeClass("hidden"); 
	});

	$("form.itemForm").ajaxForm({
		url: 'fillCollectionItemInfoCheckForce.action',
		type: 'post',
		dataType: 'json',
		beforeSubmit: function(){},
		success: function(data)
		{
			$(".item[name=" + data.infoTarget + "] input[name='value']").attr("value", data.value);
			var $unSubmittedSpan = $(".item[name=" + data.infoTarget + "] div[name='user'] span[name='notSubmit']");
			var $submitButton = $(".item[name=" + data.infoTarget + "] div[name='submit'] > input[name='submit']");
			var $submitSuccessMsg = $(".item[name=" + data.infoTarget + "] div[name='submit'] > span");
			var $deleteButton = $(".item[name=" + data.infoTarget + "] div[name='submit'] a[name='delete']");
			$unSubmittedSpan.addClass("hidden");
			$submitButton.addClass("hidden");
			$submitSuccessMsg.fadeIn(800).fadeOut();
			$deleteButton.removeClass("hidden");
		}
	});
	
	$(".valueSlider").each(function()
	{
		var $id = $(this).parent().parent().parent().attr('name');
		var $input = $(".item[name=" + $id + "] div[name='valueDisplay'] input[name='value']");
		var $min = $(this).attr("minScore") * 1;
		var $max = $(this).attr("maxScore") * 2;
		var $current = $(this).attr("value") * 1;
		
		$(this).slider({
			range: "min",
			value: $current,
			min: $min,
			max: $max,
			slide: function(event, ui) {
				$input.val(ui.value);
				$(".item[name=" + $id + "] div[name='submit'] input[name='submit']").removeClass("hidden");
				$(".item[name=" + $id + "] div[name='submit'] a[name='delete']").addClass("hidden");
			},
			change: slider_change
		});
	});
	
	$(".valueInput").bind("blur", function()
	{
		var $id = $(this).parent().parent().parent().parent().attr('name');
		var $current = $(this).attr("value") * 1;
		var $slider = $(".item[name=" + $id + "] div.valueSlider");
		$slider.slider("value", $current);
		$(".item[name=" + $id + "] div[name='submit'] input[name='submit']").removeClass("hidden");
		$(".item[name=" + $id + "] div[name='submit'] a[name='delete']").addClass("hidden");
	});
	
	$("#calculate-trigger").bind("click", function()
	{
		parent.calculate($itemId);
	});
	
	parent.subFrameLoaded();
});	

function slider_change()
{
	$(".item[name=" + $(this).attr("name") + "] div[name='submit'] input[name='submit']").trigger("click");
}

function showPic(itemId, userId)
{
	$.ajax({
		type: "GET",
		url: "queryCollectionItemInfoImage.action",
		dataType: "json",
		data: {itemId: itemId, userId: userId},
		success: function(data)
		{
			$(".item[name=" + data.userId + "] div[name='picDiv'] div.picFrame").remove();
			$.each(data.itemInfoImage, function(n, value){
				addOnePicFrame(data.userId, value.id);
			});
			$(".item[name=" + data.userId + "] div[name='picDiv']").removeClass("hidden");
		}
	});	
}

function deleteItem(itemId, userId)
{
	$.ajax({
		url: "deleteCollectionItemInfoCheckForce.action",
		data: {itemId: itemId, infoTargetId: userId},
		type: "GET",
		dataType: "json",
		success: function(data)
		{
			var $id = data.infoTargetId;
			var $input = $(".item[name=" + $id + "] div[name='valueDisplay'] input[name='value']");
			var $slider = $(".valueSlider[name='" + $id + "']");
			$(".item[name=" + $id + "] input[name='value']").removeAttr("value");
			var $unSubmittedSpan = $(".item[name=" + $id + "] div[name='user'] span[name='notSubmit']");
			var $submitButton = $(".item[name=" + $id + "] div[name='submit'] > input[name='submit']");
			var $submitSuccessMsg = $(".item[name=" + $id + "] div[name='submit'] > span");
			var $deleteButton = $(".item[name=" + $id + "] div[name='submit'] a[name='delete']");
			$unSubmittedSpan.removeClass("hidden");
			$submitButton.addClass("hidden");
			$deleteButton.addClass("hidden");
			$slider.slider({
				change: function() { }
			});
			$slider.slider({
				value: null
			});
			$slider.slider({
				change: slider_change
			});
			alert("删除成功");
		}
	});
}

function showCode(version, path, itemId, userId)
{
	parent.showCode(version, path, itemId, userId);
}

function addOnePicFrame(itemName, id)
{
	var $tempPicFrame = $("#picFrameModel").clone();
	$tempPicFrame.removeAttr("id");
	$tempPicFrame.removeClass("hidden");
	$tempPicFrame.prependTo($(".item[name=" + itemName + "] div[name='picDiv']"));
	$tempPicFrame.attr("name", id);
	$(".item[name=" + itemName + "] div[name='picDiv'] div[name=" + id + "] img").attr("src", "queryCollectionItemInfoOneImage.action?id=" + id);
	$(".item[name=" + itemName + "] div[name='picDiv'] div[name=" + id + "] img").attr("onClick", "javascript:window.open('queryCollectionItemInfoOneImage.action?id=" + id + "');");
}

function getTotalHeight(){
	return document.getElementById("theEnd").offsetTop;
}