$(function()
{
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").addClass("current");

	//header current
	$("#header .global-nav-master[name='system-moniter']").addClass("current");
	$("#header li.global-nav-master.current li[name='force-check']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
	
	$("#saveSuccessDialog, #saveErrorDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});

	$("#calculatingDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false
	});
});

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	var value = $(this).attr("value");
	if (value > 0)
	{
		$("#itemDiv").attr("src",  "queryCollectionItemInfoCheckToAllUsersForce.action?itemId=" + value);
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
		$("#markDiv > div[name='loadingDiv']").removeClass("hidden");
	}
	else
	{
		$("#itemDiv").attr("src", "");
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
	}
	$(this).addClass("current");
}

function calculate($itemId)
{
	$("#calculatingDialog").dialog("open");
	$.ajax({
		url:"calculateCollectionItemScoreById.action",
		type:"get",
		datatype:"json",
		data:{itemId: $itemId},
		success:function(data)
		{
			$("#calculatingDialog").dialog("close");
			$("#saveSuccessDialog").dialog("open");
		},
		error:function(data)
		{
			$("#calculatingDialog").dialog("close");
			$("#saveErrorDialog").dialog("open");
		}
	});
}

function subFrameLoaded()
{
	$("#markDiv").addClass("hidden");
	$("#markDiv > div[name='loadingDiv']").addClass("hidden");
	$("#itemDiv").removeClass("hidden");
}