$(function()
{
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", function(){
		$("#itemList li").removeClass("current");
		$("#itemListDiv div[name='indexDiv']").addClass("current");
		$nowEditingId = null;
		setPublicHearingsItem({publicHearingsItem:{id:null}});
	});
	
	$("#saveSuccessDialog, #saveErrorDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});

	$("#calculatingDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false
	});
	
	$("#itemForm select[name='type']").bind("change", function()
	{
		var value = $("#itemForm select[name='type']").attr("value");
		if (value == 0)
		{
			$("#itemForm table tr[name='scoreRow']").removeClass("hidden");
		}
		else if(value == 1)
		{
			$("#itemForm table tr[name='scoreRow']").addClass("hidden");
		}
	});

	$("#itemForm").ajaxForm({
		url: 'createPublicHearingsItem.action',
		type: 'GET',
		dataType: 'json',
		beforeSubmit: function(){
			var $ifErr = false;
			var $errMsg = "<strong>有如下错误，请检查后重新提交。</strong><br />";
			var $temp;
			$temp = $("#itemForm input[name='name']").attr("value");
			if ($temp == null || $temp.length == 0){
				$ifErr = true;
				$errMsg += "名称不能为空<br />";
			}
			if ($("#itemForm select[name='type']").attr("value") == 0)
			{
				var $tempMin = $("#itemForm input[name='minScore']").attr("value");
				var $tempMax = $("#itemForm input[name='maxScore']").attr("value");
				var $tempDefault = $("#itemForm input[name='defaultScore']").attr("value");
				if ($tempMin == null || $tempMin.length == 0 || $tempMax == null || $tempMax.length == 0 || $tempDefault == null || $tempDefault.length == 0){
					$ifErr = true;
					$errMsg += "最高分最低分和默认得分不能为空<br />";
				}
			}
			if ($ifErr)
			{
				$("#saveErrorDialog").html($errMsg);
				$("#saveErrorDialog").dialog("open");
				return false;
			}
		},
		success: function(data)
		{
			if (!($nowEditingId > 0))
			{
				$("#itemList").append("<li value=" + data.publicHearingsItem.id + ">" + data.publicHearingsItem.itemId + " : " + data.publicHearingsItem.name + "</li>");
				$("#itemList li:last").bind("click", itemChoose);
			}
			else
			{
				$("#itemList li[value=" + data.publicHearingsItem.id + "]").replaceWith("<li value=" + data.publicHearingsItem.id + ">" + data.publicHearingsItem.itemId + " : " + data.publicHearingsItem.name + "</li>");
				$("#itemList li[value=" + data.publicHearingsItem.id + "]").bind("click", itemChoose);
			}
			$("#saveSuccessDialog").dialog("open");
			setPublicHearingsItem(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			$("#saveErrorDialog").dialog("open");
		}
	});

	$("#calculate-trigger").bind("click", function(){
		$("#calculatingDialog").dialog("open");
		$.ajax({
			url:"calculatePublicHearingsItemScoreById.action",
			type:"get",
			datatype:"json",
			data:{itemId: $nowEditingId },
			success:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveSuccessDialog").dialog("open");
			},
			error:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveErrorDialog").dialog("open");
			}
		});
	});

	$nowEditingId = null;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#header .global-nav-master[name='system-setting']").addClass("current");
	$("#header li.global-nav-master.current li[name='public-hearings-item-setting']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$.get(
		'queryPublicHearingsItemById.action', 
		{Id: $(this).attr("value")}, 
		setPublicHearingsItem, 
		'json'
	);
	$(this).addClass("current");
}

function newItem()
{
	$("#itemList li").removeClass("current");
	var newItem = {};
	var publicHearingsItem = {};
	publicHearingsItem.id = 0;
	publicHearingsItem.itemId = null;
	publicHearingsItem.name = null;
	publicHearingsItem.minScore = null;
	publicHearingsItem.maxScore = null;
	publicHearingsItem.defaultScore = null;
	publicHearingsItem.averageScore = null;
	publicHearingsItem.deltaScore = null;
	publicHearingsItem.weight = null;
	publicHearingsItem.contents = null;
	newItem.publicHearingsItem = publicHearingsItem;
	setPublicHearingsItem(newItem);
}

function delItem()
{
	if (!($nowEditingId > 0))
	{
		alert("请选择项目");
	}
	else
	{
		$.get(
			'deletePublicHearingsItemById.action', 
			{itemId: $nowEditingId}, 
			function(data, textStatus){
				$("#itemList li[value=" + data.itemId + "]").remove();
				$nowEditingId = 0;
				var nullData = {};
				var item = {};
				item.id = 0;
				nullData.publicHearingsItem = item;
				setPublicHearingsItem(nullData);
				$("#saveSuccessDialog").dialog("open");
			}, 
			'json'
		);
	}
}

function setPublicHearingsItem(data)
{
	$nowEditingId = data.publicHearingsItem.id;
	if ($nowEditingId != null)
	{
		$("#markDiv").addClass("hidden");
		$("#itemDiv").removeClass("hidden");
	}
	else
	{
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
		return;
	}
	$("#itemDiv input[name='id']").attr("value", data.publicHearingsItem.id);
	$("#itemDiv input[name='itemId']").attr("value", data.publicHearingsItem.itemId);
	$("#itemDiv input[name='name']").attr("value", data.publicHearingsItem.name);
	$("#itemDiv input[name='minScore']").attr("value", data.publicHearingsItem.minScore);
	$("#itemDiv input[name='maxScore']").attr("value", data.publicHearingsItem.maxScore);
	$("#itemDiv input[name='defaultScore']").attr("value", data.publicHearingsItem.defaultScore);
	$("#itemDiv input[name='averageScore']").attr("value", data.publicHearingsItem.averageScore);
	$("#itemDiv input[name='deltaScore']").attr("value", data.publicHearingsItem.deltaScore);
	$("#itemDiv input[name='weight']").attr("value", data.publicHearingsItem.weight);
	$("#itemDiv select[name='type']").attr("value", data.publicHearingsItem.type);
	$("#itemDiv select[name='type']").triggerHandler("change");
	$("#itemDiv textarea[name='contents']").attr("value", data.publicHearingsItem.contents);
}