$(function()
{
	$("#header").corner("top 3px");
	$(".global-nav-master > a").each(function(){
		$(this).corner("top 3px cc:#f0890e");
	});

	$("#stateWrongDialog, #standardSystemNoFunctionDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			好的: function() {
				$(this).dialog("close");
			}
		}
	});
	
	$course_id = $("#course_id").val();
	$("input.courseId").val($course_id);
});

function jump($name, $data)
{
	window.location.href = $name + "?courseId=" + $course_id;
}

function showStateWrongDialog()
{
	$("#stateWrongDialog").dialog("open");
}
function showStandardSystemNoFunctionDialog()
{
	$("#standardSystemNoFunctionDialog").dialog('open');
}