$(function()
{
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChoose);
	
	$("#saveSuccessDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#itemForm").ajaxForm({
		url: 'createCollectionItemPublicitySet.action',
		type: 'GET',
		dataType: 'json',
		beforeSubmit: function(){},
		success: function(data)
		{
			$("#saveSuccessDialog").dialog("open");
			setCollectionItem(data);
		}, 
		error: function(XMLHttpRequest, textStatus, errorThrown){
			alert("操作不成功");
		}
	});

	$nowEditingId = 0;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#header .global-nav-master[name='system-setting']").addClass("current");
	$("#header li.global-nav-master.current li[name='item-publicity-setting']").addClass("current");
	
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$nowEditingId = $(this).attr("value");
	if ($nowEditingId > 0)
	{
		$.get(
			'queryCollectionItemById.action', 
			{Id: $nowEditingId}, 
			setCollectionItem, 
			'json'
		);
	}
	else
		setCollectionItem();
	$(this).addClass("current");
}

function setCollectionItem(data)
{
	if ($nowEditingId != null)
	{
		$("#markDiv").addClass("hidden");
		$("#itemDiv").removeClass("hidden");
	}
	else
	{
		$("#itemDiv").addClass("hidden");
		$("#markDiv").removeClass("hidden");
		return;
	}
	$("#itemDiv input[name='id']").attr("value", data.collectionItem.id);
	$("#itemDiv div[name='title'] span").text(data.collectionItem.itemId + " : " + data.collectionItem.name);
	$("#itemDiv select[name='publicityType']").attr("value", data.collectionItem.publicityType);
}