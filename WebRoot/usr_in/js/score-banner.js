$(function()
{
	$nowEditingId = 0;
	$("#itemListDiv div[name='indexDiv']").addClass("current");
	$("#itemList li").bind("click", itemChoose);
	$("#itemListDiv div[name='indexDiv']").bind("click", itemChoose);
	
	$("#saveSuccessDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false,
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
			}
		}
	});

	$("#calculatingDialog").dialog({
		autoOpen: false,
		width: 300,
		resizable: false
	});
	
	$("#calculate-trigger").bind("click", function(){
		$("#calculatingDialog").dialog("open");
		$.ajax({
			url:"calculateCollectionItemScoreById.action",
			type:"get",
			datatype:"json",
			data:{itemId: $nowEditingId },
			success:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveSuccessDialog").dialog("open");
			},
			error:function(data)
			{
				$("#calculatingDialog").dialog("close");
				$("#saveErrorDialog").dialog("open");
			}
		});
	});
	
	//header current
	$("#header .global-nav-master[name='system-moniter']").addClass("current");
	$("#header li.global-nav-master.current li[name='score-monitor']").addClass("current");
	
	//frame corder
	$("#itemListDiv, #itemListDiv > div[name='list']").corner("left");
	$("#contents").corner("right");
});

function itemChoose()
{
	$("#itemList li").removeClass("current");
	$("#itemListDiv div[name='indexDiv']").removeClass("current");
	$nowEditingId = $(this).attr("value");
	if ($nowEditingId > 0)
	{
		$.ajax({
			url:'queryScoreStatus.action',
			data: {itemId: $nowEditingId, type: 1, courseId: $course_id},
			dataType: "json",
			type: "get",
			success: function(data)
			{
				drawGraph(data.scoreStatus);
			}
		});
	}
	$(this).addClass("current");
}

function drawGraph(scoreData)
{
	//scoreData:[["20-40",20], "40-60":30, "60-80":15]
	//Your code goes here
	//Your graph should go in #graphDiv
	$.jqplot.config.enablePlugins = true;
	var s1 = [["1", 2], ["2", 6], ["3", 7]];
	var ticks = ['1', '2', '3'];

    $("#markDiv").addClass("hidden");
    $("#itemDiv").removeClass("hidden");

	$("#graphDiv").html("");
	$.jqplot('graphDiv', [scoreData.intStatus], {
         seriesDefaults: {
         renderer: $.jqplot.BarRenderer, //使用柱状图表示
         rendererOptions: {
              barMargin: 35,   //柱状体组之间间隔
              ticks: ticks
          }
         },
         axes: {
             xaxis: {
             	ticks:scoreData.ticks,
                 renderer: $.jqplot.CategoryAxisRenderer //x轴绘制方式
             }
         }
     });
     /*
	plot1 = $.jqplot('graphDiv', [scoreData.intStatus], {
	    // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
	    animate: !$.jqplot.use_excanvas,
	    seriesDefaults:{
	        renderer:$.jqplot.BarRenderer,
	        pointLabels: { show: true }
	    },
	    axes: {
	        xaxis: {
	            renderer: $.jqplot.CategoryAxisRenderer,
	            ticks: ticks
	        }
	    },
	    highlighter: { show: false }
	});*/

}