package com.geniusye.studentmanage.service;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.SysUser;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.util.ErrorType;

@Transactional
public interface ISysUserService
{
	@Transactional
	public SysUserOut findCurrentUser();
	
	@Transactional
	public SysUser findByUsername(String username);

	@Transactional
	public SysUserOut findUserOutByUsername(String username);

	@Transactional
	public SysUserOut findUserOutByWxOpenId(String wxOpenId);
	
	@Transactional
	public SysUserOut queryUserOutById(Long id);
	
	@Transactional
	public boolean changePasswordById(Long userId, String oldPassword, String newPassword);
	
	@Transactional
	public ErrorType appToResetUserPassword(String username);
	
	@Transactional
	public ErrorType resetUserPassword(String username, String password, String resetCode);

}
