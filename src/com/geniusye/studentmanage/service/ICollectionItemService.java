package com.geniusye.studentmanage.service;

import java.util.List;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;

public interface ICollectionItemService 
{
	public void merge(CollectionItem item);
	
	public void save(CollectionItem item);
	
	public void delete(CollectionItem item);
	
	public CollectionItem queryItemById(Long id);
	
	public void setCheckOperator(CollectionItem item, List<SysUserOut> newUsers);
	
	public void setPublicityType(CollectionItem item, Integer publicityType);

	public List<CollectionItem> queryItemByCheckOperatorId(Long userId, Long courseId);
	
	public List<CollectionItem> queryItemAll(Long courseId);

	public List<CollectionItem> queryItemWhichNeedCheck(Long courseId);

	public List<CollectionItem> queryItemWhichShallBePublicized(Long courseId);
}
