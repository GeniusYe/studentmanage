package com.geniusye.studentmanage.service;

import java.util.List;

import com.geniusye.studentmanage.bean.PublicHearingsItem;

public interface IPublicHearingsItemService 
{
	public void merge(PublicHearingsItem item);
	
	public void save(PublicHearingsItem item);
	
	public void delete(PublicHearingsItem item);
	
	public PublicHearingsItem queryItemById(Long id);
	
	public List<PublicHearingsItem> queryItemAll(Long courseId);
	
	public List<PublicHearingsItem> queryItemWhichShallBePublicized(Long courseId);
	
	public List<PublicHearingsItem> queryItemTextWhichShallBePublicized(Long courseId);
}
