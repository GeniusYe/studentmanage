package com.geniusye.studentmanage.service;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.util.ErrorType;

public interface ICalculateScoreService 
{
	@Transactional
	public ErrorType calculateAll(Course course);
	
	@Transactional
	public ErrorType calculateCollectionItem(CollectionItem item);
	
	@Transactional
	public ErrorType calculatePublicHearingsItem(PublicHearingsItem item);
	
	@Transactional
	public ErrorType calculateTotalScore(Course course);
}
