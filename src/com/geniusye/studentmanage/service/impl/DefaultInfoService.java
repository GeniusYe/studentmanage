package com.geniusye.studentmanage.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemInfoCheck;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemInfo;
import com.geniusye.studentmanage.bean.PublicHearingsItemInfoText;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.ScoreStatus;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.dao.CollectionItemDao;
import com.geniusye.studentmanage.dao.CollectionItemInfoCheckDao;
import com.geniusye.studentmanage.dao.CollectionItemInfoDao;
import com.geniusye.studentmanage.dao.CollectionItemScoreDao;
import com.geniusye.studentmanage.dao.InfoDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemInfoDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemInfoTextDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemScoreDao;
import com.geniusye.studentmanage.dao.TotalScoreDao;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.ScoreComparator;

public class DefaultInfoService implements IInfoService
{
	private CollectionItemDao collectionItemDao;
	private CollectionItemInfoDao collectionItemInfoDao;
	private CollectionItemInfoCheckDao collectionItemInfoCheckDao;
	private CollectionItemScoreDao collectionItemScoreDao;
	private PublicHearingsItemDao publicHearingsItemDao;
	private PublicHearingsItemInfoDao publicHearingsItemInfoDao;
	private PublicHearingsItemInfoTextDao publicHearingsItemInfoTextDao;
	private PublicHearingsItemScoreDao publicHearingsItemScoreDao;
	private TotalScoreDao totalScoreDao;
	private InfoDao infoDao;
	
	/* CollectionItemInfo */
	
	@Transactional
	public ErrorType setCollectionItemInfo(SysUserOut user, Long itemId, String text)
	{
		CollectionItem item = this.collectionItemDao.queryItemById(itemId);
		if (item == null)
			return ErrorType.COLLECTION_ITEM_NOT_EXIST;
		if (item.getStartFillingTime() != null)
		{
			Date date = new Date();
			if (date.before(item.getStartFillingTime()) || date.after(item.getEndFillingTime()))
			{
				return ErrorType.COLLECTION_ITEM_FILLING_IS_NOT_AT_A_PROPER_ITEM_STATE;
			}
		}
		CollectionItemInfo info = this.collectionItemInfoDao.queryByInfoProviderAndItemId(user.getId(), itemId);
		if (info == null)
			info = new CollectionItemInfo();
		info.setCollectionItem(item);
		info.setInfoProvider(user);
		info.setText(text);
		this.collectionItemInfoDao.merge(info);
		return ErrorType.NO_ERROR;
	}

	@Transactional
	public CollectionItemInfo queryCollectionItemInfoByInfoProviderAndItemId(Long infoProvider, Long itemId)
	{
		return this.collectionItemInfoDao.queryByInfoProviderAndItemId(infoProvider, itemId);
	}
	
	/* CollectionItemInfoCheck */
	
	@Transactional
	public ErrorType setCollectionItemInfoCheck(SysUserOut infoProvider, Long itemId, SysUserOut infoTarget, Integer value, Boolean force)
	{
		CollectionItem item = this.collectionItemDao.queryItemById(itemId);
		if (item == null)
			return ErrorType.COLLECTION_ITEM_NOT_EXIST;
		CollectionItemInfoCheck info = this.collectionItemInfoCheckDao.queryForceInfoByInfoTargetAndItemId(infoTarget.getId(), itemId);
		if (force == false && info != null && info.getForce() == true)
			return ErrorType.COLLECTION_INFO_CHECK_FORCE_ALREADY_EXIST;
		if (force)
			info = this.collectionItemInfoCheckDao.queryForceInfoByInfoTargetAndItemId(infoTarget.getId(), itemId);
		else
			info = this.collectionItemInfoCheckDao.queryByInfoProviderAndInfoTargetAndItemId(infoProvider.getId(), infoTarget.getId(), itemId);
		if (force && info != null)
		{
			this.collectionItemInfoCheckDao.delete(info);
			info = new CollectionItemInfoCheck();
		}
		if (info == null)
			info = new CollectionItemInfoCheck();
		info.setCollectionItem(item);
		if (force)
		{
			info.setInfoProvider(null);
			info.setForce(true);
		}
		else
		{
			info.setInfoProvider(infoProvider);
			info.setForce(false);
		}
		info.setInfoTarget(infoTarget);
		info.setValue(value);
		this.collectionItemInfoCheckDao.merge(info);
		return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public ErrorType unsetCollectionItemInfoCheck(SysUserOut infoProvider, Long itemId, SysUserOut infoTarget, Boolean force)
	{
		CollectionItemInfoCheck collectionItemInfoCheck;
		if (force)
			collectionItemInfoCheck = this.collectionItemInfoCheckDao.queryForceInfoByInfoTargetAndItemId(infoTarget.getId(), itemId);
		else
			collectionItemInfoCheck = this.collectionItemInfoCheckDao.queryByInfoProviderAndInfoTargetAndItemId(infoProvider.getId(), infoTarget.getId(), itemId);
		if (collectionItemInfoCheck == null)
		{
			return ErrorType.COLLECTION_ITEM_INFO_CHECK_NOT_EXIST;
		}
		
		this.collectionItemInfoCheckDao.delete(collectionItemInfoCheck);
		return ErrorType.NO_ERROR;
	}

	@Transactional
	public List<Object> queryCollectionItemInfoCheckToAllUserForce(
			CollectionItem collectionItem) {
		return this.infoDao.queryCollectionItemInfoCheckToAllUserForce(collectionItem);
	}

	@Transactional
	public List<Object> queryCollectionItemInfoCheckToAllUser(
			CollectionItem collectionItem, SysUserOut user) {
		return this.infoDao.queryCollectionItemInfoCheckToAllUser(collectionItem, user);
	}
	
	/* PublicHearingsItemInfo */
	
	@Transactional
	public ErrorType setPublicHearingsItemInfo(SysUserOut user, Long itemId, SysUserOut infoTarget, Integer value)
	{
		PublicHearingsItem item = this.publicHearingsItemDao.queryItemById(itemId);
		if (item == null || (int)item.getType() != 0)
			return ErrorType.PUBLIC_HEARINGS_ITEM_NOT_EXIST;
		PublicHearingsItemInfo info = this.publicHearingsItemInfoDao.queryByInfoProviderAndInfoTargetAndItemId(user.getId(), infoTarget.getId(), itemId);
		if (info == null)
			info = new PublicHearingsItemInfo();
		info.setPublicHearingsItem(item);
		info.setInfoProvider(user);
		info.setInfoTarget(infoTarget);
		info.setValue(value);
		this.publicHearingsItemInfoDao.merge(info);
		return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public ErrorType setPublicHearingsItemInfoText(SysUserOut user, Long itemId, SysUserOut infoTarget, String text)
	{
		PublicHearingsItem item = this.publicHearingsItemDao.queryItemById(itemId);
		if (item == null || (int)item.getType() != 1)
			return ErrorType.PUBLIC_HEARINGS_ITEM_NOT_EXIST;
		PublicHearingsItemInfoText info = this.publicHearingsItemInfoTextDao.queryByInfoProviderAndInfoTargetAndItemId(user.getId(), infoTarget.getId(), itemId);
		if (info == null)
			info = new PublicHearingsItemInfoText();
		info.setPublicHearingsItem(item);
		info.setInfoProvider(user);
		info.setInfoTarget(infoTarget);
		info.setText(text);
		this.publicHearingsItemInfoTextDao.merge(info);
		return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public List<Object> queryPublicHearingsItemInfoToAllUser(
			PublicHearingsItem publicHearingsItem, SysUserOut user) {
		return this.infoDao.queryPublicHearingsItemInfoToAllUser(publicHearingsItem, user);
	}

	@Transactional
	public List<Object> queryPublicHearingsItemInfoTextToAllUser(
			PublicHearingsItem publicHearingsItem, SysUserOut user) {
		return this.infoDao.queryPublicHearingsItemInfoTextToAllUser(publicHearingsItem, user);
	}

	@Transactional
	public List<PublicHearingsItemInfoText> queryPublicHearingsItemInfoTextByInfoTargetAndItemId(Long infoTarget, Long itemId)
	{
		return this.publicHearingsItemInfoTextDao.queryByInfoTargetAndItemId(infoTarget, itemId);
	}
	
	/* Score */
	
	@Transactional
	public ErrorType setCollectionItemScore(SysUserOut user, Long itemId, Double value, Long rank)
	{
		CollectionItem item = this.collectionItemDao.queryItemById(itemId);
		if (item == null)
			return ErrorType.COLLECTION_ITEM_NOT_EXIST;
		CollectionItemScore info = this.collectionItemScoreDao.queryByInfoTargetAndItemId(user.getId(), itemId);
		if (info == null)
			info = new CollectionItemScore();
		info.setCollectionItem(item);
		info.setInfoTarget(user);
		info.setValue(value);
		info.setRank(rank);
		this.collectionItemScoreDao.merge(info);
		return ErrorType.NO_ERROR;
	}

	@Transactional
	public CollectionItemScore queryCollectionItemScore(Long infoTarget, Long itemId)
	{
		return this.collectionItemScoreDao.queryByInfoTargetAndItemId(infoTarget, itemId);
	}

	@Transactional
	public List<CollectionItemScore> queryCollectionItemScores(Long userId, Long courseId)
	{
		return this.collectionItemScoreDao.queryByInfoTargetAndCourseId(userId, courseId);
	}
	
	@Transactional
	public ErrorType setPublicHearingsItemScore(SysUserOut user, Long itemId, Double value, Long rank)
	{
		PublicHearingsItem item = this.publicHearingsItemDao.queryItemById(itemId);
		if (item == null)
			return ErrorType.PUBLIC_HEARINGS_ITEM_NOT_EXIST;
		PublicHearingsItemScore info = this.publicHearingsItemScoreDao.queryByInfoTargetAndItemId(user.getId(), itemId);
		if (info == null)
			info = new PublicHearingsItemScore();
		info.setPublicHearingsItem(item);
		info.setInfoTarget(user);
		info.setValue(value);
		info.setRank(rank);
		this.publicHearingsItemScoreDao.merge(info);
		return ErrorType.NO_ERROR;
	}

	@Transactional
	public PublicHearingsItemScore queryPublicHearingsItemScore(Long infoTarget, Long itemId)
	{
		return this.publicHearingsItemScoreDao.queryByInfoTargetAndItemId(infoTarget, itemId);
	}

	@Transactional
	public List<PublicHearingsItemScore> queryPublicHearingsItemScores(Long userId, Long courseId)
	{
		return this.publicHearingsItemScoreDao.queryByInfoTargetAndCourseId(userId, courseId);
	}
	
	@Transactional
	public ErrorType setTotalScore(SysUserOut user, Course course, Double value, Long rank)
	{
		TotalScore info = this.totalScoreDao.queryByInfoTargetAndCourseId(course.getId(), user.getId());
		if (info == null)
			info = new TotalScore(course);
		info.setInfoTarget(user);
		info.setValue(value);
		info.setRank(rank);
		this.totalScoreDao.merge(info);
		return ErrorType.NO_ERROR;
	}

	@Transactional
	public TotalScore queryTotalScore(Long courseId, Long infoTarget)
	{
		return this.totalScoreDao.queryByInfoTargetAndCourseId(courseId, infoTarget);
	}

	@Transactional
	public ScoreStatus queryScoreStatus(CollectionItem item)
	{
		ScoreStatus status = new ScoreStatus();
		List<CollectionItemScore> info = this.collectionItemScoreDao.queryByItemId(item.getId());
		Collections.sort(info, new ScoreComparator<CollectionItemScore>(false));
		
		int seperationSize = 10;
		
		int[] intStatus = new int[seperationSize + 1];
		int[] intRange = new int[seperationSize + 1];
		int maxValue = item.getMaxScore();
		int minValue = item.getMinScore();
		if (seperationSize > maxValue - minValue)
			seperationSize = maxValue - minValue;
		int step = (maxValue - minValue) / seperationSize;
		int currentValue = minValue;
		int currentPosition = 0;
		
		intRange[0] = minValue;
		for (int i = 1; i < seperationSize + 1; i++)
		{
			intRange[i] = intRange[i - 1] + step;
		}
		for (CollectionItemScore score : info)
		{
			while (score.getValue() >= currentValue + step)
			{
				currentValue += step;
				if (currentPosition < seperationSize)
					currentPosition++;
			}
			intStatus[currentPosition]++;
		}
		double minPartValue = 0;//size * 1.0 / seperationSize / 4;
		int minPartPosition = 0;
		int maxPartPosition = seperationSize;
		for (int i = 0; i < seperationSize + 1; ++i)
		{
			if (intStatus[i] < minPartValue)
			{
				intStatus[i + 1] += intStatus[i];
				minPartPosition = i + 1;
			}
			else
				break;
		}
		for (int i = seperationSize; i > minPartPosition; --i)
		{
			if (intStatus[i] < minPartValue)
			{
				intStatus[i - 1] += intStatus[i];
				maxPartPosition = i - 1;
			}
			else
				break;
		}
		
		int resultSize = maxPartPosition - minPartPosition + 1;
		Integer[][] resultIntStatus = new Integer[resultSize][2];
		Integer[] resultTicks = new Integer[resultSize];
		
		for (int i = 0; i < resultSize; i++)
		{
			resultIntStatus[i][1] = intStatus[minPartPosition + i];
			resultIntStatus[i][0] = intRange[minPartPosition + i];
			resultTicks[i] = intRange[minPartPosition + i];
		}
		
		status.setIntStatus(resultIntStatus);
		status.setTicks(resultTicks);
		
		return status;
	}

	/* setter */
	
	public void setCollectionItemInfoCheckDao(
			CollectionItemInfoCheckDao collectionItemInfoCheckDao) {
		this.collectionItemInfoCheckDao = collectionItemInfoCheckDao;
	}

	public void setCollectionItemDao(CollectionItemDao collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	public void setInfoDao(InfoDao infoDao) {
		this.infoDao = infoDao;
	}

	public void setPublicHearingsItemDao(PublicHearingsItemDao publicHearingsItemDao) {
		this.publicHearingsItemDao = publicHearingsItemDao;
	}

	public void setPublicHearingsItemInfoDao(
			PublicHearingsItemInfoDao publicHearingsItemInfoDao) {
		this.publicHearingsItemInfoDao = publicHearingsItemInfoDao;
	}

	public void setPublicHearingsItemInfoTextDao(
			PublicHearingsItemInfoTextDao publicHearingsItemInfoTextDao) {
		this.publicHearingsItemInfoTextDao = publicHearingsItemInfoTextDao;
	}

	public void setCollectionItemScoreDao(
			CollectionItemScoreDao collectionItemScoreDao) {
		this.collectionItemScoreDao = collectionItemScoreDao;
	}

	public void setPublicHearingsItemScoreDao(
			PublicHearingsItemScoreDao publicHearingsItemScoreDao) {
		this.publicHearingsItemScoreDao = publicHearingsItemScoreDao;
	}

	public void setTotalScoreDao(TotalScoreDao totalScoreDao) {
		this.totalScoreDao = totalScoreDao;
	}

	public void setCollectionItemInfoDao(CollectionItemInfoDao collectionItemInfoDao) {
		this.collectionItemInfoDao = collectionItemInfoDao;
	}
}
