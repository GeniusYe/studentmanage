package com.geniusye.studentmanage.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.PermissionLevel;

public class DefaultPermissionCheckService implements IPermissionCheckService
{
	private ISystemStateService systemStateService;

	/* CollectionItem Editing */

	@Override
	@Transactional
	public PermissionLevel queryCollectionItemAll(SysUserOut operator, Course course)
	{
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel createCollectionItem(SysUserOut operator, Course course, CollectionItem collectionItem) 
	{
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editCollectionItem(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel deleteCollectionItem(SysUserOut operator,
			CollectionItem collectionItem) 
	{
		if (collectionItem == null)
		{
			return new PermissionLevel(ErrorType.COLLECTION_ITEM_NOT_EXIST);
		}
		if (!collectionItem.getRelatedCourse().checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editCollectionItem(collectionItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel updateCollectionItemCheck(SysUserOut operator,
			CollectionItem collectionItem) 
	{
		if (collectionItem == null)
		{
			return new PermissionLevel(ErrorType.COLLECTION_ITEM_NOT_EXIST);
		}
		if (!collectionItem.getRelatedCourse().checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editCollectionItemCheck(collectionItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel updateCollectionItemPublicity(SysUserOut operator,
			CollectionItem collectionItem) 
	{
		if (collectionItem == null)
		{
			return new PermissionLevel(ErrorType.COLLECTION_ITEM_NOT_EXIST);
		}
		if (!collectionItem.getRelatedCourse().checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editCollectionItemCheck(collectionItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}
	
	/* PublicHearingsItem Editing */

	@Override
	@Transactional
	public PermissionLevel createPublicHearingsItem(SysUserOut operator,
			Course course, PublicHearingsItem publicHearingsItem) 
	{
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editPublicHearingsItem(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel deletePublicHearingsItem(SysUserOut operator,
			PublicHearingsItem publicHearingsItem) 
	{
		if (publicHearingsItem == null)
		{
			return new PermissionLevel(ErrorType.COLLECTION_ITEM_NOT_EXIST);
		}
		if (!publicHearingsItem.getRelatedCourse().checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		if (this.systemStateService.editPublicHearingsItem(publicHearingsItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}
	
	/* Fill CollectionItem & PublicHearingsItem */
	
	@Override
	@Transactional
	public PermissionLevel querySelfCollectionItemInfo(SysUserOut operator, Course course)
	{
		if (!course.checkStuUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_STU_LIST);
		}
		if (this.systemStateService.querySelfCollectionItemInfo(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel queryPublicHearingsItemAll(SysUserOut operator, Course course)
	{
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel queryPublicHearingsItemAllForFill(SysUserOut operator, Course course)
	{
		if (!course.checkStuUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_STU_LIST);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel fillCollectionItemInfo(SysUserOut operator,
			CollectionItem collectionItem) 
	{
		if (!collectionItem.getRelatedCourse().checkStuUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_STU_LIST);
		}
		if (this.systemStateService.fillCollectionItemInfo(collectionItem) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel fillPublicHearingsItemInfo(SysUserOut operator,
			PublicHearingsItem publicHearingsItem) 
	{
		if (!publicHearingsItem.getRelatedCourse().checkStuUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_STU_LIST);
		}
		if (this.systemStateService.fillPublicHearingsItemInfo(publicHearingsItem) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	/* Fill CollectionItemCheck */
	
	@Override
	@Transactional
	public PermissionLevel queryCollectionItemInfoImage(SysUserOut operator, CollectionItem item, Long targetUserId)
	{
		if (this.systemStateService.fillCollectionItemInfo(item) != ISystemStateService.CheckResult.Suitable &&
				this.systemStateService.fillCollectionItemInfoCheck(item.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (item.checkCheckOperator(operator) == true) { }
		else if (targetUserId.equals(operator.getId())) { }
		else
		{
			return new PermissionLevel(ErrorType.USER_IS_NOT_IN_THE_CHECKOPERATORS_LIST_NOR_INFORPROVIDER);
		}
		return new PermissionLevel(true);
	}
	
	@Override
	@Transactional
	public PermissionLevel queryCollectionItemWhichNeedCheck(SysUserOut operator, Course course)
	{
		if (!course.checkTaUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_TA_LIST);
		}
		if (this.systemStateService.fillCollectionItemInfoCheck(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}
	
	@Override
	@Transactional
	public PermissionLevel fillCollectionItemInfoCheck(SysUserOut operator, Boolean force,
			CollectionItem collectionItem)
	{
		if (this.systemStateService.fillCollectionItemInfoCheck(collectionItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!force)
		{
			if (collectionItem.checkCheckOperator(operator) == false)
			{
				return new PermissionLevel(ErrorType.USER_IS_NOT_IN_THE_CHECKOPERATORS_LIST);
			}
		}
		else
		{
			if (!collectionItem.getRelatedCourse().checkAdminUsers(operator))
			{
				return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
			}
		}
		return new PermissionLevel(true);
	}
	
	/* Calculate Score */

	@Override
	@Transactional
	public PermissionLevel calculateTotalScore(SysUserOut operator, Course course) 
	{
		if (this.systemStateService.calculateScore(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		return new PermissionLevel(true);
	}
	
	@Override
	@Transactional
	public PermissionLevel queryUsers(SysUserOut operator, Course course)
	{
		if (!course.checkStuUsers(operator) && !course.checkTaUsers(operator) && !course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_LIST);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel calculateCollectionItemScore(SysUserOut operator,
			CollectionItem collectionItem) 
	{
		if (this.systemStateService.calculateScore(collectionItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!collectionItem.getRelatedCourse().checkAdminUsers(operator) && !collectionItem.checkCheckOperator(operator))
		{
			return new PermissionLevel(ErrorType.USER_IS_NOT_IN_THE_CHECKOPERATORS_LIST_NOR_ADMIN);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel calculatePublicHearingsItemScore(
			SysUserOut operator, PublicHearingsItem publicHearingsItem) 
	{
		if (this.systemStateService.calculateScore(publicHearingsItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!publicHearingsItem.getRelatedCourse().checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		return new PermissionLevel(true);
	}
	
	/* Publicity */

	@Override
	@Transactional
	public PermissionLevel queryPublicHearingsItemInfoText(PublicHearingsItem publicHearingsItem) 
	{
		if (this.systemStateService.queryPublicHearingsItemInfoText(publicHearingsItem.getRelatedCourse()) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel queryItemWhichShallBePublicity(SysUserOut operator, Course course) 
	{
		if (!course.checkStuUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_STU_LIST);
		}
		if (this.systemStateService.queryPublicityInformation(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel querySelfScore(SysUserOut operator,
			Course course, CollectionItem collectionItem, PublicHearingsItem publicHearingsItem) 
	{
		if (collectionItem != null)
			course = collectionItem.getRelatedCourse();
		else if (publicHearingsItem != null)
			course = publicHearingsItem.getRelatedCourse();
//		else
//			course must be set ahead, this is used when query total score
		if (this.systemStateService.queryPublicityInformation(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

	@Override
	@Transactional
	public PermissionLevel queryScoreStatus(SysUserOut operator,
			Course course, CollectionItem collectionItem, PublicHearingsItem publicHearingsItem) 
	{
		if (collectionItem != null)
			course = collectionItem.getRelatedCourse();
		else if (publicHearingsItem != null)
			course = publicHearingsItem.getRelatedCourse();
//		else
//			course must be set ahead, this is used when query total score
		if (this.systemStateService.calculateScore(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!course.checkAdminUsers(operator) && !course.checkTaUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_AND_TA_LIST);
		}
		return new PermissionLevel(true);
	}
	
	/* Generate Report */

	@Override
	@Transactional
	public PermissionLevel generateReport(SysUserOut operator, Course course) 
	{
		if (this.systemStateService.generateReport(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		if (!course.checkAdminUsers(operator))
		{
			return new PermissionLevel(ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
		}
		return new PermissionLevel(true);
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	@Override
	public PermissionLevel wxQuerySelfScore(SysUserOut operator, Course course) 
	{
		if (this.systemStateService.queryPublicityInformation(course) != ISystemStateService.CheckResult.Suitable)
		{
			return new PermissionLevel(ErrorType.SYSTEM_STATE_NOT_SUITABLE);
		}
		return new PermissionLevel(true);
	}

}
