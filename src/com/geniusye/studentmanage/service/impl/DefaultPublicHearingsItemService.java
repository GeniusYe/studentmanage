package com.geniusye.studentmanage.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.dao.PublicHearingsItemDao;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;

public class DefaultPublicHearingsItemService implements IPublicHearingsItemService 
{
	private PublicHearingsItemDao publicHearingsItemDao;

	@Transactional
	public void save(PublicHearingsItem item) 
	{
		this.publicHearingsItemDao.save(item);
	}

	@Transactional
	public void merge(PublicHearingsItem item)
	{
		this.publicHearingsItemDao.merge(item);
	}

	@Transactional
	public void delete(PublicHearingsItem item) 
	{
		this.publicHearingsItemDao.delete(item);
	}

	@Transactional
	public PublicHearingsItem queryItemById(Long id) 
	{
		return this.publicHearingsItemDao.queryItemById(id);
	}
	
	@Transactional
	public List<PublicHearingsItem> queryItemAll(Long courseId)
	{
		return this.publicHearingsItemDao.queryItemAll(courseId);
	}

	@Transactional
	public List<PublicHearingsItem> queryItemTextWhichShallBePublicized (Long courseId) 
	{
		return this.publicHearingsItemDao.queryItemTextWhichShallBePublicized(courseId);
	}

	@Transactional
	public List<PublicHearingsItem> queryItemWhichShallBePublicized (Long courseId) 
	{
		return this.publicHearingsItemDao.queryItemWhichShallBePublicized(courseId);
	}

	public void setPublicHearingsItemDao(PublicHearingsItemDao publicHearingsItemDao) {
		this.publicHearingsItemDao = publicHearingsItemDao;
	}
}
