package com.geniusye.studentmanage.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.hibernate.Hibernate;

import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemInfoImage;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.CollectionItemInfoDao;
import com.geniusye.studentmanage.dao.CollectionItemInfoImageDao;
import com.geniusye.studentmanage.service.ICollectionItemInfoImageService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.util.SmallImageGenerator;

public class MySQLCollectionItemInfoImageService implements ICollectionItemInfoImageService
{
	private CollectionItemInfoDao collectionItemInfoDao;
	private CollectionItemInfoImageDao collectionItemInfoImageDao;
	private IInfoService infoService;

	public Long createCollectionItemInfoImage(SysUserOut user, Long itemId, InputStream inputStream, String type) throws IOException, SQLException
	{
		CollectionItemInfo itemInfo = this.collectionItemInfoDao.queryByInfoProviderAndItemId(user.getId(), itemId);
		if (itemInfo == null)
		{
			this.infoService.setCollectionItemInfo(user, itemId, null);
			itemInfo = this.collectionItemInfoDao.queryByInfoProviderAndItemId(user.getId(), itemId);
		}
		CollectionItemInfoImage image = new CollectionItemInfoImage();
		byte[] buffer = new byte[(int)inputStream.available()];
		inputStream.read(buffer); 
		image.setImage(Hibernate.createBlob(buffer));
		byte[] smallBuffer = SmallImageGenerator.generate(buffer, 100, 70);
		image.setSmallImage(Hibernate.createBlob(smallBuffer));
		image.setType(type);
		image.setCollectionItemInfo(itemInfo);
		this.collectionItemInfoImageDao.save(image);
		return image.getId();
	}

	public void setCollectionItemInfoImageDao(
			CollectionItemInfoImageDao collectionItemInfoImageDao) {
		this.collectionItemInfoImageDao = collectionItemInfoImageDao;
	}


	public void setCollectionItemInfoDao(CollectionItemInfoDao collectionItemInfoDao) {
		this.collectionItemInfoDao = collectionItemInfoDao;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}
}
