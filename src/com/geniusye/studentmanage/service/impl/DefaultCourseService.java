package com.geniusye.studentmanage.service.impl;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.dao.CourseDao;
import com.geniusye.studentmanage.service.ICourseService;

public class DefaultCourseService implements ICourseService
{
	private CourseDao courseDao;
	
	public Course queryById(Long courseId)
	{
		return this.courseDao.queryById(courseId);
	}

	public void setCourseDao(CourseDao courseDao) {
		this.courseDao = courseDao;
	}
}
