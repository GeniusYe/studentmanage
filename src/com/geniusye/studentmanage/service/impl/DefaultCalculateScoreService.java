package com.geniusye.studentmanage.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfoCheck;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemInfo;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.dao.CollectionItemDao;
import com.geniusye.studentmanage.dao.CollectionItemInfoCheckDao;
import com.geniusye.studentmanage.dao.CollectionItemScoreDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemInfoDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemScoreDao;
import com.geniusye.studentmanage.dao.SysUserDao;
import com.geniusye.studentmanage.service.ICalculateScoreService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.ScoreComparator;

public class DefaultCalculateScoreService implements ICalculateScoreService
{
	private CollectionItemDao collectionItemDao;
	private CollectionItemInfoCheckDao collectionItemInfoCheckDao;
	private CollectionItemScoreDao collectionItemScoreDao;
	private PublicHearingsItemDao publicHearingsItemDao;
	private PublicHearingsItemInfoDao publicHearingsItemInfoDao;
	private PublicHearingsItemScoreDao publicHearingsItemScoreDao;
	private SysUserDao sysUserDao;
	private IInfoService infoService;

	@Transactional
	public ErrorType calculateAll(Course course)
	{
		ErrorType errorCode;
		List<CollectionItem> collectionItems = this.collectionItemDao.queryItemAll(course.getId());
		for (CollectionItem item : collectionItems)
		{
			if ((errorCode = calculateCollectionItem(item)) != ErrorType.NO_ERROR)
			{
				return errorCode;
			}
		}
		List<PublicHearingsItem> publicHearingsItems = this.publicHearingsItemDao.queryItemAll(course.getId());
		for (PublicHearingsItem item : publicHearingsItems)
		{
			if ((errorCode = calculatePublicHearingsItem(item)) != ErrorType.NO_ERROR)
			{
				return errorCode;
			}
		}
		if ((errorCode = calculateTotalScore(course)) != ErrorType.NO_ERROR)
			return errorCode;
		else 
			return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public ErrorType calculateCollectionItem(CollectionItem item)
	{
		int maxScore = item.getMaxScore(), minScore = item.getMinScore(), defaultScore = item.getDefaultScore();
		Integer averageScore = item.getAverageScore(), deltaScore = item.getDeltaScore();
		NumList numList;
		
		List<SysUserOut> users = this.sysUserDao.queryAllStusByCourse(item.getRelatedCourse().getId());
		List<CollectionItemScore> itemScores = new ArrayList<CollectionItemScore>();
		
		HashMap<Long, NumList> hashMap = new HashMap<Long, NumList>();
		for (SysUserOut user : users)
		{
			NumList tempList = new NumList(defaultScore, maxScore, minScore, averageScore, deltaScore);
			hashMap.put(user.getId(), tempList);
		}
		
		for (SysUserOut user : item.getCheckOperator())
		{
			List<CollectionItemInfoCheck> oneInfoChecks = this.collectionItemInfoCheckDao.queryByInfoProviderAndItemId(user.getId(), item.getId());
			numList = new NumList(defaultScore, maxScore, minScore, averageScore, deltaScore);
			for (CollectionItemInfoCheck info : oneInfoChecks)
			{
				if (info.getValue() != null)
					numList.addNum(info.getValue());
				else
					numList.addDefaultNum();
			}
			numList.calculate();
			for (CollectionItemInfoCheck info : oneInfoChecks)
			{
				Long targetId = info.getInfoTarget().getId();
				NumList tempList = hashMap.get(targetId);
				if (tempList == null)
					continue;
				if (info.getValue() != null)
				{
					tempList.addNum(numList.getResult(info.getValue()));
				}
				else
				{
					tempList.addNum(numList.getDefaultResult());
				}
			}
		}
		
		numList = new NumList(defaultScore, maxScore, minScore, null, null);
		double tempI;
		for (SysUserOut user : users)
		{
			NumList tempList = (NumList)hashMap.get(user.getId());
			CollectionItemScore itemScore = this.collectionItemScoreDao.queryByInfoTargetAndItemId(user.getId(), item.getId());
			if (itemScore == null)
				itemScore = new CollectionItemScore();
			tempI = tempList.getAverageCurrent();
			itemScore.setCollectionItem(item);
			itemScore.setInfoTarget(user);
			itemScore.setValue(tempI);
			itemScores.add(itemScore);
			numList.addNum(tempI);
		}
		
		numList.calculate();
		double min = Double.MAX_VALUE;
		long rank = 0;
		long rankS = 0;
		
		for (int i = 0; i < itemScores.size(); ++i)
		{
			itemScores.get(i).setValue(numList.getResult(itemScores.get(i).getValue()));
			CollectionItemInfoCheck checkInfo = this.collectionItemInfoCheckDao.queryForceInfoByInfoTargetAndItemId(itemScores.get(i).getInfoTarget().getId(), item.getId());
			if (checkInfo != null)
			{
				itemScores.get(i).setForce(true);
				itemScores.get(i).setValue(checkInfo.getValue().doubleValue());
			}
		}
		Collections.sort(itemScores, new ScoreComparator<CollectionItemScore>());
		
		for (CollectionItemScore itemScore : itemScores)
		{
			double scoreT = itemScore.getValue();
			if (scoreT == min)
				rankS++;
			else
			{
				min = scoreT;
				rankS++;
				rank = rankS;
			}
			this.infoService.setCollectionItemScore(itemScore.getInfoTarget(), item.getId(), scoreT, rank);
		}
		
		return calculateTotalScore(item.getRelatedCourse());
	}
	
	@Transactional
	public ErrorType calculatePublicHearingsItem(PublicHearingsItem item)
	{
		if ((int)item.getType() == 0)
		{
			int maxScore = item.getMaxScore(), minScore = item.getMinScore(), defaultScore = item.getDefaultScore();
			Integer averageScore = item.getAverageScore(), deltaScore = item.getDeltaScore();
			NumList numList;
		
			List<SysUserOut> users = this.sysUserDao.queryAllStusByCourse(item.getRelatedCourse().getId());
			
			HashMap<Long, NumList> hashMap = new HashMap<Long, NumList>();
			for (SysUserOut user : users)
			{
				NumList tempList = new NumList(defaultScore, maxScore, minScore, averageScore, deltaScore);
				hashMap.put(user.getId(), tempList);
			}
			for (SysUserOut user : users)
			{
				List<PublicHearingsItemInfo> oneInfos = this.publicHearingsItemInfoDao.queryByInfoProviderAndItemId(user.getId(), item.getId());
				numList = new NumList(defaultScore, maxScore, minScore, averageScore, deltaScore);
				for (PublicHearingsItemInfo info : oneInfos)
				{
					if (info.getValue() != null)
						numList.addNum(info.getValue());
					else
						numList.addDefaultNum();
				}
				numList.calculate();
				for (PublicHearingsItemInfo info : oneInfos)
				{
					Long targetId = info.getInfoTarget().getId();
					NumList tempList = hashMap.get(targetId);
					if (info.getValue() != null)
					{
						tempList.addNum(numList.getResult(info.getValue()));
					}
					else
					{
						tempList.addNum(numList.getDefaultResult());
					}
				}
			}
			
			List<PublicHearingsItemScore> itemScores = new ArrayList<PublicHearingsItemScore>();
			numList = new NumList(defaultScore, maxScore, minScore, averageScore, deltaScore);
			double tempI;
			for (SysUserOut user : users)
			{
				NumList tempList = (NumList)hashMap.get(user.getId());
				PublicHearingsItemScore itemScore = this.publicHearingsItemScoreDao.queryByInfoTargetAndItemId(user.getId(), item.getId());
				if (itemScore == null)
					itemScore = new PublicHearingsItemScore();
				tempI = tempList.getAverageCurrent();
				itemScore.setPublicHearingsItem(item);
				itemScore.setInfoTarget(user);
				itemScore.setValue(tempI);
				itemScores.add(itemScore);
				numList.addNum(tempI);
			}
			numList.calculate();
			Collections.sort(itemScores, new ScoreComparator<PublicHearingsItemScore>());
			double min = Double.MAX_VALUE;
			long rank = 0;
			long rankS = 0;
			for (PublicHearingsItemScore itemScore : itemScores)
			{
				double scoreT = itemScore.getValue();
				if (scoreT == min)
					rankS++;
				else
				{
					min = scoreT;
					rankS++;
					rank = rankS;
				}
				this.infoService.setPublicHearingsItemScore(itemScore.getInfoTarget(), item.getId(), numList.getResult(scoreT), rank);
			}
		}
		
		return calculateTotalScore(item.getRelatedCourse());
	}
	
	@Transactional
	public ErrorType calculateTotalScore(Course course)
	{
		List<SysUserOut> sysUserOut = this.sysUserDao.queryAllStusByCourse(course.getId());
		List<TotalScore> totalScores = new ArrayList<TotalScore>();
		double totalScore = 0;
		for (SysUserOut user : sysUserOut)
		{
			totalScore = 0;
			List<PublicHearingsItemScore> publicHearingsItemScores = this.publicHearingsItemScoreDao.queryByInfoTargetAndCourseId(user.getId(), course.getId());
			List<CollectionItemScore> collectionItemScores = this.collectionItemScoreDao.queryByInfoTargetAndCourseId(user.getId(), course.getId());
			for (PublicHearingsItemScore score : publicHearingsItemScores)
			{
				Double scoreValue = score.getValue();
				PublicHearingsItem item = score.getPublicHearingsItem();
				if (item.getWeight() != null)
					totalScore += (scoreValue * item.getWeight() / item.getMaxScore());
				else
					totalScore += scoreValue;
			}
			for (CollectionItemScore score : collectionItemScores)
			{
				Double scoreValue = score.getValue();
				CollectionItem item = score.getCollectionItem();
				if (score.getCollectionItem().getWeight() != null)
					totalScore += (scoreValue * item.getWeight() / item.getMaxScore());
				else
					totalScore += scoreValue;
			}
			TotalScore tempScore = new TotalScore(course);
			tempScore.setInfoTarget(user);
			tempScore.setValue(totalScore);
			totalScores.add(tempScore);
		}
		Collections.sort(totalScores, new ScoreComparator<TotalScore>());
		double min = Double.MAX_VALUE;
		long rank = 0;
		long rankS = 0;
		for (TotalScore score : totalScores)
		{
			double scoreT = score.getValue();
			if (scoreT == min)
				rankS++;
			else
			{
				min = scoreT;
				rankS++;
				rank = rankS;
			}
			this.infoService.setTotalScore(score.getInfoTarget(), course, scoreT, rank);
		}
		return ErrorType.NO_ERROR;
	}
	
	public class NumList 
	{
		private Integer deltaNum = 0;
		private Integer defaultNum = 0;
		private Integer maxNum = 0;
		private Integer minNum = 0;
		private Integer averageNum;
		
		private Double averageCurrent;
		private Double averageInstance;
		private Double deltaCurrent;
		private Double deltaInstance;
		
		private List<Double> numSet = new ArrayList<Double>();
		
		public NumList(Integer defaultNum, Integer maxNum, Integer minNum, Integer averageNum, Integer deltaNum)
		{
			this.defaultNum = defaultNum;
			this.maxNum = maxNum;
			this.minNum = minNum;
			this.averageNum = averageNum;
			this.deltaNum = deltaNum;
		}
		
		public void addNum(Double num)
		{
			this.numSet.add(validateNum(num));
		}
		
		public void addNum(Integer num)
		{
			this.numSet.add(validateNum(num));
		}
		
		public void addDefaultNum()
		{
			this.numSet.add((double)defaultNum);
		}
		
		public void calculate()
		{
			calculateAverage();
		}
		
		public void calculateAverage()
		{
			double sum = 0;
			double delta_sum = 0;
			int count = this.numSet.size();
			for (Double i : this.numSet)
			{
				sum += i;
			}
			if (count == 0)
				this.averageCurrent = (double)this.defaultNum;
			else
				this.averageCurrent = sum / count;
			if (this.averageNum == null)
				this.averageInstance = 0D;
			else
				this.averageInstance = this.averageNum - this.averageCurrent;

			if (this.deltaNum != null)
			{
				for (Double i : this.numSet)
				{
					delta_sum += (i - this.averageCurrent) * (i - this.averageCurrent);
				}
				if (count == 0)
					this.deltaCurrent = 1.0;
				else
					this.deltaCurrent = delta_sum / count;
				if (this.deltaCurrent == 0)
					this.deltaCurrent = (double)this.deltaNum;
				this.deltaInstance = Math.sqrt(this.deltaNum / this.deltaCurrent);
			}
		}
		
		public Double getResult(Integer originalNum)
		{
			return getResult((double)originalNum);
		}
		
		public Double getResult(Double originalNum)
		{
			if (originalNum == null)
				originalNum = (double)this.defaultNum;
			Double result = originalNum;
			if (this.averageNum != null)
			{
				result += this.averageInstance;
				if (this.deltaNum != null)
				{
					result = (result - this.averageNum) * this.deltaInstance + this.averageNum;
				}
			}
			return validateNum(result);
		}
		
		public Double getDefaultResult()
		{
			return validateNum(this.defaultNum + this.averageInstance);
		}
		
		public Double validateNum(Integer num)
		{
			if (num > this.maxNum)
				return (double)this.maxNum;
			else if (num < this.minNum)
				return (double)this.minNum;
			else
				return (double)num;
		}
		
		public Double validateNum(Double num)
		{
			if (num > this.maxNum)
				return (double)this.maxNum;
			else if (num < this.minNum)
				return (double)this.minNum;
			else
				return num;
		}
		
		public Double getAverageCurrent()
		{
			if (this.averageCurrent == null)
				calculateAverage();
			return this.averageCurrent;
		}
	}

	public void setCollectionItemInfoCheckDao(
			CollectionItemInfoCheckDao collectionItemInfoCheckDao) {
		this.collectionItemInfoCheckDao = collectionItemInfoCheckDao;
	}

	public void setCollectionItemScoreDao(
			CollectionItemScoreDao collectionItemScoreDao) {
		this.collectionItemScoreDao = collectionItemScoreDao;
	}

	public void setPublicHearingsItemInfoDao(
			PublicHearingsItemInfoDao publicHearingsItemInfoDao) {
		this.publicHearingsItemInfoDao = publicHearingsItemInfoDao;
	}

	public void setPublicHearingsItemScoreDao(
			PublicHearingsItemScoreDao publicHearingsItemScoreDao) {
		this.publicHearingsItemScoreDao = publicHearingsItemScoreDao;
	}

	public void setSysUserDao(SysUserDao sysUserDao) {
		this.sysUserDao = sysUserDao;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setCollectionItemDao(CollectionItemDao collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	public void setPublicHearingsItemDao(PublicHearingsItemDao publicHearingsItemDao) {
		this.publicHearingsItemDao = publicHearingsItemDao;
	}
}
