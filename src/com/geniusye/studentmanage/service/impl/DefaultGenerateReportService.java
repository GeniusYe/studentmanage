package com.geniusye.studentmanage.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.dao.CollectionItemDao;
import com.geniusye.studentmanage.dao.CollectionItemScoreDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemDao;
import com.geniusye.studentmanage.dao.PublicHearingsItemScoreDao;
import com.geniusye.studentmanage.dao.TotalScoreDao;
import com.geniusye.studentmanage.service.IGenerateReportService;
import com.geniusye.studentmanage.util.ReportGenerator;

public class DefaultGenerateReportService implements IGenerateReportService
{
	private CollectionItemDao collectionItemDao;
	private PublicHearingsItemDao publicHearingsItemDao;
	private CollectionItemScoreDao collectionItemScoreDao;
	private PublicHearingsItemScoreDao publicHearingsItemScoreDao;
	private TotalScoreDao totalScoreDao;
	
	public void generateReport(Course course, String fileName) throws IOException, RowsExceededException, WriteException
	{
		ReportGenerator reportGenerator = new ReportGenerator(fileName);
		HashMap<Long, Integer> usersHash = new HashMap<Long, Integer>();
		HashMap<Long, Integer> collectionItemsHash = new HashMap<Long, Integer>();
		HashMap<Long, Integer> publicHearingsItemsHash = new HashMap<Long, Integer>();
		List<SysUserOut> users = course.getStuUsers();
		reportGenerator.addLabel("学号", 0, 0);
		reportGenerator.addLabel("姓名", 0, 1);
		reportGenerator.addLabel("总分", 0, 2);
		reportGenerator.addLabel("名次", 0, 3);
		int i = 1;
		for (SysUserOut user : users)
		{
			usersHash.put(user.getId(), i);
			reportGenerator.addLabel(user.getUsername(), i, 0);
			reportGenerator.addLabel(user.getName(), i, 1);
			i++;
		}
		i = 4;
		List<CollectionItem> collectionItems = this.collectionItemDao.queryItemAll(course.getId());
		for (CollectionItem item : collectionItems)
		{
			collectionItemsHash.put(item.getId(), i);
			reportGenerator.addLabel(item.getName(), 0, i);
			i++;
		}
		List<PublicHearingsItem> publicHearingsItems = this.publicHearingsItemDao.queryItemAll(course.getId());
		for (PublicHearingsItem item : publicHearingsItems)
		{
			publicHearingsItemsHash.put(item.getId(), i);
			reportGenerator.addLabel(item.getName(), 0, i);
			i++;
		}
		Integer targetR, itemC;
		List<CollectionItemScore> collectionItemScores = this.collectionItemScoreDao.queryByCourseId(course.getId());
		for (CollectionItemScore item : collectionItemScores)
		{
			targetR = usersHash.get(item.getInfoTarget().getId());
			itemC = collectionItemsHash.get(item.getCollectionItem().getId());
			if (targetR == null || itemC == null)
				continue;
			reportGenerator.addNumber(item.getValue(), targetR, itemC);
		}
		List<PublicHearingsItemScore> publicHearingsItemScores = this.publicHearingsItemScoreDao.queryByCourseId(course.getId());
		for (PublicHearingsItemScore item : publicHearingsItemScores)
		{
			targetR = usersHash.get(item.getInfoTarget().getId());
			itemC = publicHearingsItemsHash.get(item.getPublicHearingsItem().getId());
			if (targetR == null || itemC == null)
				continue;
			reportGenerator.addNumber(item.getValue(), targetR, itemC);
		}
		List<TotalScore> totalScores = this.totalScoreDao.queryByCourseId(course.getId());
		for (TotalScore item : totalScores)
		{
			targetR = usersHash.get(item.getInfoTarget().getId());
			if (targetR == null)
				continue;
			reportGenerator.addNumber(item.getValue(), targetR, 2);
			reportGenerator.addNumber(item.getRank(), targetR, 3);
		}
		reportGenerator.generateReport();
	}

	public void setCollectionItemDao(CollectionItemDao collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	public void setPublicHearingsItemDao(
			PublicHearingsItemDao publicHearingsItemDao) {
		this.publicHearingsItemDao = publicHearingsItemDao;
	}

	public void setCollectionItemScoreDao(
			CollectionItemScoreDao collectionItemScoreDao) {
		this.collectionItemScoreDao = collectionItemScoreDao;
	}

	public void setPublicHearingsItemScoreDao(
			PublicHearingsItemScoreDao publicHearingsItemScoreDao) {
		this.publicHearingsItemScoreDao = publicHearingsItemScoreDao;
	}

	public void setTotalScoreDao(TotalScoreDao totalScoreDao) {
		this.totalScoreDao = totalScoreDao;
	}

}
