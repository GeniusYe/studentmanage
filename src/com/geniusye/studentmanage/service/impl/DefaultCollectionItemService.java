package com.geniusye.studentmanage.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.CollectionItemDao;
import com.geniusye.studentmanage.service.ICollectionItemService;

public class DefaultCollectionItemService implements ICollectionItemService 
{
	private CollectionItemDao collectionItemDao;

	@Transactional
	public void merge(CollectionItem item)
	{
		this.collectionItemDao.merge(item);
	}
	
	@Transactional
	public void save(CollectionItem item)
	{
		this.collectionItemDao.save(item);
	}

	@Transactional
	public CollectionItem queryItemById(Long id)
	{
		return this.collectionItemDao.queryItemById(id);
	}
	
	@Transactional
	public void setCheckOperator(CollectionItem item, List<SysUserOut> newUsers)
	{
		item.getCheckOperator().clear();
		for (SysUserOut user : newUsers)
		{
			item.getCheckOperator().add(user);
		}
	}
	
	@Transactional
	public void setPublicityType(CollectionItem item, Integer publicityType)
	{
		item.setPublicityType(publicityType);
	}

	@Transactional
	public void delete(CollectionItem item) 
	{
		this.collectionItemDao.delete(item);
	}
	
	public void setCollectionItemDao(CollectionItemDao collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	@Override
	public List<CollectionItem> queryItemAll(Long courseId) 
	{
		return this.collectionItemDao.queryItemAll(courseId);
	}

	@Override
	public List<CollectionItem> queryItemByCheckOperatorId(Long userId, Long courseId) 
	{
		return this.collectionItemDao.queryItemByCheckOperatorId(userId, courseId);
	}

	@Override
	public List<CollectionItem> queryItemWhichNeedCheck(Long courseId) 
	{
		return this.collectionItemDao.queryItemWhichNeedCheck(courseId);
	}

	@Override
	public List<CollectionItem> queryItemWhichShallBePublicized(Long courseId) 
	{
		return this.collectionItemDao.queryItemWhichShallBePublicized(courseId);
	}
}
