package com.geniusye.studentmanage.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.dao.SystemStateDao;
import com.geniusye.studentmanage.service.ICalculateScoreService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;

@Transactional
public class DefaultSystemStateService implements ISystemStateService
{
	private SystemStateDao systemStateDao;
	
	private ICalculateScoreService calculateScoreService;
	
	/*
	 * 0 评分软件
	 * 1 助教软件
	 */
	
	private int systemType;
	
	/*
	1 设定阶段
	2 收集阶段
	3 认定阶段
	4 预留阶段
	5 预留阶段
	6 公示阶段
	7 生成报表
	8 运行结束
	*/
	
	@Transactional
	public SystemState getSystemState(Course course)
	{
		return this.systemStateDao.query(course.getId());
	}

	@Transactional
	public ErrorType changeToNextStep(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getState() == 8)
		{
			return ErrorType.SYSTEM_STATE_NOT_SUITABLE;
		}
		if (systemState.getState() == 3)
		{
			this.calculateScoreService.calculateAll(course);
			systemState.setState(6);
		}
		else
		{
			systemState.setState(systemState.getState() + 1);
		}
		this.systemStateDao.merge(systemState);
		return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public Integer isEnabled(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		return systemState.getEnabled();
	}
	
	@Transactional
	public void changeToDisabled(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		systemState.setEnabled(0);
		this.systemStateDao.merge(systemState);
	}
	
	@Transactional
	public void changeToEnabled(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		systemState.setEnabled(1);
		this.systemStateDao.merge(systemState);
	}
	
	@Transactional
	public CheckResult editCollectionItem(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() == 1 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult editPublicHearingsItem(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() == 1 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult editCollectionItemCheck(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() <= 2 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult editCollectionItemPublicity(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() < 6 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult querySelfCollectionItemInfo(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() == 2 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		else
			return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult fillCollectionItemInfo(CollectionItem item)
	{
		SystemState systemState = this.systemStateDao.query(item.getRelatedCourse().getId());
		if (systemState.getSystemType() == 1)
		{
			if (item != null && item.ifFillable())
				return CheckResult.Suitable;
			else
				return CheckResult.NotSuitable;
		}
		else if (systemState.getState() == 2 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		else
			return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult fillCollectionItemInfoCheck(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() == 3 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult fillPublicHearingsItemInfo(PublicHearingsItem item)
	{
		SystemState systemState = this.systemStateDao.query(item.getRelatedCourse().getId());
		if (systemState.getSystemType() == 1)
		{
			if (item.ifCheckable())
				return CheckResult.Suitable;
			else
				return CheckResult.NotSuitable;
		}
		else if ((systemState.getState() == 2 || systemState.getState() == 3) && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		else
			return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult queryPublicityInformation(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if (systemState.getState() == 6 && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult queryPublicHearingsItemInfoText(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if ((systemState.getState() == 6 || systemState.getState() == 7) && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult calculateScore(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}
	
	@Transactional
	public CheckResult generateReport(Course course)
	{
		SystemState systemState = this.systemStateDao.query(course.getId());
		if (systemState.getSystemType() == 1)
			return CheckResult.Suitable;
		else if ((systemState.getState() == 6 || systemState.getState() == 7) && systemState.getEnabled() == 1)
			return CheckResult.Suitable;
		return CheckResult.NotSuitable;
	}

	public void setCalculateScoreService(ICalculateScoreService calculateScoreService) {
		this.calculateScoreService = calculateScoreService;
	}
	
	public void setSystemStateDao(SystemStateDao systemStateDao) {
		this.systemStateDao = systemStateDao;
	}

	public int getSystemType() {
		return systemType;
	}

	public void setSystemType(int systemType) {
		this.systemType = systemType;
	}
}
