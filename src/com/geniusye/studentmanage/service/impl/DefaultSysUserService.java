package com.geniusye.studentmanage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.SysUser;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.SensitiveSysUserDao;
import com.geniusye.studentmanage.dao.SysUserDao;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;

@Transactional
public class DefaultSysUserService implements ISysUserService, UserDetailsService
{
	private SysUserDao sysUserDao;
	private SensitiveSysUserDao sensitiveSysUserDao;
	
	@Transactional
	public SysUserOut findCurrentUser()
	{
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println(username);
		if (username == null)
			return null;
		SysUserOut userOut = findUserOutByUsername(username);
		return userOut;
	}
	
	@Transactional
	public SysUser findByUsername(String username)
	{
		return this.sensitiveSysUserDao.findByUsername(username);
	}

	@Transactional
	public SysUserOut findUserOutByUsername(String username)
	{
		return this.sysUserDao.findUserOutByUsername(username);
	}

	@Transactional
	public SysUserOut findUserOutByWxOpenId(String wxOpenId)
	{
		return this.sysUserDao.findUserOutByWxOpenId(wxOpenId);
	}
	
	@Transactional
	public SysUserOut queryUserOutById(Long id)
	{
		return this.sysUserDao.queryUserOutById(id);
	}
	
	@Transactional
	public boolean changePasswordById(Long userId, String oldPassword, String newPassword)
	{
		SysUser user = this.sensitiveSysUserDao.queryById(userId);
		if (user.getPassword().equals(oldPassword))
		{
			user.setPassword(newPassword);
			this.sensitiveSysUserDao.merge(user);
			return true;
		}
		return false;
	}
	
	@Transactional
	public ErrorType appToResetUserPassword(String username)
	{
		SysUser sysUser = findByUsername(username);
		if (sysUser == null)
			return ErrorType.USER_NOT_EXIST;
		if (sysUser.getMailAddress() == null)
		{
			return ErrorType.NO_MAIL_REGISTERED;
		}
		if (sysUser.getResetPasswordTime() != null)
		{
			Date now = new Date();
			long diff = now.getTime() - sysUser.getResetPasswordTime().getTime();    
			long seconds = diff / 1000;
			if (seconds < 60)
				return ErrorType.RESET_CODE_APPLY_SO_CLOSE;
		}
		Random r = new Random();
		char[] b = new char[200];
		for (int i = 0; i < b.length; i++)
		{
			b[i] = (char) (r.nextDouble() * 26 + 65);
		}
		sysUser.setResetPasswordCode(String.valueOf(b));
		sysUser.setResetPasswordTime(new Date());
		this.sensitiveSysUserDao.merge(sysUser);
		return ErrorType.NO_ERROR;
	}
	
	@Transactional
	public ErrorType resetUserPassword(String username, String password, String resetCode)
	{
		SysUser sysUser = findByUsername(username);
		if (sysUser == null)
			return ErrorType.USER_NOT_EXIST;
		if (sysUser.getResetPasswordCode() == null || !sysUser.getResetPasswordCode().equals(resetCode))
		{
			return ErrorType.RESET_CODE_WRONG;
		}
		Date now = new Date();
		long diff = now.getTime() - sysUser.getResetPasswordTime().getTime();    
		long seconds = diff / 1000;
		if (seconds > 3600 * 48)
			return ErrorType.RESET_OUT_OF_DATE;
		sysUser.setPassword(password);
		sysUser.setResetPasswordCode(null);
		sysUser.setResetPasswordTime(null);
		this.sensitiveSysUserDao.merge(sysUser);
		return ErrorType.NO_ERROR;
	}
	
	public UserDetails loadUserByUsername(String username)
		throws UsernameNotFoundException, DataAccessException 
	{
		if (username == null || (username = username.trim()).equals("")) {
			throw new UsernameNotFoundException("");
		}
		SysUser uo = this.sensitiveSysUserDao.findByUsername(username);
		if (uo == null)
		{
			return null;
		}
		String password = uo.getPassword();
		String role = "ROLE_"+uo.getRole();
		List<GrantedAuthority> authsList = new ArrayList<GrantedAuthority>();
		authsList.add(new SimpleGrantedAuthority(role));
		
		boolean accountNonExpired = true;  
		boolean credentialsNonExpired = true;  
		boolean accountNonLocked = true;  
		
		UserDetails userdetails = new User(username, password, true, accountNonExpired, credentialsNonExpired, accountNonLocked, authsList); 
		return userdetails;
	}
	
	public void setSysUserDao(SysUserDao sysUserDao) {
		this.sysUserDao = sysUserDao;
	}

	public void setSensitiveSysUserDao(SensitiveSysUserDao sensitiveSysUserDao) {
		this.sensitiveSysUserDao = sensitiveSysUserDao;
	}
}
