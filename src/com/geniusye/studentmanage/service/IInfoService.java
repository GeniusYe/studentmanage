package com.geniusye.studentmanage.service;

import java.util.List;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemInfoText;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.ScoreStatus;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.util.ErrorType;

public interface IInfoService 
{
	/* CollectionItemInfo */
	
	public ErrorType setCollectionItemInfo(SysUserOut user, Long itemId, String text);
	
	//use in fill collectionItemInfoImage
	public CollectionItemInfo queryCollectionItemInfoByInfoProviderAndItemId(Long infoProvider, Long itemId);
	
	/* CollectionItemInfoCheck */
	
	public ErrorType setCollectionItemInfoCheck(SysUserOut user, Long itemId, SysUserOut infoTarget, Integer value, Boolean force);

	public ErrorType unsetCollectionItemInfoCheck(SysUserOut infoProvider, Long itemId, SysUserOut infoTarget, Boolean force);
	
	public List<Object> queryCollectionItemInfoCheckToAllUserForce(CollectionItem collectionItem);
	
	public List<Object> queryCollectionItemInfoCheckToAllUser(CollectionItem collectionItem, SysUserOut user);
	
	/* PublicHearingsItemInfo */
	
	public ErrorType setPublicHearingsItemInfo(SysUserOut user, Long itemId, SysUserOut infoTarget, Integer value);

	public ErrorType setPublicHearingsItemInfoText(SysUserOut user, Long itemId, SysUserOut infoTarget, String text);
	
	public List<Object> queryPublicHearingsItemInfoToAllUser(PublicHearingsItem publicHearingsItem, SysUserOut user);
	
	public List<Object> queryPublicHearingsItemInfoTextToAllUser(PublicHearingsItem publicHearingsItem, SysUserOut user);
	
	public List<PublicHearingsItemInfoText> queryPublicHearingsItemInfoTextByInfoTargetAndItemId(Long infoTarget, Long itemId);
	
	/* Score */
	
	public ErrorType setCollectionItemScore(SysUserOut user, Long itemId, Double value, Long rank);
	
	public CollectionItemScore queryCollectionItemScore(Long infoTarget, Long itemId);
	
	public List<CollectionItemScore> queryCollectionItemScores(Long userId, Long courseId);
	
	public ErrorType setPublicHearingsItemScore(SysUserOut user, Long itemId, Double value, Long rank);
	
	public PublicHearingsItemScore queryPublicHearingsItemScore(Long infoTarget, Long itemId);
	
	public List<PublicHearingsItemScore> queryPublicHearingsItemScores(Long userId, Long courseId);
	
	public ErrorType setTotalScore(SysUserOut user, Course course, Double value, Long rank);

	public TotalScore queryTotalScore(Long courseId, Long infoTarget);

	public ScoreStatus queryScoreStatus(CollectionItem item);
}
