package com.geniusye.studentmanage.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import com.geniusye.studentmanage.bean.SysUserOut;

public interface ICollectionItemInfoImageService 
{
	public Long createCollectionItemInfoImage(SysUserOut user, Long itemId, InputStream inputStream, String type) throws IOException, SQLException;
}
