package com.geniusye.studentmanage.service;

import com.geniusye.studentmanage.bean.Course;

public interface ICourseService 
{
	public Course queryById(Long courseId);
}
