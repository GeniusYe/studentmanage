package com.geniusye.studentmanage.service;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.util.ErrorType;

public interface ISystemStateService 
{
	public enum CheckResult
	{
		NotSuitable,
		Suitable
	}
	
	public SystemState getSystemState(Course course);

	public ErrorType changeToNextStep(Course course);
	
	public Integer isEnabled(Course course);
	
	public void changeToDisabled(Course course);
	
	public void changeToEnabled(Course course);
	
	public CheckResult editCollectionItem(Course course);
	
	public CheckResult editPublicHearingsItem(Course course);
	
	public CheckResult editCollectionItemCheck(Course course);
	
	public CheckResult editCollectionItemPublicity(Course course);
	
	public CheckResult querySelfCollectionItemInfo(Course course);
	
	public CheckResult fillCollectionItemInfo(CollectionItem item);
	
	public CheckResult fillCollectionItemInfoCheck(Course course);

	public CheckResult fillPublicHearingsItemInfo(PublicHearingsItem item);
	
	public CheckResult queryPublicityInformation(Course course);
	
	public CheckResult queryPublicHearingsItemInfoText(Course course);
	
	public CheckResult calculateScore(Course course);
	
	public CheckResult generateReport(Course course);

}
