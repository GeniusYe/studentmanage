package com.geniusye.studentmanage.service;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.util.PermissionLevel;

public interface IPermissionCheckService 
{
	/* CollectionItem Editing */
	
	@Transactional
	public PermissionLevel queryCollectionItemAll(SysUserOut operator, Course course);
	
	@Transactional
	public PermissionLevel createCollectionItem(SysUserOut operator, Course course, CollectionItem collectionItem);
	
	@Transactional
	public PermissionLevel deleteCollectionItem(SysUserOut operator, CollectionItem collectionItem);
	
	@Transactional
	public PermissionLevel updateCollectionItemCheck(SysUserOut operator, CollectionItem collectionItem);
	
	@Transactional
	public PermissionLevel updateCollectionItemPublicity(SysUserOut operator, CollectionItem collectionItem);
	
	/* PublicHearingsItem Editing */
	
	@Transactional
	public PermissionLevel queryPublicHearingsItemAll(SysUserOut operator, Course course);
	
	@Transactional
	public PermissionLevel createPublicHearingsItem(SysUserOut operator, Course course, PublicHearingsItem publicHearingsItem);

	@Transactional
	public PermissionLevel deletePublicHearingsItem(SysUserOut operator, PublicHearingsItem publicHearingsItem);
	
	/* Fill CollectionItem & PublicHearingsItem */

	@Transactional
	public PermissionLevel querySelfCollectionItemInfo(SysUserOut operator, Course course);

	@Transactional
	public PermissionLevel queryPublicHearingsItemAllForFill(SysUserOut operator, Course course);

	@Transactional
	public PermissionLevel fillCollectionItemInfo(SysUserOut operator, CollectionItem collectionItem);
	
	@Transactional
	public PermissionLevel fillPublicHearingsItemInfo(SysUserOut operator, PublicHearingsItem publicHearingsItem);
	
	/* Fill CollectionItemCheck */

	@Transactional
	public PermissionLevel queryCollectionItemInfoImage(SysUserOut operator, CollectionItem item, Long targetUserId);
	
	@Transactional
	public PermissionLevel queryCollectionItemWhichNeedCheck(SysUserOut operator, Course course);

	@Transactional
	public PermissionLevel fillCollectionItemInfoCheck(SysUserOut operator, Boolean force, CollectionItem collectionItem);
	
	/* Calculate Score */

	@Transactional
	public PermissionLevel calculateTotalScore(SysUserOut operator, Course course);
	
	@Transactional
	public PermissionLevel queryUsers(SysUserOut operator, Course course);

	@Transactional
	public PermissionLevel calculateCollectionItemScore(SysUserOut operator, CollectionItem collectionItem);

	@Transactional
	public PermissionLevel calculatePublicHearingsItemScore(SysUserOut operator, PublicHearingsItem publicHearingsItem);
	
	/* Publicity */
	
	@Transactional
	public PermissionLevel queryItemWhichShallBePublicity(SysUserOut operator, Course course);
	
	@Transactional
	public PermissionLevel queryPublicHearingsItemInfoText(PublicHearingsItem publicHearingsItem);
	
	@Transactional
	public PermissionLevel querySelfScore(SysUserOut operator, Course course, CollectionItem collectionItem, PublicHearingsItem publicHearingsItem);
	
	@Transactional
	public PermissionLevel wxQuerySelfScore(SysUserOut operator, Course course);
	
	@Transactional
	public PermissionLevel queryScoreStatus(SysUserOut operator, Course course, CollectionItem collectionItem, PublicHearingsItem publicHearingsItem);
	
	/* Report */

	@Transactional
	public PermissionLevel generateReport(SysUserOut operator, Course course);
	
}
