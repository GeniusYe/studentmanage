package com.geniusye.studentmanage.service;

import java.io.IOException;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.geniusye.studentmanage.bean.Course;

public interface IGenerateReportService 
{
	public void generateReport(Course course, String fileName) throws IOException, RowsExceededException, WriteException;
}
