package com.geniusye.studentmanage.bean;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="SYS_USER")
public class SysUserOut
{	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false, unique = true)
	private String username;
	
	@Column(nullable=true, unique = true)
	private String wxOpenId;
	
	@Column(nullable=false)
	private String name;
	
	@ManyToMany(mappedBy="checkOperator", fetch=FetchType.LAZY)
	private List<CollectionItem> checkItems;
	
//	@ManyToMany(mappedBy="adminUsers", fetch=FetchType.LAZY)
//	private List<Course> adminCourses;
//	
//	@ManyToMany(mappedBy="taUsers", fetch=FetchType.LAZY)
//	private List<Course> taCourses;
//	
//	@ManyToMany(mappedBy="stuUsers", fetch=FetchType.LAZY)
//	private List<Course> stuCourses;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<CollectionItemInfo> collectionItemInfo;
	
	//the CollectionItemInfoCheckProvider
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<CollectionItemInfoCheck> collectionItemInfoCheckProvider;

	//the CollectionItemInfoCheckTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<CollectionItemInfoCheck> collectionItemInfoCheckTarget;
	
	//the CollectionItemScoreTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<CollectionItemScore> collectionItemScoreTarget;
	
	//the PublicItemInfoProvider
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<PublicHearingsItemInfo> publicHearingsItemInfoProvider;
	
	//the PublicItemInfoTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<PublicHearingsItemInfo> publicHearingsItemInfoTarget;
	
	//the PublicItemInfoTestProvider
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<PublicHearingsItemInfoText> publicHearingsItemInfoTextProvider;
	
	//the PublicItemInfoTextTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<PublicHearingsItemInfoText> publicHearingsItemInfoTextTarget;
	
	//the PublicHearingsItemScoreTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<PublicHearingsItemScore> publicHearingsItemScoreTarget;
	
	//the TotalScoreTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<TotalScore> totalScoreTarget;
	
	//the EvalutionTaInfoStuProvider
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<EvalutionTaInfoStu> evalutionTaInfoStuProvider;
	
	//the EvalutionTaInfoStuTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<EvalutionTaInfoStu> evalutionTaInfoStuTarget;
	
	//the EvalutionTaInfoTeacherProvider
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<EvalutionTaInfoTeacher> evalutionTaInfoTeacherProvider;
	
	//the EvalutionTaInfoTeacherTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<EvalutionTaInfoTeacher> evalutionTaInfoTeacherTarget;
	
	//the TaScoreTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoTarget", fetch=FetchType.LAZY)
	private List<TaScore> taScoreTarget;
	
	//the EvalutionTaInfoTeacherTarget
	@OneToMany(cascade=CascadeType.ALL, mappedBy="infoProvider", fetch=FetchType.LAZY)
	private List<TaApplicationRecord> taApplicationRecordProvider;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CollectionItem> getCheckItems() {
		return checkItems;
	}

	public List<CollectionItemInfo> getCollectionItemInfo() {
		return collectionItemInfo;
	}

	public List<CollectionItemInfoCheck> getCollectionItemInfoCheckProvider() {
		return collectionItemInfoCheckProvider;
	}

	public List<CollectionItemInfoCheck> getCollectionItemInfoCheckTarget() {
		return collectionItemInfoCheckTarget;
	}

	public List<CollectionItemScore> getCollectionItemScoreTarget() {
		return collectionItemScoreTarget;
	}

	public List<PublicHearingsItemInfo> getPublicHearingsItemInfoProvider() {
		return publicHearingsItemInfoProvider;
	}

	public List<PublicHearingsItemInfo> getPublicHearingsItemInfoTarget() {
		return publicHearingsItemInfoTarget;
	}

	public List<PublicHearingsItemInfoText> getPublicHearingsItemInfoTextProvider() {
		return publicHearingsItemInfoTextProvider;
	}

	public List<PublicHearingsItemInfoText> getPublicHearingsItemInfoTextTarget() {
		return publicHearingsItemInfoTextTarget;
	}

	public List<PublicHearingsItemScore> getPublicHearingsItemScoreTarget() {
		return publicHearingsItemScoreTarget;
	}

	public List<TotalScore> getTotalScoreTarget() {
		return totalScoreTarget;
	}

	public List<EvalutionTaInfoStu> getEvalutionTaInfoStuProvider() {
		return evalutionTaInfoStuProvider;
	}

	public List<EvalutionTaInfoStu> getEvalutionTaInfoStuTarget() {
		return evalutionTaInfoStuTarget;
	}

	public List<EvalutionTaInfoTeacher> getEvalutionTaInfoTeacherProvider() {
		return evalutionTaInfoTeacherProvider;
	}

	public List<EvalutionTaInfoTeacher> getEvalutionTaInfoTeacherTarget() {
		return evalutionTaInfoTeacherTarget;
	}

	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public List<TaScore> getTaScoreTarget() {
		return taScoreTarget;
	}

	public List<TaApplicationRecord> getTaApplicationRecordProvider() {
		return taApplicationRecordProvider;
	}
}
