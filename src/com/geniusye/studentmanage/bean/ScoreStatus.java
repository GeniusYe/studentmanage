package com.geniusye.studentmanage.bean;

public class ScoreStatus 
{
	private Integer[][] intStatus;
	
	private Integer[] ticks;

	public Integer[] getTicks() {
		return ticks;
	}

	public void setTicks(Integer[] ticks) {
		this.ticks = ticks;
	}

	public Integer[][] getIntStatus() {
		return intStatus;
	}

	public void setIntStatus(Integer[][] intStatus) {
		this.intStatus = intStatus;
	}
}
