package com.geniusye.studentmanage.bean;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="COURSE")
public class Course 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="COURSE_USER_TEACHER",
			joinColumns=@JoinColumn(name="COURSE_ID"),
			inverseJoinColumns=@JoinColumn(name="SYS_USER_ID"))
	private List<SysUserOut> teacherUsers;

	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="COURSE_USER_ADMIN",
			joinColumns=@JoinColumn(name="COURSE_ID"),
			inverseJoinColumns=@JoinColumn(name="SYS_USER_ID"))
	private List<SysUserOut> adminUsers;

	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="COURSE_USER_TA",
			joinColumns=@JoinColumn(name="COURSE_ID"),
			inverseJoinColumns=@JoinColumn(name="SYS_USER_ID"))
	private List<SysUserOut> taUsers;

	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="COURSE_USER_STU",
			joinColumns=@JoinColumn(name="COURSE_ID"),
			inverseJoinColumns=@JoinColumn(name="SYS_USER_ID"))
	private List<SysUserOut> stuUsers;

	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
	@OneToOne(optional = true, cascade = CascadeType.ALL, mappedBy = "course", fetch = FetchType.LAZY)
	private SystemState systemState;
	
	public Course() { }
	public Course(String name)
	{
		this.name = name;
	}
	
	public Long getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean checkTeacherUsers(SysUserOut sysUser)
	{
		return checkUsers(sysUser, teacherUsers);
	}
	
	public boolean checkAdminUsers(SysUserOut sysUser)
	{
		return checkUsers(sysUser, adminUsers);
	}
	
	public boolean checkTaUsers(SysUserOut sysUser)
	{
		return checkUsers(sysUser, taUsers);
	}
	
	public boolean checkStuUsers(SysUserOut sysUser)
	{
		return checkUsers(sysUser, stuUsers);
	}
	
	private boolean checkUsers(SysUserOut sysUser, List<SysUserOut> list) 
	{
		if (list == null || sysUser == null)
			return false;
		boolean t = false;
		for (SysUserOut user : list)
		{
			if (user.getId().equals(sysUser.getId()))
			{
				t = true;
				break;
			}
		}
		return t;
	}
	
	public List<SysUserOut> getStuUsers() {
		return stuUsers;
	}
	public SystemState getSystemState() {
		return systemState;
	}
}
