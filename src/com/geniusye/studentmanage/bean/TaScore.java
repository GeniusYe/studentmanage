package com.geniusye.studentmanage.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="TA_SCORE")
public class TaScore 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@ForeignKey(name = "TA_SCORE_TARGET")
	private SysUserOut infoTarget;
	
	@ManyToOne
	@ForeignKey(name="TA_SCORE_COURSE")
	private Course relatedCourse;

	private Integer valuePatienceFromTeacher;
	
	private Integer valueCapabilityFromTeacher;

	private Integer valuePatienceFromStu;
	
	private Integer valueCapabilityFromStu;
	
	public TaScore() { }
	public TaScore(Course relatedCourse)
	{
		this.relatedCourse = relatedCourse;
	}
	public Course getRelatedCourse() {
		return relatedCourse;
	}
	public SysUserOut getInfoTarget() {
		return infoTarget;
	}
	public void setInfoTarget(SysUserOut infoTarget) {
		this.infoTarget = infoTarget;
	}
	public Long getId() {
		return id;
	}
	public Integer getValuePatienceFromTeacher() {
		return valuePatienceFromTeacher;
	}
	public void setValuePatienceFromTeacher(Integer valuePatienceFromTeacher) {
		this.valuePatienceFromTeacher = valuePatienceFromTeacher;
	}
	public Integer getValueCapabilityFromTeacher() {
		return valueCapabilityFromTeacher;
	}
	public void setValueCapabilityFromTeacher(Integer valueCapabilityFromTeacher) {
		this.valueCapabilityFromTeacher = valueCapabilityFromTeacher;
	}
	public Integer getValuePatienceFromStu() {
		return valuePatienceFromStu;
	}
	public void setValuePatienceFromStu(Integer valuePatienceFromStu) {
		this.valuePatienceFromStu = valuePatienceFromStu;
	}
	public Integer getValueCapabilityFromStu() {
		return valueCapabilityFromStu;
	}
	public void setValueCapabilityFromStu(Integer valueCapabilityFromStu) {
		this.valueCapabilityFromStu = valueCapabilityFromStu;
	}
}
