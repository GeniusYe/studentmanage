package com.geniusye.studentmanage.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="EVALUTION_TA_INFO_TEACHER")
public class EvalutionTaInfoTeacher 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@ForeignKey(name = "EVALUTION_TEACHER_INFO_TEACHER_PROVIDER")
	private SysUserOut infoProvider;
	
	@ManyToOne
	@ForeignKey(name = "EVALUTION_TEACHER_INFO_TEACHER_TARGET")
	private SysUserOut infoTarget;
	
	@ManyToOne
	@ForeignKey(name="EVALUTION_TA_TEACHER_COURSE")
	private Course relatedCourse;
	
	private Integer valuePatience;
	
	private Integer valueCapability;
	
	public EvalutionTaInfoTeacher() { }
	public EvalutionTaInfoTeacher(Course relatedCourse)
	{
		this.relatedCourse = relatedCourse;
	}
	public SysUserOut getInfoProvider() {
		return infoProvider;
	}
	public void setInfoProvider(SysUserOut infoProvider) {
		this.infoProvider = infoProvider;
	}
	public Course getRelatedCourse() {
		return relatedCourse;
	}
	public SysUserOut getInfoTarget() {
		return infoTarget;
	}
	public void setInfoTarget(SysUserOut infoTarget) {
		this.infoTarget = infoTarget;
	}
	public Integer getValuePatience() {
		return valuePatience;
	}
	public void setValuePatience(Integer valuePatience) {
		this.valuePatience = valuePatience;
	}
	public Integer getValueCapability() {
		return valueCapability;
	}
	public void setValueCapability(Integer valueCapability) {
		this.valueCapability = valueCapability;
	}
	public Long getId() {
		return id;
	}
}
