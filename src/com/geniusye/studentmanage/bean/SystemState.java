package com.geniusye.studentmanage.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="SYSTEM_STATE")
public class SystemState 
{
	
	//This value must be the same as systemType, otherwise, it would cause cache miss or interaction
	@SuppressWarnings("unused")
	@Id
	private Long id;

	@OneToOne
	private Course course;
	
	@Column(nullable=false)
	private Integer state;
	
	@Column(nullable=false)
	private Integer enabled;
	
	@Column(nullable=false)
	private Integer systemType;
	
	public Course getCourse() {
		return course;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getSystemType() {
		return systemType;
	}

	public void setSystemType(Integer systemType) {
		this.systemType = systemType;
	}

}
