package com.geniusye.studentmanage.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
@Entity
@Table(name="TA_APPLICATOIN_RECORD")
public class TaApplicationRecord 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@ForeignKey(name = "TA_APPLICATION_INFO_PROVIDER")
	private SysUserOut infoProvider;
	
	@ManyToOne
	@ForeignKey(name="TA_APPLICATION_COURSE")
	private Course relatedCourse;
	
	/* 0 - pending, 1 - cancelled, 2 - accepted, 3 - rejected */
	private Integer status;
	
	public TaApplicationRecord() { }
	public TaApplicationRecord(Course relatedCourse)
	{
		this.relatedCourse = relatedCourse;
	}
	public Course getRelatedCourse() {
		return relatedCourse;
	}
	public Long getId() {
		return id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public SysUserOut getInfoProvider() {
		return infoProvider;
	}
	public void setInfoProvider(SysUserOut infoProvider) {
		this.infoProvider = infoProvider;
	}
}
