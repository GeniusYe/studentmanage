package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryCollectionItemInfoCheckToAllUsersAction extends ActionSupport 
{
	private ISysUserService sysUserService;
	private ICollectionItemService collectionItemService;
	private IPermissionCheckService permissionCheckService;
	private IInfoService infoService;
	
	private Long itemId;
	
	private List<Object> results;
	
	private CollectionItem collectionItem;

	@Transactional
	public String executeWithoutForce()
	{
		return innerExecute(false);
	}
	
	@Transactional
	public String executeWithForce()
	{
		return innerExecute(true);
	}
	
	private String innerExecute(boolean force)
	{
		if (this.itemId != null)
			this.collectionItem = this.collectionItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillCollectionItemInfoCheck(currentUser, force, this.collectionItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (force)
			this.results = this.infoService.queryCollectionItemInfoCheckToAllUserForce(this.collectionItem);
		else
			this.results = this.infoService.queryCollectionItemInfoCheckToAllUser(this.collectionItem, currentUser);
		return Action.SUCCESS;
	}
	
	public List<Object> getResults() {
		return results;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public Long getItemId() {
		return itemId;
	}

	public CollectionItem getCollectionItem() {
		return collectionItem;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}
	
}
