package com.geniusye.studentmanage.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class CreateCollectionItemCheckAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private Long id;
	
	private Long[] checkOperatorId;
	
	@Transactional
	public String execute()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		CollectionItem item = null;
		if (this.id != null && this.id > 0)
			item = this.collectionItemService.queryItemById(this.id);

		PermissionLevel permissionLevel = permissionCheckService.updateCollectionItemCheck(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		Course course = item.getRelatedCourse();
		
		List<SysUserOut> newUsers = new ArrayList<SysUserOut>();
		if (this.checkOperatorId != null && this.checkOperatorId.length != 0)
		{
			int k = this.checkOperatorId.length;
			SysUserOut tempUser;
			for (int i = 0; i < k; i++)
			{
				tempUser = this.sysUserService.queryUserOutById(this.checkOperatorId[i]);
				if (!course.checkTaUsers(tempUser))
				{
					FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_TA_LIST);
					return Action.INPUT;
				}
				newUsers.add(tempUser);
			}
		}
		this.collectionItemService.setCheckOperator(item, newUsers);

		return Action.SUCCESS;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setCheckOperatorId(Long[] checkOperatorId) {
		this.checkOperatorId = checkOperatorId;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
