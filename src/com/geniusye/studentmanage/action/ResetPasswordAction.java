package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ResetPasswordAction extends ActionSupport
{
	private ISysUserService sysUserService;
	
	private String username, newPassword, resetCode;
	
	private SystemState systemState;

	@Transactional
	public String execute()
	{
		if (newPassword.length() < 5 || newPassword.length() > 16)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PASSWORD_LENGTH_WRONG);
			return Action.INPUT;
		}
		ErrorType errorCode;
		if ((errorCode = this.sysUserService.resetUserPassword(username, newPassword, resetCode)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, errorCode);
			return Action.INPUT;
		}
	}
	
	public String gotoResetPasswordPage()
	{
		return Action.SUCCESS;
	}
	
	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public String getResetCode() {
		return resetCode;
	}
}
