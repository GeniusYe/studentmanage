package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ChangeCurrentUserPasswordAction extends ActionSupport 
{
	private ISysUserService sysUserService;
	private String oldPassword, newPassword;
	
	private SystemState systemState;

	@Transactional
	public String execute()
	{
		SysUserOut user = this.sysUserService.findCurrentUser();
		if (this.sysUserService.changePasswordById(user.getId(), oldPassword, newPassword))
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.OLD_PASSWORD_WRONG);
			return Action.INPUT;
		}
	}

	@Transactional
	public String gotoChangePasswordPage()
	{
		return Action.SUCCESS;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
	}

	public SystemState getSystemState() {
		return systemState;
	}
}
