package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryPublicHearingsItemAction extends ActionSupport 
{
	private IPublicHearingsItemService publicHearingsItemService;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	
	private List<PublicHearingsItem> publicHearingsItemList;
	private SystemState systemState;

	private Long courseId;
	
	@Transactional
	public String queryItemAll()
	{
		return queryItemAll(0);
	}
	
	@Transactional
	public String queryItemAllForFill()
	{
		return queryItemAll(1);
	}
	
	/**
	 * @param querySource 0 -- admin, 1 -- stu query for fill
	 */
	@Transactional
	private String queryItemAll(int querySource)
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = null;
		if (querySource == 1)
			permissionLevel = permissionCheckService.queryPublicHearingsItemAllForFill(currentUser, course);
		else
			permissionLevel = permissionCheckService.queryPublicHearingsItemAll(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		this.publicHearingsItemList = this.publicHearingsItemService.queryItemAll(courseId);
		return Action.SUCCESS;
	}

	public SystemState getSystemState() {
		return this.systemState;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public List<PublicHearingsItem> getPublicHearingsItemList() {
		return publicHearingsItemList;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public Long getCourseId() {
		return courseId;
	}
}
