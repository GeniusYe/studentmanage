package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryCollectionItemOneAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private ISysUserService sysUserService;
	
	private Long id;
	private CollectionItem collectionItem;

	@Transactional
	public String queryItemById()
	{
		if (this.id == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		this.collectionItem = this.collectionItemService.queryItemById(this.id);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		if (!this.collectionItem.getRelatedCourse().checkAdminUsers(currentUser))
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
			return Action.INPUT;
		}
		return Action.SUCCESS;
	}

	@Transactional
	public String queryItemWithCheckOperatorsById()
	{
		if (this.id == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_ITEM_NOT_EXIST);
			return Action.INPUT;
		}
		this.collectionItem = this.collectionItemService.queryItemById(this.id);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		if (!this.collectionItem.getRelatedCourse().checkAdminUsers(currentUser))
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
			return Action.INPUT;
		}
		this.collectionItem.getCheckOperator().size();
		return Action.SUCCESS;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public CollectionItem getCollectionItem() {
		return this.collectionItem;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
}
