package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryPublicHearingsItemInfoAndTextToAllUsersAction extends ActionSupport 
{
	private ISysUserService sysUserService;
	private IPublicHearingsItemService publicHearingsItemService;
	private IInfoService infoService;
	private IPermissionCheckService permissionCheckService;
	
	private Long itemId;
	
	private List<Object> results;
	
	private PublicHearingsItem publicHearingsItem;
	private SystemState systemState;

	@Transactional
	public String execute()
	{
		if (this.itemId != null)
			this.publicHearingsItem = this.publicHearingsItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillPublicHearingsItemInfo(currentUser, this.publicHearingsItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		if (this.publicHearingsItem.getType() != null)
		{
			if (this.publicHearingsItem.getType() == 0) 
				this.results = this.infoService.queryPublicHearingsItemInfoToAllUser(this.publicHearingsItem, currentUser);
			else if (this.publicHearingsItem.getType() == 1)
				this.results = this.infoService.queryPublicHearingsItemInfoTextToAllUser(this.publicHearingsItem, currentUser);
		}
		return Action.SUCCESS;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public PublicHearingsItem getPublicHearingsItem() {
		return publicHearingsItem;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public List<Object> getResults() {
		return results;
	}
	
}
