package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class FillPublicHearingsItemInfoAction extends ActionSupport 
{
	private IPublicHearingsItemService publicHearingsItemService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	private IInfoService infoService;
	
	private Long itemId;
	
	private Long infoTarget;
	
	private Integer value;
	
	private String text;

	@Transactional
	public String fillInfo()
	{
		PublicHearingsItem item = null;
		if (this.itemId != null)
			item = this.publicHearingsItemService.queryItemById(itemId);
		if (item == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PUBLIC_HEARINGS_ITEM_NOT_EXIST);
			return Action.INPUT;
		}
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillPublicHearingsItemInfo(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (this.value == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		SysUserOut user = this.sysUserService.findCurrentUser();
		SysUserOut infoTargetUser = this.sysUserService.queryUserOutById(this.infoTarget);
		if (infoTargetUser == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_EXIST);
			return Action.INPUT;
		}
		ErrorType errorType;
		if ((errorType = this.infoService.setPublicHearingsItemInfo(user, this.itemId, infoTargetUser, this.value)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, errorType);
			return Action.ERROR;
		}
	}

	@Transactional
	public String fillText()
	{
		PublicHearingsItem item = null;
		if (this.itemId != null)
			item = this.publicHearingsItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillPublicHearingsItemInfo(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (this.text == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		SysUserOut user = this.sysUserService.findCurrentUser();
		SysUserOut infoTargetUser = this.sysUserService.queryUserOutById(this.infoTarget);
		if (infoTargetUser == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_EXIST);
			return Action.INPUT;
		}
		ErrorType errorType;
		if ((errorType = this.infoService.setPublicHearingsItemInfoText(user, this.itemId, infoTargetUser, this.text)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, errorType);
			return Action.ERROR;
		}
	}
	
	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
	
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	
	public void setInfoTarget(Long infoTarget) {
		this.infoTarget = infoTarget;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public Long getInfoTarget() {
		return infoTarget;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}
}
