package com.geniusye.studentmanage.action;

import java.util.Iterator;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class LoginAction extends ActionSupport {

	@Transactional
	public String execute() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null && auth.isAuthenticated()) 
		{
			Iterator<? extends GrantedAuthority> iter = auth.getAuthorities().iterator();
			while(iter.hasNext())
				return iter.next().getAuthority();
		}
		return Action.ERROR;
	}
}
