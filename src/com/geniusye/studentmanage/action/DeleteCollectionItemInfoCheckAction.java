package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class DeleteCollectionItemInfoCheckAction extends ActionSupport
{
	private ICollectionItemService collectionItemService;
	private IInfoService infoService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private Long itemId;
	
	private Long infoTargetId;
	
	@Transactional
	public String execute()
	{
		return innerExecute(false);
	}
	
	@Transactional
	public String deleteForcedItem()
	{
		return innerExecute(true);
	}
	
	private String innerExecute(boolean force)
	{
		CollectionItem item = null;
		if (this.itemId != null)
			item = this.collectionItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillCollectionItemInfoCheck(currentUser, force, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		SysUserOut infoTarget = this.sysUserService.queryUserOutById(infoTargetId);
		if (infoTarget == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_EXIST);
			return Action.SUCCESS;
		}
		ErrorType errorType;
		if ((errorType = this.infoService.unsetCollectionItemInfoCheck(currentUser, itemId, infoTarget, force)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, errorType);
			return Action.ERROR;
		}
	}
	
	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public Long getInfoTargetId() {
		return infoTargetId;
	}

	public void setInfoTargetId(Long infoTargetId) {
		this.infoTargetId = infoTargetId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

}
