package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ICalculateScoreService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class CalculateScoreAction extends ActionSupport
{
	private ICalculateScoreService calculateScoreService;
	private ICollectionItemService collectionItemService;
	private IPublicHearingsItemService publicHearingsItemService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;

	private Long itemId;
	
	private Long courseId;
	
	@Transactional
	public String calculateAll()
	{
		Course course = this.courseService.queryById(courseId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.calculateTotalScore(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		ErrorType errorCode;
		if ((errorCode = this.calculateScoreService.calculateTotalScore(course)) != ErrorType.NO_ERROR)
		{
			FieldErrorGenerator.addFieldError(this, errorCode);
			return Action.INPUT;
		}
		else
			return Action.SUCCESS;
	}
	
	@Transactional
	public String calculateCollectionItemByItemId()
	{
		CollectionItem collectionItem = null;
		if (itemId != null && itemId > 0)
			collectionItem = this.collectionItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.calculateCollectionItemScore(currentUser, collectionItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		ErrorType errorCode;
		if ((errorCode = this.calculateScoreService.calculateCollectionItem(collectionItem)) != ErrorType.NO_ERROR)
		{
			FieldErrorGenerator.addFieldError(this, errorCode);
			return Action.INPUT;
		}
		else
			return Action.SUCCESS;
	}
	
	@Transactional
	public String calculatePublicHearingsItemByItemId()
	{
		PublicHearingsItem publicHearingsItem = null;
		if (itemId != null && itemId > 0)
			publicHearingsItem = this.publicHearingsItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.calculatePublicHearingsItemScore(currentUser, publicHearingsItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		ErrorType errorCode;
		if ((errorCode = this.calculateScoreService.calculatePublicHearingsItem(publicHearingsItem)) != ErrorType.NO_ERROR)
		{
			FieldErrorGenerator.addFieldError(this, errorCode);
			return Action.INPUT;
		}
		else
			return Action.SUCCESS;
	}

	public void setCalculateScoreService(ICalculateScoreService calculateScoreService) {
		this.calculateScoreService = calculateScoreService;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setPublicHearingsItemService(IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

}
