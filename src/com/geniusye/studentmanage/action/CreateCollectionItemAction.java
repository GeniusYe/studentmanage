package com.geniusye.studentmanage.action;

import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class CreateCollectionItemAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	
	private Long id;
	private Long courseId;
	private String itemId;
	private String name;
	private String contents;
	private String checkRequirement;
	private String codeDirectory;
	private Short type;
	private Integer minScore, maxScore, defaultScore, averageScore, deltaScore, weight;
	private String ifUserSubmitText, ifUserSubmitPic, ifUserSubmitCode;
	private Date startFillingTime, endFillingTime;
	
	private CollectionItem collectionItem;

	@Transactional
	public String execute()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.createCollectionItem(currentUser, course, collectionItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		this.collectionItem = null;
		if (id != null && id > 0)
			collectionItem = this.collectionItemService.queryItemById(id);
		if (collectionItem == null)
		{
			collectionItem = new CollectionItem(course);
		}
		collectionItem.setItemId(itemId);
		collectionItem.setName(name);
		collectionItem.setContents(contents);
		collectionItem.setCheckRequirement(checkRequirement);
		collectionItem.setCodeDirectory(codeDirectory);
		collectionItem.setType(type);
		collectionItem.setAverageScore(averageScore);
		collectionItem.setMaxScore(maxScore);
		collectionItem.setMinScore(minScore);
		collectionItem.setDefaultScore(defaultScore);
		collectionItem.setDeltaScore(deltaScore);
		collectionItem.setWeight(weight);
		collectionItem.setStartFillingTime(startFillingTime);
		collectionItem.setEndFillingTime(endFillingTime);
		if (ifUserSubmitText != null && ifUserSubmitText.equals("on"))
			collectionItem.setIfUserSubmitText(true);
		else
			collectionItem.setIfUserSubmitText(false);
		if (ifUserSubmitPic != null && ifUserSubmitPic.equals("on"))
			collectionItem.setIfUserSubmitPic(true);
		else
			collectionItem.setIfUserSubmitPic(false);
		if (ifUserSubmitCode != null && ifUserSubmitCode.equals("on"))
			collectionItem.setIfUserSubmitCode(true);
		else
			collectionItem.setIfUserSubmitCode(false);
		if (collectionItem.getId() != null && collectionItem.getId() > 0)
			this.collectionItemService.merge(collectionItem);
		else
			this.collectionItemService.save(collectionItem);
		return Action.SUCCESS;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public CollectionItem getCollectionItem() {
		return collectionItem;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setCollectionItem(CollectionItem collectionItem) {
		this.collectionItem = collectionItem;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public void setCheckRequirement(String checkRequirement) {
		this.checkRequirement = checkRequirement;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public void setMinScore(Integer minScore) {
		this.minScore = minScore;
	}

	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}

	public void setDefaultScore(Integer defaultScore) {
		this.defaultScore = defaultScore;
	}

	public void setAverageScore(Integer averageScore) {
		this.averageScore = averageScore;
	}

	public void setDeltaScore(Integer deltaScore) {
		this.deltaScore = deltaScore;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public void setIfUserSubmitText(String ifUserSubmitText) {
		this.ifUserSubmitText = ifUserSubmitText;
	}

	public void setIfUserSubmitPic(String ifUserSubmitPic) {
		this.ifUserSubmitPic = ifUserSubmitPic;
	}

	public void setCodeDirectory(String codeDirectory) {
		this.codeDirectory = codeDirectory;
	}

	public void setIfUserSubmitCode(String ifUserSubmitCode) {
		this.ifUserSubmitCode = ifUserSubmitCode;
	}

	public void setStartFillingTime(Date startFillingTime) {
		this.startFillingTime = startFillingTime;
	}

	public void setEndFillingTime(Date endFillingTime) {
		this.endFillingTime = endFillingTime;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

}
