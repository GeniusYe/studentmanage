package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class CreatePublicHearingsItemAction extends ActionSupport 
{
	private IPublicHearingsItemService publicHearingsItemService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	
	private Long id;
	private Long courseId;
	private String itemId;
	private String name;
	private String contents;
	private Short type;
	private Integer minScore, maxScore, defaultScore, averageScore, deltaScore, weight;
	
	private PublicHearingsItem publicHearingsItem;

	@Transactional
	public String execute()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.createPublicHearingsItem(currentUser, course, publicHearingsItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		this.publicHearingsItem = null;
		if (id != null && id > 0)
			publicHearingsItem = this.publicHearingsItemService.queryItemById(id);
		if (publicHearingsItem == null)
			publicHearingsItem = new PublicHearingsItem(course);
		publicHearingsItem.setItemId(itemId);
		publicHearingsItem.setName(name);
		publicHearingsItem.setContents(contents);
		publicHearingsItem.setType(type);
		publicHearingsItem.setAverageScore(averageScore);
		publicHearingsItem.setMaxScore(maxScore);
		publicHearingsItem.setMinScore(minScore);
		publicHearingsItem.setDefaultScore(defaultScore);
		publicHearingsItem.setDeltaScore(deltaScore);
		publicHearingsItem.setWeight(weight);
		if (publicHearingsItem.getId() != null && publicHearingsItem.getId() > 0)
			this.publicHearingsItemService.merge(publicHearingsItem);
		else
			this.publicHearingsItemService.save(publicHearingsItem);
		return Action.SUCCESS;
	}

	public PublicHearingsItem getPublicHearingsItem() {
		return publicHearingsItem;
	}

	public void setPublicHearingsItem(PublicHearingsItem publicHearingsItem) {
		this.publicHearingsItem = publicHearingsItem;
	}

	public void setPublicHearingsItemService(IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public void setMinScore(Integer minScore) {
		this.minScore = minScore;
	}

	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}

	public void setDefaultScore(Integer defaultScore) {
		this.defaultScore = defaultScore;
	}

	public void setAverageScore(Integer averageScore) {
		this.averageScore = averageScore;
	}

	public void setDeltaScore(Integer deltaScore) {
		this.deltaScore = deltaScore;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
}
