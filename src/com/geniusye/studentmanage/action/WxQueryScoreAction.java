package com.geniusye.studentmanage.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class WxQueryScoreAction extends ActionSupport
{
	private IPermissionCheckService permissionCheckService;
	private ICourseService courseService;
	private ISysUserService sysUserService;
	private IInfoService infoService;
	
	private Long courseId;
	
	private String wxOpenId;
	
	private List<QueryResult> results;
	private QueryResult totalResult;
	
	@Transactional
	public String execute()
	{
		SysUserOut wxUser = this.sysUserService.findUserOutByWxOpenId(wxOpenId);
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.wxQuerySelfScore(wxUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		TotalScore totalScore = this.infoService.queryTotalScore(courseId, wxUser.getId());
		List<CollectionItemScore> collectionItemScores = this.infoService.queryCollectionItemScores(wxUser.getId(), courseId);
		List<PublicHearingsItemScore> publicHearingsItemScores = this.infoService.queryPublicHearingsItemScores(wxUser.getId(), courseId);
		
		results = new ArrayList<QueryResult>();
		for (CollectionItemScore item : collectionItemScores)
		{
			CollectionItem cItem = item.getCollectionItem();
			if (cItem.getPublicityType() >= 2)
				results.add(new QueryResult(cItem.getItemId(), cItem.getName(), item.getValue(), item.getRank()));
			else if (cItem.getPublicityType() >= 1)
				results.add(new QueryResult(cItem.getItemId(), cItem.getName(), item.getValue(), null));
		}
		for (PublicHearingsItemScore item : publicHearingsItemScores)
		{
			PublicHearingsItem cItem = item.getPublicHearingsItem();
			results.add(new QueryResult(cItem.getItemId(), cItem.getName(), item.getValue(), item.getRank()));
		}
		
		if (totalScore != null)
			totalResult = new QueryResult("", "", totalScore.getValue(), totalScore.getRank());
		
		return Action.SUCCESS;
	}
	
	public class QueryResult
	{
		private String itemNumber;
		
		private String itemName;
		
		private Double value;

		private Long rank;
		
		public QueryResult(String itemNumber, String itemName, Double value, Long long1)
		{
			this.itemNumber = itemNumber;
			this.itemName = itemName;
			this.value = value;
			this.rank = long1;
		}
		
		public String getItemNumber() {
			return itemNumber;
		}

		public void setItemNumber(String itemNumber) {
			this.itemNumber = itemNumber;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public Double getValue() {
			return value;
		}

		public void setValue(Double value) {
			this.value = value;
		}

		public Long getRank() {
			return rank;
		}

		public void setRank(Long rank) {
			this.rank = rank;
		}
	}

	public List<QueryResult> getResults() {
		return results;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	public QueryResult getTotalResult() {
		return totalResult;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}
}
