package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemScore;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemScore;
import com.geniusye.studentmanage.bean.ScoreStatus;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.bean.TotalScore;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryScoreByItemIdAction extends ActionSupport 
{
	private Integer type;
	/*
	 * 0 all
	 * 1 collectionItem
	 * 2 publicHearingsItem
	 */
	
	private Long itemId;
	private Long courseId;
	private Integer totalUserSize;
	
	private List<CollectionItemScore> collectionItemScores;
	private CollectionItemScore collectionItemScore;
	private PublicHearingsItemScore publicHearingsItemScore;
	private SystemState systemState;
	private CollectionItem collectionItem;
	private PublicHearingsItem publicHearingsItem;
	private TotalScore totalScore;
	private ScoreStatus scoreStatus;
	
	private ICollectionItemService collectionItemService;
	private IPublicHearingsItemService publicHearingsItemService;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	private IInfoService infoService;

	@Transactional
	public String queryScore()
	{
		return innerExecute(0);
	}

	@Transactional
	public String queryScoreStatus()
	{
		return innerExecute(1);
	}

	@Transactional
	public String innerExecute(int switches)
	{
		if (type == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		if (itemId == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}

		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = null;
		if (type == 0)
			course = this.courseService.queryById(courseId);
		
		//switches == 0 user switches == 1 admin
		if (switches == 0)
		{
			if (type == 1)
			{
				this.collectionItem = this.collectionItemService.queryItemById(itemId);
				course = this.collectionItem.getRelatedCourse();
			}
			else if (type == 2)
			{
				this.publicHearingsItem = this.publicHearingsItemService.queryItemById(itemId);
				course = this.publicHearingsItem.getRelatedCourse();
			}
			PermissionLevel permissionLevel = permissionCheckService.querySelfScore(currentUser, course, this.collectionItem, this.publicHearingsItem);
			if (!permissionLevel.canExecute())
			{
				FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
				return Action.ERROR;
			}
			innerQueryScore(course);
		}
		else if (switches == 1)
		{
			if (type == 1)
			{
				this.collectionItem = this.collectionItemService.queryItemById(itemId);
				course = this.collectionItem.getRelatedCourse();
			}
			PermissionLevel permissionLevel = permissionCheckService.queryScoreStatus(currentUser, course, this.collectionItem, this.publicHearingsItem);
			if (!permissionLevel.canExecute())
			{
				FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
				return Action.ERROR;
			}
			innerQueryScoreStatus(course);
		}
		this.totalUserSize = course.getStuUsers().size();
		this.systemState = this.systemStateService.getSystemState(course);
		return Action.SUCCESS;
	}

	@Transactional
	private void innerQueryScore(Course course) 
	{
		Integer publicityType;
		if (type == 0)
		{
			this.totalScore = this.infoService.queryTotalScore(course.getId(), this.sysUserService.findCurrentUser().getId());
		}
		else if (type == 1)
		{
			publicityType = this.collectionItem.getPublicityType();
			if (publicityType.equals(1) || publicityType.equals(2))
			{
				this.collectionItemScore = this.infoService.queryCollectionItemScore(this.sysUserService.findCurrentUser().getId(), itemId);			
//				if (publicityType.equals(1))
//					this.collectionItemScore.setRank(null);
			}
			else if (publicityType.equals(3))
			{
				this.collectionItemScore = this.infoService.queryCollectionItemScore(this.sysUserService.findCurrentUser().getId(), itemId);
				//this.collectionItemScores = this.infoService.queryCollectionItemScores(itemId);
			}
		}
		else if (type == 2)
		{
			this.publicHearingsItemScore = this.infoService.queryPublicHearingsItemScore(this.sysUserService.findCurrentUser().getId(), itemId);
		}
	}

	@Transactional
	private void innerQueryScoreStatus(Course course) 
	{
		if (type == 1)
		{
			this.scoreStatus = this.infoService.queryScoreStatus(this.collectionItem);
		}
	}

	public List<CollectionItemScore> getCollectionItemScores() {
		return collectionItemScores;
	}

	public CollectionItemScore getCollectionItemScore() {
		return collectionItemScore;
	}

	public PublicHearingsItemScore getPublicHearingsItemScore() {
		return publicHearingsItemScore;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public CollectionItem getCollectionItem() {
		return collectionItem;
	}

	public PublicHearingsItem getPublicHearingsItem() {
		return publicHearingsItem;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public Integer getType() {
		return type;
	}

	public Long getItemId() {
		return itemId;
	}

	public Integer getTotalUserSize() {
		return totalUserSize;
	}

	public TotalScore getTotalScore() {
		return totalScore;
	}
	
	public ScoreStatus getScoreStatus() {
		return scoreStatus;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}
}
