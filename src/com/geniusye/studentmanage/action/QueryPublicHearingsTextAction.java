package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.PublicHearingsItemInfoText;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryPublicHearingsTextAction extends ActionSupport 
{
	private IPublicHearingsItemService publicHearingsItemService;
	private IInfoService infoService;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private Long itemId;
	
	private List<PublicHearingsItemInfoText> publicHearingsItemInfoTextList;

	private SystemState systemState;

	@Transactional
	public String QueryPublicHearingsTextByItemId()
	{
		PublicHearingsItem publicHearingsItem = null;
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		if (this.itemId != null)
			publicHearingsItem = this.publicHearingsItemService.queryItemById(itemId);
		if (!publicHearingsItem.getRelatedCourse().checkStuUsers(currentUser))
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_STU_LIST);
			return Action.INPUT;
		}
		this.systemState = this.systemStateService.getSystemState(publicHearingsItem.getRelatedCourse());
		PermissionLevel permissionLevel = permissionCheckService.queryPublicHearingsItemInfoText(publicHearingsItem);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.publicHearingsItemInfoTextList = this.infoService.queryPublicHearingsItemInfoTextByInfoTargetAndItemId(this.sysUserService.findCurrentUser().getId(), itemId);
		return Action.SUCCESS;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public List<PublicHearingsItemInfoText> getPublicHearingsItemInfoTextList() {
		return publicHearingsItemInfoTextList;
	}
	
	public void setInfoService(
			IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setPublicHearingsItemService(IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}
}
