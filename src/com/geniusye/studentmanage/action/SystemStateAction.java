package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class SystemStateAction extends ActionSupport 
{
	private ISystemStateService systemStateService;
	private ICourseService courseService;

	private SystemState systemState;
	private Long courseId;

	@Transactional
	public String changeToNextStep()
	{
		ErrorType errorCode;
		Course course = this.courseService.queryById(courseId);
		this.systemState = this.systemStateService.getSystemState(course);
		if (!this.systemStateService.isEnabled(course).equals(1))
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.SYSTEM_STATE_NOT_SUITABLE);
			return Action.INPUT;
		}
		else
		{
			this.systemStateService.changeToDisabled(course);
			if ((errorCode = this.systemStateService.changeToNextStep(course)) == ErrorType.NO_ERROR)
			{
				this.systemStateService.changeToEnabled(course);
				this.systemState = this.systemStateService.getSystemState(course);
				return Action.SUCCESS;
			}
			this.systemStateService.changeToEnabled(course);
		}
		this.systemState = this.systemStateService.getSystemState(course);
		FieldErrorGenerator.addFieldError(this, errorCode);
		return Action.INPUT;
	}

	@Transactional
	public String querySystemState()
	{
		Course course = this.courseService.queryById(courseId);
		this.systemState = this.systemStateService.getSystemState(course);
		return Action.SUCCESS;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public Long getCourseId() {
		return courseId;
	}

}
