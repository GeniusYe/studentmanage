package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class FillCollectionItemInfoCheckAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private IInfoService infoService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private Long itemId;
	
	private Long infoTarget;
	
	private Integer value;

	@Transactional
	public String execute()
	{
		return innerExecute(false);
	}

	@Transactional
	public String addForcedItem()
	{
		return innerExecute(true);
	}
	
	private String innerExecute(Boolean force)
	{
		CollectionItem item = null;
		if (this.itemId != null)
			item = this.collectionItemService.queryItemById(itemId);
		if (item == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_ITEM_NOT_EXIST);
			return Action.INPUT;
		}
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillCollectionItemInfoCheck(currentUser, force, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (!force)
		{
			if (this.value > item.getMaxScore() || this.value < item.getMinScore())
			{
				FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_CHECK_VALUE_OUT_OF_RANGE);
				return Action.ERROR;
			}
		}

		SysUserOut infoTargetUser = this.sysUserService.queryUserOutById(this.infoTarget);
		if (infoTargetUser == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_EXIST);
			return Action.INPUT;
		}
		ErrorType errorType;
		if ((errorType = this.infoService.setCollectionItemInfoCheck(currentUser, this.itemId, infoTargetUser, this.value, force)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else
		{
			FieldErrorGenerator.addFieldError(this, errorType);
			return Action.ERROR;
		}
	}
	
	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}
	
	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
	
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	
	public void setInfoTarget(Long infoTarget) {
		this.infoTarget = infoTarget;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public Long getInfoTarget() {
		return infoTarget;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
