package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemInfoImage;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.CollectionItemInfoImageDao;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class DeleteCollectionItemInfoImageAction extends ActionSupport 
{
	private Long id;
	
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private CollectionItemInfoImageDao collectionItemInfoImageDao;

	@Transactional
	public String execute()
	{
		CollectionItemInfoImage collectionItemInfoImage = null;
		if (this.id != null && this.id > 0)
			collectionItemInfoImage = this.collectionItemInfoImageDao.queryById(id);
		if (collectionItemInfoImage == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_ITEM_INFO_IMAGE_NOT_EXIST);
			return Action.INPUT;
		}
		CollectionItemInfo info = collectionItemInfoImage.getCollectionItemInfo();
		if (this.systemStateService.fillCollectionItemInfo(info.getCollectionItem()) != ISystemStateService.CheckResult.Suitable)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.SYSTEM_STATE_NOT_SUITABLE);
			return Action.INPUT;
		}
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Long userId = info.getInfoProvider().getId();
		if (userId.equals(currentUser.getId()))
		{
			this.collectionItemInfoImageDao.deleteCollectionItemInfoImageById(this.id);
			return Action.SUCCESS;
		}
		else
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_IS_NOT_INFOPROVIDER);
			return Action.INPUT;
		}
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId()
	{
		return this.id;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCollectionItemInfoImageDao(
			CollectionItemInfoImageDao collectionItemInfoImageDao) {
		this.collectionItemInfoImageDao = collectionItemInfoImageDao;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}
}
