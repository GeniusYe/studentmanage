package com.geniusye.studentmanage.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemInfoImage;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryCollectionItemInfoImageAction extends ActionSupport
{
	private ICollectionItemService collectionItemService;
	private IInfoService infoService;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private SystemState systemState;
	
	private Long itemId;
	private Long userId;
	
	private List<CollectionItemInfoImage> itemInfoImage;

	@Transactional
	public String execute()
	{
		CollectionItem item = null;
		if (this.itemId != null)
			item = this.collectionItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.queryCollectionItemInfoImage(currentUser, item, this.userId);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(item.getRelatedCourse());
		
		CollectionItemInfo collectionItemInfo = null;
		if (itemId != null && itemId > 0)
			collectionItemInfo = this.infoService.queryCollectionItemInfoByInfoProviderAndItemId(this.userId, this.itemId);
		
		if (collectionItemInfo != null)
			this.itemInfoImage = collectionItemInfo.getImages();
		else
			this.itemInfoImage = new ArrayList<CollectionItemInfoImage>();
		
		return Action.SUCCESS;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public List<CollectionItemInfoImage> getItemInfoImage() {
		return itemInfoImage;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
