package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryPublicHearingsItemOneAction extends ActionSupport 
{
	private IPublicHearingsItemService publicHearingsItemService;
	private PublicHearingsItem publicHearingsItem;
	private ISysUserService sysUserService;
	
	private Long id;

	@Transactional
	public String queryItemById()
	{
		if (this.id == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		this.publicHearingsItem = this.publicHearingsItemService.queryItemById(this.id);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		if (!this.publicHearingsItem.getRelatedCourse().checkAdminUsers(currentUser))
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
			return Action.INPUT;
		}
		return Action.SUCCESS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PublicHearingsItem getPublicHearingsItem() {
		return publicHearingsItem;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
}
