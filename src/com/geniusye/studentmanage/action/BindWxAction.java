package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.SysUser;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class BindWxAction extends ActionSupport
{	
	private ISysUserService sysUserService;
	
	private String username;
	
	private String password;
	
	private String wxOpenId;
	
	@Transactional
	public String execute()
	{
		SysUser user = this.sysUserService.findByUsername(username);
		
		if (user.getPassword().equals(password))
		{
			SysUserOut oriUser = this.sysUserService.findUserOutByWxOpenId(wxOpenId);
			if (oriUser != null && !oriUser.getId().equals(user.getId()))
			{
				FieldErrorGenerator.addFieldError(this, ErrorType.WXOPENID_ALREADY_EXISTS);
				return Action.INPUT;
			}
			
			user.setWxOpenId(wxOpenId);
			return Action.SUCCESS;
		}
		else
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USERNAME_OR_PASSWORD_WRONG);
			return Action.ERROR;
		}
	}
	
	@Transactional
	public String unbind()
	{
		SysUserOut oriUser = this.sysUserService.findUserOutByWxOpenId(wxOpenId);

		if (oriUser != null)
			oriUser.setWxOpenId(null);
		
		return Action.SUCCESS;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}
	
}
