package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IInfoService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class FillCollectionItemInfoAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private ISysUserService sysUserService;
	private IInfoService infoService;
	private IPermissionCheckService permissionCheckService;

	private Long itemId;
	
	private String text;

	@Transactional
	public String execute()
	{
		CollectionItem item = null;
		if (this.itemId != null)
			item = this.collectionItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillCollectionItemInfo(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		SysUserOut user = this.sysUserService.findCurrentUser();
		ErrorType errorCode;
		if ((errorCode = this.infoService.setCollectionItemInfo(user, this.itemId, this.text)) == ErrorType.NO_ERROR)
			return Action.SUCCESS;
		else 
		{
			FieldErrorGenerator.addFieldError(this, errorCode);
			return Action.INPUT;
		}
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getItemId() {
		return itemId;
	}

	public String getText() {
		return text;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setInfoService(IInfoService infoService) {
		this.infoService = infoService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
