package com.geniusye.studentmanage.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.ICollectionItemInfoImageService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class FillCollectionItemInfoImageAction extends ActionSupport
{
	private ISysUserService sysUserService;
	private ICollectionItemInfoImageService collectionItemInfoImageService;
	private ICollectionItemService collectionItemDao;
	private IPermissionCheckService permissionCheckService;
	
	private Long itemId;
	private Long id;		//itemInfoImage id
	private File upload;
	private String uploadContentType;
	private SystemState systemState;

	@Transactional
	public String execute() throws IOException, SQLException
	{
		CollectionItem item = null;
		if (this.itemId != null)
			item = this.collectionItemDao.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.fillCollectionItemInfo(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (upload == null || uploadContentType == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
			return Action.INPUT;
		}
		if (item.getStartFillingTime() != null)
		{
			Date date = new Date();
			if (date.before(item.getStartFillingTime()) || date.after(item.getEndFillingTime()))
			{
				FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_ITEM_FILLING_IS_NOT_AT_A_PROPER_ITEM_STATE);
			}
		}
		FileInputStream inputStream = new FileInputStream(this.upload);
		this.id = this.collectionItemInfoImageService.createCollectionItemInfoImage(this.sysUserService.findCurrentUser(), itemId, inputStream, uploadContentType);
		
		return Action.SUCCESS;
	}

	public Long getId() {
		return id;
	}

	public Long getItemId() {
		return itemId;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	public void setCollectionItemInfoImageService(
			ICollectionItemInfoImageService collectionItemInfoImageService) {
		this.collectionItemInfoImageService = collectionItemInfoImageService;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

}
