package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class DeletePublicHearingsItemAction extends ActionSupport 
{
	private Long itemId;
	
	private IPublicHearingsItemService publicHearingsItemService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;

	@Transactional
	public String execute()
	{
		PublicHearingsItem item = null;
		if (this.itemId != null)
			item = this.publicHearingsItemService.queryItemById(itemId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.deletePublicHearingsItem(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.publicHearingsItemService.delete(item);
		return Action.SUCCESS;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
