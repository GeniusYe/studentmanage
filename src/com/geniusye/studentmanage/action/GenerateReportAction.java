package com.geniusye.studentmanage.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.struts2.ServletActionContext;
import org.springframework.transaction.annotation.Transactional;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICalculateScoreService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IGenerateReportService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class GenerateReportAction extends ActionSupport 
{
	private ICalculateScoreService calculateScoreService;
	private IGenerateReportService generateReportService;
	private ISystemStateService systemStateService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	private ISysUserService sysUserService;
	
	private SystemState systemState;
	
	private Long courseId;

	@Transactional
	public InputStream getDownloadFile() throws FileNotFoundException 
	{ 
		String path = ServletActionContext.getServletContext().getRealPath("/") + "usr_in\\report\\report.xls";
		InputStream inputStream = new FileInputStream(path);
		return inputStream;
	}

	@Transactional
	public String execute()
	{
		Course course = this.courseService.queryById(courseId);
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.generateReport(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		if (this.systemStateService.generateReport(course) != ISystemStateService.CheckResult.Suitable)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.SYSTEM_STATE_NOT_SUITABLE);
			return Action.INPUT;
		}
		calculateScoreService.calculateTotalScore(course);
		try {
			String path = ServletActionContext.getServletContext().getRealPath("/");
			File f = new File(path + "usr_in\\report");
			f.mkdir();
			this.generateReportService.generateReport(course, path + "usr_in\\report\\report.xls");
			return Action.SUCCESS;
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Action.ERROR;
	}

	public void setGenerateReportService(IGenerateReportService generateReportService) {
		this.generateReportService = generateReportService;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setCalculateScoreService(ICalculateScoreService calculateScoreService) {
		this.calculateScoreService = calculateScoreService;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

}
