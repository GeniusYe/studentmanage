package com.geniusye.studentmanage.action;

import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Message;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.MessageDao;
import com.geniusye.studentmanage.service.ISysUserService;
import com.opensymphony.xwork2.Action;

public class CreateMessageAction {
	private ISysUserService sysUserService;
	private MessageDao messageDao;		
	private Message message;
	private Long infoTargetId; //收件人id 用户id是long型，message的id是Integer型
	private String messageText;   //站内信正文
	private String messageTitle;  //站内信标题
	private String infoTargetUsername;
	
	@Transactional
	public String execute(){
		//SysUserOut infoTarget = this.sysUserService.queryUserOutWithCheckItemById(this.infoTargetId);
		SysUserOut infoTarget = this.sysUserService.findUserOutByUsername(this.infoTargetUsername);
		SysUserOut infoProvider = this.sysUserService.findCurrentUser();		
		Date date = new Date();
		message = new Message();
		message.setInfoTarget(infoTarget);
		message.setInfoProvider(infoProvider);
		message.setMessage(messageText);
		message.setTitle(messageTitle);
		message.setTime(date);//发信日期为处理发送操作时的系统时间
		if (message.getId() != null && message.getId() > 0)
			this.messageDao.merge(message);
		else
			this.messageDao.save(message);
		return Action.SUCCESS;
	}
	
	public ISysUserService getSysUserService() {
		return sysUserService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public MessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public Long getInfoTargetId() {
		return infoTargetId;
	}

	public void setInfoTargetId(Long infoTargetId) {
		this.infoTargetId = infoTargetId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getMessageTitle() {
		return messageTitle;
	}

	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	
	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public String getInfoTargetUsername() {
		return infoTargetUsername;
	}

	public void setInfoTargetUsername(String infoTargetUsername) {
		this.infoTargetUsername = infoTargetUsername;
	}


}
