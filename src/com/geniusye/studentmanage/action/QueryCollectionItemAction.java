package com.geniusye.studentmanage.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryCollectionItemAction extends ActionSupport 
{
	private ICollectionItemService collectionItemDao;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	
	private List<CollectionItem> collectionItemList;
	private SystemState systemState;
	
	private Long courseId;

	@Transactional
	public String queryItemAll()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryCollectionItemAll(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		this.collectionItemList = this.collectionItemDao.queryItemAll(courseId);
		return Action.SUCCESS;
	}
	
	@Transactional
	public String queryItemWhichNeedCheck()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryCollectionItemAll(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		this.collectionItemList = this.collectionItemDao.queryItemWhichNeedCheck(courseId);
		return Action.SUCCESS;
	}
//
//	@Transactional
//	public String queryItemWhichShallBePublicized()
//	{
//		SysUserOut currentUser = this.sysUserService.findCurrentUser();
//		Course course = this.courseService.queryById(courseId);
//		PermissionLevel permissionLevel = permissionCheckService.query(currentUser, course);
//		if (!permissionLevel.canExecute())
//		{
//			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
//			return Action.ERROR;
//		}
//		this.systemState = this.systemStateService.getSystemState(course);
//		this.collectionItemList = this.collectionItemDao.queryItemWhichShallBePublicized(courseId);
//		return Action.SUCCESS;
//	}
//
	@Transactional
	public String queryItemWithPersonalInfoAll()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.querySelfCollectionItemInfo(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		@SuppressWarnings("unused")
		SysUserOut user = this.sysUserService.findCurrentUser();
		this.collectionItemList = this.collectionItemDao.queryItemAll(courseId);
		List<CollectionItemInfo> allItemInfoList = new ArrayList<CollectionItemInfo>();
		
		int i = 0, j = 0, ci = this.collectionItemList.size(), cj = allItemInfoList.size();
		boolean t;
		CollectionItemInfo itemInfo = null;
		while (i < ci)
		{
			CollectionItem item = this.collectionItemList.get(i);
			
			t = false;
			if (j < cj)
			{
				itemInfo = allItemInfoList.get(j);
				while (itemInfo.getCollectionItem().getId() < item.getId() && j < cj - 1)
				{
					j++;
					itemInfo = allItemInfoList.get(j);
				}
				if (itemInfo.getCollectionItem().getId().equals(item.getId()))
				{
					itemInfo.setCollectionItem(item);
					List<CollectionItemInfo> itemInfos = new ArrayList<CollectionItemInfo>();
					itemInfos.add(itemInfo);
					item.setCollectedItemInfo(itemInfos);
					j++;
					t = true;
				}
			}
			if (!t)
				item.setCollectedItemInfo(new ArrayList<CollectionItemInfo>());
			
			i++;
		}
		return Action.SUCCESS;
	}
	
	@Transactional
	public String queryItemByCurrentUser()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryCollectionItemWhichNeedCheck(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		
		this.collectionItemList = this.collectionItemDao.queryItemByCheckOperatorId(currentUser.getId(), courseId);
		return Action.SUCCESS;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemDao) {
		this.collectionItemDao = collectionItemDao;
	}

	public List<CollectionItem> getCollectionItemList() {
		return this.collectionItemList;
	}

	public SystemState getSystemState() {
		return this.systemState;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public Long getCourseId() {
		return courseId;
	}
}
