package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class IdentifyUserRoleAction extends ActionSupport
{
	private ICourseService courseService;
	
	private ISysUserService sysUserService;
	
	private Long courseId;
	
	private String redirectAddress;
	
	@Transactional
	public String execute()
	{
		SysUserOut user = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		if (course.checkStuUsers(user))
			redirectAddress = "usr/queryItemWhichShallBePublicized.action?courseId=" + courseId;
		else if (course.checkTeacherUsers(user) || course.checkTaUsers(user))
			redirectAddress = "usr_in/gotoIndexPage.action?courseId=" + courseId;
		else
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_LIST);
			return Action.INPUT;
		}
		return Action.SUCCESS;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getRedirectAddress() {
		return redirectAddress;
	}

}
