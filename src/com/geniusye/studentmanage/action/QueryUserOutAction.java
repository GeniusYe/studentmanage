package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.SysUserDao;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryUserOutAction extends ActionSupport 
{
	private SysUserDao sysUserDao;
	private ISysUserService sysUserService;
	private ICourseService courseService;
	private IPermissionCheckService permissionCheckService;
	
	private Long courseId;
	
	private SysUserOut user;
	private List<SysUserOut> users;

	@Transactional
	public String queryTaUsers()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryUsers(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		users = this.sysUserDao.queryAllTasByCourse(courseId);
		return Action.SUCCESS;
	}
	
//	@Transactional
//	public String queryUserOutById()
//	{
//		if (id == null)
//		{
//			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
//			return Action.INPUT;
//		}
//		this.user = this.sysUserService.queryUserOutById(id);
//		return Action.SUCCESS;
//	}

//	@Transactional
//	public String queryAllUserOut()
//	{
//		SysUserOut currentUser = this.sysUserService.findCurrentUser();
//		Course course = this.courseService.queryById(courseId);
//		if (!course.checkAdminUsers(currentUser))
//		{
//			FieldErrorGenerator.addFieldError(this, ErrorType.USER_NOT_IN_COURSE_ADMIN_LIST);
//			return Action.INPUT;
//		}
//		this.users = this.sysUserService.queryAllUserOut(courseId);
//		return Action.SUCCESS;
//	}
//
//	@Transactional
//	public String queryAllUser()
//	{
//		this.users = this.sysUserService.queryAllUser();
//		return Action.SUCCESS;
//	}

//	@Transactional
//	public String queryUserOutWithCheckItemById()
//	{
//		if (id == null)
//		{
//			FieldErrorGenerator.addFieldError(this, ErrorType.PARAMETER_NO_GIVEN);
//			return Action.INPUT;
//		}
//		this.user = this.sysUserService.queryUserOutWithCheckItemById(id);
//		return Action.SUCCESS;
//	}

	public SysUserOut getUser() {
		return this.user;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public List<SysUserOut> getUsers() {
		return users;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setSysUserDao(SysUserDao sysUserDao) {
		this.sysUserDao = sysUserDao;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
