package com.geniusye.studentmanage.action;

import java.io.InputStream;
import java.sql.SQLException;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.CollectionItemInfo;
import com.geniusye.studentmanage.bean.CollectionItemInfoImage;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.dao.CollectionItemInfoImageDao;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.ErrorType;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryCollectionItemInfoImageOneAction extends ActionSupport
{
	private CollectionItemInfoImageDao collectionItemInfoImageDao;
	private ISystemStateService systemStateService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private SystemState systemState;
	
	private Long id;
	private Boolean ifOriPic;
	
	private CollectionItemInfoImage itemInfoImage;
	
	private int errorCode;

	@Transactional
	public String execute()
	{
		if (id != null && id > 0)
			this.itemInfoImage = this.collectionItemInfoImageDao.queryById(id);
		if (this.itemInfoImage == null)
		{
			FieldErrorGenerator.addFieldError(this, ErrorType.COLLECTION_ITEM_INFO_IMAGE_NOT_EXIST);
			return Action.ERROR;
		}
		
		CollectionItem item = this.itemInfoImage.getCollectionItemInfo().getCollectionItem();
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.queryCollectionItemInfoImage(currentUser, item, this.itemInfoImage.getCollectionItemInfo().getInfoProvider().getId());
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(item.getRelatedCourse());
		CollectionItemInfo itemInfo = this.itemInfoImage.getCollectionItemInfo();
		if (itemInfo == null)
		{
			this.errorCode = 103;
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(item.getRelatedCourse());
		
		return Action.SUCCESS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIfOriPic(Boolean ifOriPic) {
		this.ifOriPic = ifOriPic;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setCollectionItemInfoImageDao(
			CollectionItemInfoImageDao collectionItemInfoImageDao) {
		this.collectionItemInfoImageDao = collectionItemInfoImageDao;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
	
	public InputStream getImage() throws SQLException
	{
		if (ifOriPic != null && ifOriPic == true)
			return this.itemInfoImage.getImage().getBinaryStream();
		else
			return this.itemInfoImage.getSmallImage().getBinaryStream();
	}
	
	public String getType()
	{
		if (ifOriPic != null && ifOriPic == true)
			return this.itemInfoImage.getType();
		else
			return "image/jpg";
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
