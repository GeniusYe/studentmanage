package com.geniusye.studentmanage.action;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.Message;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.dao.MessageDao;
import com.geniusye.studentmanage.service.ISysUserService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryMessageByTwoUsersAction extends ActionSupport 
{
	private ISysUserService sysUserService;
	private MessageDao messageDao;		
	private Message latestM;
	private List<Message> messageList;
	private Long user_out_id;
	private Long user_in_id;
	
	@Transactional
	public String execute()//这一段纯属写来测试的= =
	{
		/*queryByInfoProvider();
		for(int i = 0; i<this.messageList.size(); i++)
		{
			this.message = this.messageList.get(i);
			System.out.println("check infoProvider:\nmessage: \t" + "id=" + this.message.getId());
		}
		queryByInfoTarget();
		for(int i = 0; i<this.messageList.size(); i++)
		{
			this.message = this.messageList.get(i);
			System.out.println("check infoTarget:\nmessage: \t" + "id=" + this.message.getId());
		}*/
		return Action.SUCCESS;
		
	}
	
	@Transactional
	public String queryByInfoTarget() //查看所有发给自己的信，相当于查看“收件”
	{
		SysUserOut infoTarget = this.sysUserService.findCurrentUser();
		this.messageList = this.messageDao.queryByInfoTarget(infoTarget);
		return Action.SUCCESS;
	}
	
	@Transactional
	public String queryByInfoProvider() //查看所有自己发出的信,相当于查看“已发送”
	{
		SysUserOut infoProvider = this.sysUserService.findCurrentUser();
		this.messageList = this.messageDao.queryByInfoProvider(infoProvider);
		return Action.SUCCESS;
	}
	
	@Transactional
	public String queryByTwoUsers() //查看所有自己发出的信,相当于查看“已发送”
	{
		SysUserOut user1 = this.sysUserService.findCurrentUser();
		SysUserOut user2 = this.sysUserService.queryUserOutById(this.user_out_id);
		this.messageList = this.messageDao.queryByTwoUsers(user1, user2);
		this.latestM = this.messageList.get(this.messageList.size()-1);
		this.user_in_id = user1.getId();
		return Action.SUCCESS;
	}

	public List<Message> getMessageList() {
		return messageList;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public Long getUser_out_id() {
		return user_out_id;
	}

	public void setUser_out_id(Long user_out_id) {
		this.user_out_id = user_out_id;
	}

	public Message getLatestM() {
		return latestM;
	}

	public void setLatestM(Message latestM) {
		this.latestM = latestM;
	}

	public Long getUser_in_id() {
		return user_in_id;
	}

	public void setUser_in_id(Long user_in_id) {
		this.user_in_id = user_in_id;
	}

}

