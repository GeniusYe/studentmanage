package com.geniusye.studentmanage.action;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class CreateCollectionItemPublicitySetAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private ISysUserService sysUserService;
	private IPermissionCheckService permissionCheckService;
	
	private Long id;
	private Integer publicityType;

	@Transactional
	public String execute()
	{
		CollectionItem item = null;
		if (this.id != null && this.id > 0)
			item = this.collectionItemService.queryItemById(this.id);

		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		PermissionLevel permissionLevel = permissionCheckService.updateCollectionItemPublicity(currentUser, item);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		
		if (this.publicityType == null)
			this.publicityType = 0;
		
		this.collectionItemService.setPublicityType(item, this.publicityType);
		
		return Action.SUCCESS;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setPublicityType(Integer publicityType) {
		this.publicityType = publicityType;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}
}
