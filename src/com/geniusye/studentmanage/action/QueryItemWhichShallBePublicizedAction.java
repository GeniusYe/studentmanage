package com.geniusye.studentmanage.action;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.Course;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;
import com.geniusye.studentmanage.bean.SystemState;
import com.geniusye.studentmanage.service.ICollectionItemService;
import com.geniusye.studentmanage.service.IPublicHearingsItemService;
import com.geniusye.studentmanage.service.ICourseService;
import com.geniusye.studentmanage.service.IPermissionCheckService;
import com.geniusye.studentmanage.service.ISysUserService;
import com.geniusye.studentmanage.service.ISystemStateService;
import com.geniusye.studentmanage.util.FieldErrorGenerator;
import com.geniusye.studentmanage.util.PermissionLevel;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class QueryItemWhichShallBePublicizedAction extends ActionSupport 
{
	private ICollectionItemService collectionItemService;
	private IPublicHearingsItemService publicHearingsItemService;
	private ISystemStateService systemStateService;
	private IPermissionCheckService permissionCheckService;
	private ICourseService courseService;
	private ISysUserService sysUserService;
	
	private List<CollectionItem> collectionItemList;
	private List<PublicHearingsItem> publicHearingsItemList;
	private SystemState systemState;
	
	private Long courseId;

	@Transactional
	public String execute()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryItemWhichShallBePublicity(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		this.collectionItemList = this.collectionItemService.queryItemWhichShallBePublicized(courseId);
		this.publicHearingsItemList = this.publicHearingsItemService.queryItemWhichShallBePublicized(courseId);
		
		return Action.SUCCESS;
	}

	@Transactional
	public String executeText()
	{
		SysUserOut currentUser = this.sysUserService.findCurrentUser();
		Course course = this.courseService.queryById(courseId);
		PermissionLevel permissionLevel = permissionCheckService.queryItemWhichShallBePublicity(currentUser, course);
		if (!permissionLevel.canExecute())
		{
			FieldErrorGenerator.addFieldError(this, permissionLevel.getError());
			return Action.ERROR;
		}
		this.systemState = this.systemStateService.getSystemState(course);
		this.publicHearingsItemList = this.publicHearingsItemService.queryItemTextWhichShallBePublicized(courseId);
		return Action.SUCCESS;
	}

	public List<CollectionItem> getCollectionItemList() {
		return collectionItemList;
	}

	public List<PublicHearingsItem> getPublicHearingsItemList() {
		return publicHearingsItemList;
	}

	public List<CollectionItem> getCollectionItems() {
		return collectionItemList;
	}

	public List<PublicHearingsItem> getPublicHearingsItem() {
		return publicHearingsItemList;
	}

	public SystemState getSystemState() {
		return systemState;
	}

	public void setCollectionItemService(ICollectionItemService collectionItemService) {
		this.collectionItemService = collectionItemService;
	}

	public void setPublicHearingsItemService(
			IPublicHearingsItemService publicHearingsItemService) {
		this.publicHearingsItemService = publicHearingsItemService;
	}

	public void setSystemStateService(ISystemStateService systemStateService) {
		this.systemStateService = systemStateService;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setPermissionCheckService(
			IPermissionCheckService permissionCheckService) {
		this.permissionCheckService = permissionCheckService;
	}

	public void setCourseService(ICourseService courseService) {
		this.courseService = courseService;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setSysUserService(ISysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
}
