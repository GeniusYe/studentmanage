package com.geniusye.studentmanage.dao;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.CollectionItemInfo;

public class CollectionItemInfoDao extends HibernateDaoSupport 
{
	public CollectionItemInfo save(CollectionItemInfo collectionItemInfo)
	{
		return (CollectionItemInfo)getSession().save(collectionItemInfo);
	}
	
	public void merge(CollectionItemInfo collectionItemInfo)
	{
		getSession().merge(collectionItemInfo);
	}
	
	public CollectionItemInfo queryByInfoProviderAndItemId(Long infoProvider, Long itemId)
	{
		Query q = getSession().createQuery("SELECT i FROM CollectionItemInfo AS i WHERE i.collectionItem = :itemId AND i.infoProvider = :infoProvider");
		q.setLong("itemId", itemId);
		q.setLong("infoProvider", infoProvider);
		return (CollectionItemInfo) q.uniqueResult();
	}
}
