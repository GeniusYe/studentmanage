package com.geniusye.studentmanage.dao;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.SysUser;

public class SensitiveSysUserDao extends HibernateDaoSupport 
{
	public void merge(SysUser user)
	{
		getSession().merge(user);
	}
	
	public SysUser findByUsername(String name) {
		Query q = getSession().createQuery("select u from SysUser as u where u.username = :name");
		q.setString("name", name);
		return (SysUser) q.uniqueResult();
	}
	
	public SysUser queryById(Long userId) {
		Query q = getSession().createQuery("select u from SysUser as u where u.id = :userId");
		q.setLong("userId", userId);
		return (SysUser) q.uniqueResult();
	}
}
