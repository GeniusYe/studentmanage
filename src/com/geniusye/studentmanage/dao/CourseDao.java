package com.geniusye.studentmanage.dao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.Course;

public class CourseDao extends HibernateDaoSupport
{
	public Course queryById(Long courseId)
	{
		return (Course) getSession().load(Course.class, courseId);
	}
}
