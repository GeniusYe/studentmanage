package com.geniusye.studentmanage.dao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.SystemState;

public class SystemStateDao extends HibernateDaoSupport
{
	public void merge(SystemState systemState)
	{
		getSession().merge(systemState);
	}
	
	public SystemState query(Long courseId)
	{
		return (SystemState) getSession().load(SystemState.class, courseId);
	}
}
