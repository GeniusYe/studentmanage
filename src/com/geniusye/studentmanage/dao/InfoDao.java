package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.CollectionItem;
import com.geniusye.studentmanage.bean.PublicHearingsItem;
import com.geniusye.studentmanage.bean.SysUserOut;

public class InfoDao extends HibernateDaoSupport
{
	/**
	 * 
	 * @param collectionItem
	 * @param user
	 * @return List<Object> {userid, username, value, sforce, user_text, user_expected_value}
	 */
	public List<Object> queryCollectionItemInfoCheckToAllUserForce(CollectionItem collectionItem) 
	{
		String infoCheckStr = "COLLECTION_ITEM_INFO_CHECK AS ciic ON ciic.infoTarget_id = u.id AND ciic.collectionItem_id = :itemId AND ciic.sforce = 1";
		String infoStr = "COLLECTION_ITEM_INFO AS cii ON cii.infoProvider_id = u.id AND cii.collectionItem_id = :itemId";
		String courseStuStr = "SELECT c.sys_user_id FROM COURSE_USER_STU AS c WHERE c.course_id = :courseId";
		String userStr = "SELECT u.id, u.name, ciic.value, ciic.sforce, cii.text FROM SYS_USER AS u LEFT JOIN " + infoCheckStr + " LEFT JOIN " + infoStr + " WHERE u.id IN (" + courseStuStr + ")";
		Query q = getSession().createSQLQuery(userStr);
		q.setLong("courseId", collectionItem.getRelatedCourse().getId());
		q.setLong("itemId", collectionItem.getId());
		@SuppressWarnings("unchecked")
		List<Object> list = q.list();
		return list;
	}
	
	/**
	 * 
	 * @param collectionItem
	 * @param user
	 * @return List<Object> {userid, username, value, sforce, user_text, user_expected_value}
	 */
	public List<Object> queryCollectionItemInfoCheckToAllUser(CollectionItem collectionItem, SysUserOut user) 
	{
		String infoCheckStr = "COLLECTION_ITEM_INFO_CHECK AS ciic ON ciic.infoTarget_id = u.id AND ciic.collectionItem_id = :itemId AND (ciic.infoProvider_id = :infoProvider OR ciic.sforce = 1)";
		String infoStr = "COLLECTION_ITEM_INFO AS cii ON cii.infoProvider_id = u.id AND cii.collectionItem_id = :itemId";
		String courseStuStr = "SELECT c.sys_user_id FROM COURSE_USER_STU AS c WHERE c.course_id = :courseId";
		String userStr = "SELECT u.id, u.name, ciic.value, ciic.sforce, cii.text FROM SYS_USER AS u LEFT JOIN " + infoCheckStr + " LEFT JOIN " + infoStr + " WHERE u.id IN (" + courseStuStr + ")";
		Query q = getSession().createSQLQuery(userStr);
		q.setLong("courseId", collectionItem.getRelatedCourse().getId());
		q.setLong("itemId", collectionItem.getId());
		q.setLong("infoProvider", user.getId());
		@SuppressWarnings("unchecked")
		List<Object> list = q.list();
		return list;
	}
	
	/**
	 * 
	 * @param collectionItem
	 * @param user
	 * @return List<Object> {userid, username, value}
	 */
	public List<Object> queryPublicHearingsItemInfoToAllUser(PublicHearingsItem publicHearingsItem, SysUserOut user) 
	{

		String infoCheckStr = "PUB_HEARINGS_ITEM_INFO AS ciic ON ciic.infoTarget_id = u.id AND ciic.publicHearingsItem_id = :itemId AND ciic.infoProvider_id = :infoProvider";
		String courseStuStr = "SELECT c.sys_user_id FROM COURSE_USER_STU AS c WHERE c.course_id = :courseId";
		String userStr = "SELECT u.id, u.name, ciic.value FROM SYS_USER AS u LEFT JOIN " + infoCheckStr + " WHERE u.id IN (" + courseStuStr + ")";
		Query q = getSession().createSQLQuery(userStr);
		q.setLong("courseId", publicHearingsItem.getRelatedCourse().getId());
		q.setLong("itemId", publicHearingsItem.getId());
		q.setLong("infoProvider", user.getId());
		@SuppressWarnings("unchecked")
		List<Object> list = q.list();
		return list;
	}
	
	/**
	 * 
	 * @param collectionItem
	 * @param user
	 * @return List<Object> {userid, username, text}
	 */
	public List<Object> queryPublicHearingsItemInfoTextToAllUser(PublicHearingsItem publicHearingsItem, SysUserOut user) 
	{
		String infoCheckStr = "PUB_HEARINGS_ITEM_INFO_TEXT AS ciic ON ciic.infoTarget_id = u.id AND ciic.publicHearingsItem_id = :itemId AND ciic.infoProvider_id = :infoProvider";
		String courseStuStr = "SELECT c.sys_user_id FROM COURSE_USER_STU AS c WHERE c.course_id = :courseId";
		String userStr = "SELECT u.id, u.name, ciic.text FROM SYS_USER AS u LEFT JOIN " + infoCheckStr + " WHERE u.id IN (" + courseStuStr + ")";
		Query q = getSession().createSQLQuery(userStr);
		q.setLong("courseId", publicHearingsItem.getRelatedCourse().getId());
		q.setLong("itemId", publicHearingsItem.getId());
		q.setLong("infoProvider", user.getId());
		@SuppressWarnings("unchecked")
		List<Object> list = q.list();
		return list;
	}
}
