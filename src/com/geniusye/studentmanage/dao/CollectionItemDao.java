package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.CollectionItem;

public class CollectionItemDao extends HibernateDaoSupport 
{
	public void save(CollectionItem item)
	{
		getSession().save(item);
	}
	
	public void merge(CollectionItem item)
	{
		getSession().merge(item);
	}
	
	public void delete(CollectionItem item)
	{
		getSession().delete(item);
	}
	
	public CollectionItem queryItemById(Long id)
	{
		return (CollectionItem)getSession().load(CollectionItem.class, id);
	}
	
	public List<CollectionItem> queryItemByCheckOperatorId(Long userId, Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM CollectionItem AS item WHERE item.relatedCourse = :courseId AND :userId IN (FROM item.checkOperator) ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setLong("userId", userId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<CollectionItem> items = (List<CollectionItem>) q.list();
		return items;
	}
	
	public List<CollectionItem> queryItemAll(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM CollectionItem AS item WHERE item.relatedCourse = :courseId ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<CollectionItem> items = (List<CollectionItem>) q.list();
		return items;
	}
	
	public List<CollectionItem> queryItemWhichNeedCheck(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM CollectionItem AS item WHERE item.relatedCourse = :courseId ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<CollectionItem> items = (List<CollectionItem>) q.list();
		return items;
	}
	
	public List<CollectionItem> queryItemWhichShallBePublicized(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM CollectionItem AS item WHERE item.relatedCourse = :courseId AND item.publicityType >= 1 ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<CollectionItem> items = (List<CollectionItem>) q.list();
		return items;
	}
}
