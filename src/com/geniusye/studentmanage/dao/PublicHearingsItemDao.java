package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.PublicHearingsItem;

public class PublicHearingsItemDao extends HibernateDaoSupport 
{
	public void save(PublicHearingsItem item)
	{
		getSession().save(item);
	}
	
	public void merge(PublicHearingsItem item)
	{
		getSession().merge(item);
	}
	
	public void delete(PublicHearingsItem item)
	{
		getSession().delete(item);
	}
	
	public PublicHearingsItem queryItemById(Long id)
	{
		return (PublicHearingsItem) getSession().load(PublicHearingsItem.class, id);
	}
	
	public List<PublicHearingsItem> queryItemAll(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM PublicHearingsItem AS item WHERE item.relatedCourse = :courseId ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<PublicHearingsItem> items = (List<PublicHearingsItem>) q.list();
		return items;
	}
	
	public List<PublicHearingsItem> queryItemWhichShallBePublicized(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM PublicHearingsItem AS item WHERE item.relatedCourse = :courseId AND item.type = 0 ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<PublicHearingsItem> items = (List<PublicHearingsItem>) q.list();
		return items;
	}
	
	public List<PublicHearingsItem> queryItemTextWhichShallBePublicized(Long courseId)
	{
		Query q = getSession().createQuery("SELECT item FROM PublicHearingsItem AS item WHERE item.relatedCourse = :courseId AND item.type = 1 ORDER BY item.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<PublicHearingsItem> items = (List<PublicHearingsItem>) q.list();
		return items;
	}
}
