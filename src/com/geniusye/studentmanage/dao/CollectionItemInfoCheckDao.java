package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.CollectionItemInfoCheck;

public class CollectionItemInfoCheckDao extends HibernateDaoSupport 
{
	public void save(CollectionItemInfoCheck collectionItemInfoCheck)
	{
		getSession().save(collectionItemInfoCheck);
	}
	
	public void merge(CollectionItemInfoCheck collectionItemInfoCheck)
	{
		getSession().merge(collectionItemInfoCheck);
	}
	
	public void delete(CollectionItemInfoCheck collectionItemInfoCheck)
	{
		getSession().delete(collectionItemInfoCheck);
	}

	public List<CollectionItemInfoCheck> queryByInfoProviderAndItemId(Long infoProvider, Long itemId)
	{
		Query q = getSession().createQuery("SELECT i FROM CollectionItemInfoCheck AS i WHERE i.collectionItem = :itemId AND (i.infoProvider = :infoProvider OR i.infoProvider IS NULL) ORDER BY i.infoTarget");
		q.setLong("itemId", itemId);
		q.setLong("infoProvider", infoProvider);
		@SuppressWarnings("unchecked")
		List<CollectionItemInfoCheck> list = (List<CollectionItemInfoCheck>) q.list();
		return list;
	}
	
	public CollectionItemInfoCheck queryByInfoProviderAndInfoTargetAndItemId(Long infoProvider, Long infoTarget, Long itemId)
	{
		Query q = getSession().createQuery("SELECT i FROM CollectionItemInfoCheck AS i WHERE i.collectionItem = :itemId AND i.infoTarget = :infoTarget AND i.infoProvider = :infoProvider AND i.force = 0");
		q.setLong("itemId", itemId);
		q.setLong("infoTarget", infoTarget);
		q.setLong("infoProvider", infoProvider);
		return (CollectionItemInfoCheck) q.uniqueResult();
	}

	public CollectionItemInfoCheck queryForceInfoByInfoTargetAndItemId(Long infoTarget, Long itemId)
	{
		Query q = getSession().createQuery("SELECT i FROM CollectionItemInfoCheck AS i WHERE i.collectionItem = :itemId AND i.infoTarget = :infoTarget AND i.force = 1");
		q.setLong("itemId", itemId);
		q.setLong("infoTarget", infoTarget);
		return (CollectionItemInfoCheck) q.uniqueResult();
	}
}
