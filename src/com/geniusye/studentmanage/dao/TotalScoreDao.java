package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.TotalScore;

public class TotalScoreDao extends HibernateDaoSupport 
{
	public void save(TotalScore totalScore)
	{
		getSession().save(totalScore);
	}
	
	public void merge(TotalScore totalScore)
	{
		getSession().merge(totalScore);
	}
	
	//Use when generate report
	public List<TotalScore> queryByCourseId(Long courseId)
	{
		Query q = getSession().createQuery("SELECT i FROM TotalScore AS i WHERE i.relatedCourse = :courseId ORDER BY i.id");
		q.setLong("courseId", courseId);
		@SuppressWarnings("unchecked")
		List<TotalScore> totalScores = q.list();
		return totalScores;
	}
	
	//Use when setInfo
	public TotalScore queryByInfoTargetAndCourseId(Long courseId, Long infoTarget)
	{
		Query q = getSession().createQuery("SELECT i FROM TotalScore AS i WHERE i.relatedCourse = :courseId AND i.infoTarget = :infoTarget");
		q.setLong("courseId", courseId);
		q.setLong("infoTarget", infoTarget);
		return (TotalScore) q.uniqueResult();
	}
}
