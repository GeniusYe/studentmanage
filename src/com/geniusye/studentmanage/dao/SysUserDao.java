package com.geniusye.studentmanage.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.geniusye.studentmanage.bean.SysUserOut;

public class SysUserDao extends HibernateDaoSupport 
{
	
	public SysUserOut findUserOutByUsername(String name) {
		Query q = getSession().createQuery("select u from SysUserOut as u where u.username = :name");
		q.setString("name", name);
		q.setCacheable(true);
		return (SysUserOut) q.uniqueResult();
	}
	
	public SysUserOut findUserOutByWxOpenId(String wxOpenId) {
		Query q = getSession().createQuery("select u from SysUserOut as u where u.wxOpenId = :wxOpenId");
		q.setString("wxOpenId", wxOpenId);
		q.setCacheable(true);
		return (SysUserOut) q.uniqueResult();
	}
	
	public List<SysUserOut> queryAllTasByCourse(Long courseId) {
		Query q = getSession().createQuery("SELECT u FROM Course AS c LEFT JOIN c.taUsers AS u WHERE c.id = :courseId ORDER BY u.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<SysUserOut> list = q.list();
		return list;
	}
	
	public List<SysUserOut> queryAllStusByCourse(Long courseId) {
		Query q = getSession().createQuery("SELECT u FROM Course AS c LEFT JOIN c.stuUsers AS u WHERE c.id = :courseId ORDER BY u.id");
		q.setLong("courseId", courseId);
		q.setCacheable(true);
		@SuppressWarnings("unchecked")
		List<SysUserOut> list = q.list();
		return list;
	}
	
	public SysUserOut queryUserOutById(Long id)
	{
		return (SysUserOut) getSession().load(SysUserOut.class, id);
	}
}
