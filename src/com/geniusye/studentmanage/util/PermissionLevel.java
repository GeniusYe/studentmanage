package com.geniusye.studentmanage.util;

public class PermissionLevel 
{
	private boolean execute;
	private ErrorType error;

	public PermissionLevel(ErrorType error)
	{
		this.execute = false;
		this.error = error;
	}
	
	public PermissionLevel(boolean execute)
	{
		this.execute = execute;
	}
	
	public boolean canExecute()
	{
		return this.execute;
	}
	
	public ErrorType getError()
	{
		return this.error;
	}
}
