package com.geniusye.studentmanage.util;

import com.opensymphony.xwork2.ActionSupport;

public class FieldErrorGenerator 
{
	public static void addFieldError(ActionSupport action, ErrorType errorType)
	{
		throw new ActionException(errorType);
	}
}
