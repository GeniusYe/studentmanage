package com.geniusye.studentmanage.util;

import java.util.Comparator;

import com.geniusye.studentmanage.bean.ScoreInterface;

public class ScoreComparator<T extends ScoreInterface> implements Comparator<T>
{
	boolean revert;
	
	public ScoreComparator()
	{
		this(true);
	}
	public ScoreComparator(boolean revert)
	{
		this.revert = revert;
	}
	
	@Override
	public int compare(T o1, T o2) {
		if (revert)
			return -o1.getValue().compareTo(o2.getValue());
		else
			return o1.getValue().compareTo(o2.getValue());
	}
}