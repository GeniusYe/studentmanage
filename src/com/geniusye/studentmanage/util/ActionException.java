package com.geniusye.studentmanage.util;

@SuppressWarnings("serial")
public class ActionException extends RuntimeException
{
	private ErrorType err;
	
	public ActionException(ErrorType err)
	{
		super(err.toString());
		this.err = err;
	}

	public ErrorType getErr() {
		return err;
	}
	
	public String getErrStr()
	{
		String errStr;
		switch(err)
		{
		case USER_NOT_IN_COURSE_LIST:
			errStr = "您不是这门课程的教师、助教或学生";
			break;
		default:
			errStr = err.toString();
		}
		return errStr;
	}
}
