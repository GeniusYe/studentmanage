<?php 
function query_score($courseId, $wxOpenId)
{
	$post_data = array("courseId" => $courseId, "wxOpenId" => $wxOpenId);
	$url = 'http://ics.fudan.edu.cn/score/wx/queryScore.action';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// post数据
	curl_setopt($ch, CURLOPT_POST, 1);
	// post的变量
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	$output = curl_exec($ch);
	curl_close($ch);
	
	$data = json_decode($output, true);
	
	$str = "您的成绩如下：\n";
	foreach ($data["results"] as $item)
	{
		$str = $str . $item["itemNumber"] . '-' . $item["itemName"] . ' 成绩 ' . $item["value"];
		if (isset($item["rank"]))
			$str = $str . " 排名 " . $item["rank"];
		$str = $str . "\n";
	}
	if (isset($data["totalResult"]))
		$str = $str . '总成绩为' . $data["totalResult"]["value"] . ' 排名为' . $data["totalResult"]["rank"] . "\n";
	
	return $str;
}