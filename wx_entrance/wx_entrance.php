<?php
/**
  * wechat php test
  */

//define your token
define("TOKEN", "egphesflehg43wjoew");
$wechatObj = new wechatCallbackapiTest();
$wechatObj->responseMsg();

class wechatCallbackapiTest
{
	public function valid()
    {
        $echoStr = $_GET["echostr"];

        //valid signature , option
        if($this->checkSignature()){
        	echo $echoStr;
        	exit;
        }
    }

    public function responseMsg()
    {
		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

      	//extract post data
		if (!empty($postStr)){
                
              	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
                $fromUsername = $postObj->FromUserName;
                $toUsername = $postObj->ToUserName;
                $keyword = trim($postObj->Content);
                $time = time();
                $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
							
                $linkTpl = "<xml>
							 <ToUserName><![CDATA[%s]]></ToUserName>
							 <FromUserName><![CDATA[%s]]></FromUserName>
							 <CreateTime>%s</CreateTime>
							 <MsgType><![CDATA[news]]></MsgType>
							 <ArticleCount>1</ArticleCount>
							 <Articles>
							 <item>
							 <Title><![CDATA[%s]]></Title> 
							 <Description><![CDATA[%s]]></Description>
							 <PicUrl><![CDATA[%s]]></PicUrl>
							 <Url><![CDATA[%s]]></Url>
							 </item>
							 </Articles>
							 </xml>";
				if(!empty( $keyword ))
                {
					if ($keyword == "bd")
					{
						$titleStr = "请点击下面连接进行绑定";
						$descriptionStr = "";
						$url = "http://ics.fudan.edu.cn/score/wx/bindWx.jsp?wxOpenId=" . $fromUsername;
						$resultStr = sprintf($linkTpl, $fromUsername, $toUsername, $time, $titleStr, $descriptionStr, "", $url);
						//error_log($resultStr);
					}
					else if ($keyword == "unbd")
					{
						$url = "http://ics.fudan.edu.cn/score/wx/unbindWx.action?wxOpenId=" . $fromUsername;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						// post数据
						curl_setopt($ch, CURLOPT_POST, 1);
						// post的变量
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
						$output = curl_exec($ch);
						curl_close($ch);
						
						$msgType = "text";
						$contentStr = "解绑成功";
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					else if ($keyword == "cxcj")
					{
						$msgType = "text";
						$contentStr = 
						"目前已经开通的课程有：(课程年级与名称 - 编号)\n".
						"13程序设计(戴开宇)-07\n13程序设计(陈荣华)-08\n".
						"12计算机系统基础下(李弋)-01\n12计算机系统基础下(唐渊)-13\n12数据结构与算法分析(郑骁庆)-02\n12面向对象程序设计(外教)-12\n".
						"11智能系统原理与设计-03\n11多媒体基础-04\n11数据库设计-05\n11操作系统-06\n".
						"11科学的理论与实践-10\n11形式语言与计算理论-11\n".
						"10编译原理-09\n".
						"输入'cxcj+空格+课程编号'进行成绩查询，比如输入'cxcj 04'查询自己在多媒体基础课程中的成绩\n".
						"更多课程正在添加中，请耐心等待";
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					else if (substr($keyword, 0, 4) == "cxcj")
					{
						$courseId = intval(substr($keyword, 4));
						
						require_once("query_score.php");
						
						$msgType = "text";
						if ($courseId != 0)
							$contentStr = query_score($courseId, $fromUsername);
						else
							$contentStr = "对不起，您输入的格式有误，请重新输入，查询成绩请输入cxcj 空格 课程编号";
							
						error_log($contentStr);
							
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					}
					else
					{
						$msgType = "text";
						$contentStr = 
						"按键介绍：\n".
						"输入'bd'进行账号绑定\n输入'cxcj'进行成绩查询(绑定后才可以进行查询)\n".
						"更多功能正在开发中，请耐心等待";
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                	}
					echo $resultStr;
                }else{
                	echo "Input something...";
                }

        }else {
        	echo "";
        	exit;
        }
    }
		
	private function checkSignature()
	{
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];	
        		
		$token = TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}
}

?>